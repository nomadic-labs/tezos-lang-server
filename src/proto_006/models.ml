module Proto = Tezos_protocol_006_PsCARTHA.Protocol
module Context = Proto.Environment.Context
module Alpha_context = Proto.Alpha_context
module Script = Alpha_context.Script
module Michelson_v1_parser = Tezos_client_006_PsCARTHA.Michelson_v1_parser

module Contract = struct
  type t =
    { script : Script.t;
      self : Proto.Alpha_context.Contract.t;
      balance : int;
      ast : Michelson_v1_parser.parsed;
      addr : string
    }

  type unbuilt =
    { addr : string; code : string; storage : string; balance : int }

  let unbuilt_of_tuple (addr, code, storage, balance) =
    { addr; code; storage; balance }

  let parse_code code =
    match Michelson_v1_parser.parse_toplevel code with
    | (ast, []) -> return ast
    | (_, errl) -> Lwt.return @@ Error errl

  let parse_storage storage =
    match Michelson_v1_parser.parse_expression storage with
    | (storage, []) -> return storage.expanded
    | (_, errl) -> Lwt.return @@ Error errl

  let parse_addr addr =
    Lwt.return @@ Proto.Alpha_context.Contract.of_b58check addr
    >|= Proto.Environment.wrap_error

  let parse_script code storage =
    parse_code code >>=? fun code ->
    parse_storage storage >>=? fun storage ->
    return
      { Script.code = Script.lazy_expr code.expanded;
        storage = Script.lazy_expr storage
      }

  let parse_contract unbuilt =
    parse_script unbuilt.code unbuilt.storage >>=? fun script ->
    parse_addr unbuilt.addr >>=? fun self ->
    parse_code unbuilt.code >>=? fun ast ->
    return { script; balance = unbuilt.balance; addr = unbuilt.addr; self; ast }

  let build contracts = map_s parse_contract contracts
end

module OperationResult = struct
  type call_info =
    { addr : string;
      result : Proto.Script_interpreter.execution_result;
      trace : Proto.Script_interpreter.execution_trace;
      storage : Script.expr
    }

  type t =
    | RevealResult of { consumed_gas : int64 }
    | TransactionResult of
        { balance_updates : Alpha_context.Delegate.balance_updates;
          consumed_gas : int64;
          paid_storage_size_diff : int64;
          storage_size : int64;
          call_info : call_info option
        }
    | OriginationResult of
        { balance_updates : Alpha_context.Delegate.balance_updates;
          originated_contracts : Alpha_context.Contract.t list;
          consumed_gas : int64;
          storage_size : int64;
          paid_storage_size_diff : int64
        }
    | DelegationResult of { consumed_gas : int64 }

  type trace_result =
    | CallSuccess of
        Contract.t
        * Proto.Script_interpreter.execution_result
        * Proto.Script_interpreter.execution_trace
    | CallFailed of Contract.t * error list

  type call_result = Call of trace_result | Other

  module Repr = struct
    let pp ?(aliases = Client_context.Accounts.empty) ppf = function
      | OriginationResult s ->
          Format.fprintf ppf "Originated contracts: " ;
          List.iter
            (fun ct -> Format.fprintf ppf "%a@." Alpha_context.Contract.pp ct)
            s.originated_contracts
      | TransactionResult { balance_updates; call_info; _ } -> (
          List.iter
            (fun (balance, update) ->
              let open Alpha_context.Delegate in
              let addr =
                match balance with
                | Contract c ->
                    Format.asprintf "%a" (Client_context.Accounts.pp aliases) c
                | _ -> "Not yet implemented [rewards repr]"
              in
              let update =
                match update with
                | Credited tez -> "+" ^ Alpha_context.Tez.to_string tez
                | Debited tez -> "-" ^ Alpha_context.Tez.to_string tez
              in
              Format.fprintf ppf "%s %s@." addr update)
            balance_updates ;
          match call_info with
          | None -> ()
          | Some { storage; _ } ->
              Format.fprintf
                ppf
                "New storage: %a@."
                Michelson_v1_printer.print_expr
                storage )
      | _ -> Format.fprintf ppf "Not yet implemented [result repr]"
  end

  module Encodings = struct
    open Data_encoding

    let reveal_result_encoding =
      obj2 (req "kind" (constant "reveal")) (req "consumed_gas" int64)

    let transaction_result_encoding =
      obj5
        (req "kind" (constant "transaction"))
        (req "balance_updates" Alpha_context.Delegate.balance_updates_encoding)
        (req "consumed_gas" int64)
        (req "paid_storage_size_diff" int64)
        (req "storage_size" int64)

    let origination_result_encoding =
      obj6
        (req "kind" (constant "origination"))
        (req "balance_updates" Alpha_context.Delegate.balance_updates_encoding)
        (req "originated_contracts" @@ list Alpha_context.Contract.encoding)
        (req "consumed_gas" int64)
        (req "storage_size" int64)
        (req "paid_storage_size_diff" int64)

    let delegation_result_encoding =
      obj2 (req "kind" (constant "delegation")) (req "consumed_gas" int64)
  end
end

module Snapshot = struct
  type status = Success of OperationResult.t | Failed of error list | Skipped

  type operation_snapshot =
    | Traced of OperationResult.trace_result
    | UntraceableOp of status

  type t = { op : Alpha_context.packed_internal_operation; status : status }

  let originated_contracts t =
    match t.status with
    | Success (OriginationResult { originated_contracts; _ }) ->
        return originated_contracts
    | Success _ -> failwith "%s" "not an origination"
    | Failed errs -> Lwt.return @@ Error errs
    | Skipped -> failwith "%s" "None originated, skipped operation"

  let balance_updates t =
    match t.status with
    | Success (TransactionResult { balance_updates; _ }) -> Some balance_updates
    | _ -> None

  module Encodings = struct
    open Data_encoding

    let skipped_encoding = obj1 (req "status" (constant "skipped"))

    let failed_encoding =
      obj2 (req "status" (constant "failed")) (req "errs" string)
  end

  module Repr = struct
    let pp ?(aliases = Client_context.Account_map.empty) ppf t =
      match t.status with
      | Success t ->
          Format.fprintf ppf "%a" (OperationResult.Repr.pp ~aliases) t
      | Skipped -> Format.fprintf ppf "skipped"
      | Failed errs -> Format.printf "%a" Error_monad.pp_print_error errs
  end
end

module History = struct
  type t = Snapshot.t list

  let balance_updates t =
    List.map Snapshot.balance_updates t
    |> List.filter_map (fun i -> i)
    |> List.flatten
end

module StepConstants = struct
  type raw =
    { source : string;
      payer : string;
      self : string;
      amount : string;
      chain_id : string
    }

  type t = Proto.Script_interpreter.step_constants

  (* Taken from protocol-006-PsCARTHA *)
  (* { source : Alpha_context.Contract.t;
       payer : Alpha_context.Contract.t;
       self : Alpha_context.Contract.t;
       amount : Alpha_context.Tez.t;
       chain_id : Chain_id.t
     } *)

  type error += Not_an_int of string

  let () =
    register_error_kind
      `Permanent
      ~id:"int_of_string"
      ~title:"Data was given in a wrong format: Not an int."
      ~description:"A piece of data was not an int."
      ~pp:(fun ppf s ->
        Format.fprintf ppf "Wrong data type: %s is not an int" s)
      (* Data_encoding.(obj2 (req "point" point_encoding) (req "sequence" string)) *)
      Data_encoding.(obj1 (req "value" string))
      (function Not_an_int s -> Some s | _ -> None)
      (fun s -> Not_an_int s)

  let lwt_of_opt f value =
    match f value with Some x -> return x | None -> Lwt.return @@ Error []

  let parse_amount amount =
    lwt_of_opt Alpha_context.Tez.of_mutez @@ Int64.of_int amount

  let int_of_string i =
    match int_of_string i with
    | exception Failure _ -> Lwt.return @@ Error_monad.error (Not_an_int i)
    | s -> return s

  let tez_of_string amount = int_of_string amount >>=? fun i -> parse_amount i

  let parse_chain_id chain_id = lwt_of_opt Chain_id.of_string_opt chain_id

  let make_raw source payer self amount chain_id =
    let x : raw = { source; payer; self; amount; chain_id } in
    x

  (* (function
       | Invalid_utf8_sequence (point, str) -> Some (point, str) | _ -> None)
     (fun (point, str) -> Invalid_utf8_sequence (point, str)) ; *)

  let cook (raw : raw) =
    Contract.parse_addr raw.source >>=? fun source ->
    Contract.parse_addr raw.payer >>=? fun payer ->
    Contract.parse_addr raw.self >>=? fun self ->
    tez_of_string raw.amount >>=? fun amount ->
    (* parse_amount raw.amount >>=? fun amount -> *)
    parse_chain_id raw.chain_id >>=? fun chain_id ->
    return ({ source; payer; self; amount; chain_id } : t)
end
