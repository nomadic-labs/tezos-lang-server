open Proto

let parse_timestamp timestamp =
  match timestamp with
  | "now" -> Time.System.to_protocol (Tezos_stdlib_unix.Systime_os.now ())
  | s -> (
      match Time.Protocol.of_notation s with
      | Some stamp -> stamp
      | None -> Stdlib.failwith "not a correct timestamp" )
