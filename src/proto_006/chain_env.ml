module Proto = Tezos_protocol_006_PsCARTHA.Protocol
open Models

let genesis_block =
  Block_hash.of_b58check_exn
    "BLockGenesisGenesisGenesisGenesisGenesisf79b5d1CoW2"

let protocol_param_key = ["protocol_parameters"]

let bootstrap_balances = Array.make 5 10000000L

let make_accounts bootstrap_balances =
  let open Proto.Parameters_repr in
  [ { public_key_hash =
        Signature.Public_key_hash.of_b58check_exn
          "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx";
      public_key =
        Some
          (Signature.Public_key.of_b58check_exn
             "edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav");
      amount = Proto.Tez_repr.of_mutez_exn bootstrap_balances.(0)
    };
    { public_key_hash =
        Signature.Public_key_hash.of_b58check_exn
          "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN";
      public_key =
        Some
          (Signature.Public_key.of_b58check_exn
             "edpktzNbDAUjUk697W7gYg2CRuBQjyPxbEg8dLccYYwKSKvkPvjtV9");
      amount = Proto.Tez_repr.of_mutez_exn bootstrap_balances.(1)
    };
    { public_key_hash =
        Signature.Public_key_hash.of_b58check_exn
          "tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU";
      public_key =
        Some
          (Signature.Public_key.of_b58check_exn
             "edpkuTXkJDGcFd5nh6VvMz8phXxU3Bi7h6hqgywNFi1vZTfQNnS1RV");
      amount = Proto.Tez_repr.of_mutez_exn bootstrap_balances.(2)
    };
    { public_key_hash =
        Signature.Public_key_hash.of_b58check_exn
          "tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv";
      public_key =
        Some
          (Signature.Public_key.of_b58check_exn
             "edpkuFrRoDSEbJYgxRtLx2ps82UdaYc1WwfS9sE11yhauZt5DgCHbU");
      amount = Proto.Tez_repr.of_mutez_exn bootstrap_balances.(3)
    };
    { public_key_hash =
        Signature.Public_key_hash.of_b58check_exn
          "tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv";
      public_key =
        Some
          (Signature.Public_key.of_b58check_exn
             "edpkv8EUUH68jmo3f7Um5PezmfGrRF24gnfLpH3sVNwJnV5bVCxL2n");
      amount = Proto.Tez_repr.of_mutez_exn 8_000_000_000L
    } ]

module Helpers = struct
  let mutez_of_int amount =
    match Proto.Tez_repr.of_mutez @@ Int64.of_int amount with
    | Some amount -> return amount
    (* This seems sketchy *)
    | None -> Lwt.return @@ Error []
end

let initial_context (scripts : Models.Contract.t list) boot_balances =
  let bootstrap_accounts = make_accounts boot_balances in
  let bootstrap_contract_of_script (script : Contract.t) =
    let bootstrap = List.hd bootstrap_accounts in
    Format.printf "%d@." script.balance ;
    Helpers.mutez_of_int script.balance >>=? fun amount ->
    let script =
      Data_encoding.Binary.to_bytes_exn Script.encoding script.script
      |> Data_encoding.Binary.of_bytes_exn Proto.Script_repr.encoding
    in
    return
    @@ Proto.Parameters_repr.
         { delegate = bootstrap.public_key_hash; amount; script }
  in
  map_s bootstrap_contract_of_script scripts >>=? fun bootstrap_contracts ->
  let constants =
    { Proto.Constants_repr.preserved_cycles = 5;
      blocks_per_cycle = 4096l;
      blocks_per_commitment = 32l;
      blocks_per_roll_snapshot = 256l;
      blocks_per_voting_period = 32768l;
      time_between_blocks = List.map Proto.Period_repr.of_seconds_exn [60L; 75L];
      endorsers_per_block = 32;
      hard_gas_limit_per_operation = Z.of_int 800_000;
      hard_gas_limit_per_block = Z.of_int 8_000_000;
      proof_of_work_threshold = Int64.(sub (shift_left 1L 46) 1L);
      tokens_per_roll = Proto.Tez_repr.(mul_exn one 8_000);
      michelson_maximum_type_size = 1000;
      seed_nonce_revelation_tip =
        ( match Proto.Tez_repr.(one /? 8L) with
        | Ok c -> c
        | Error _ -> assert false );
      origination_size = 257;
      block_security_deposit = Proto.Tez_repr.(mul_exn one 512);
      endorsement_security_deposit = Proto.Tez_repr.(mul_exn one 64);
      (* This is not correct, but this shouldn't matter in our context
         (We only trace contract execution) *)
      baking_reward_per_endorsement = [Proto.Tez_repr.zero];
      (* block_reward = Proto.Tez_repr.(mul_exn one 16); *)
      (* same here *)
      endorsement_reward = [];
      hard_storage_limit_per_operation = Z.of_int 60_000;
      cost_per_byte = Proto.Tez_repr.of_mutez_exn 1_000L;
      test_chain_duration = Int64.mul 32768L 60L;
      (* Those are bogus data - We should not need true values for our purposes *)
      quorum_min = Int32.zero;
      quorum_max = Int32.one;
      min_proposal_quorum = Int32.one;
      delay_per_missing_endorsement = Proto.Period_repr.one_minute;
      initial_endorsers = 0
    }
  in
  let dummy_shell_header =
    { Tezos_base.Block_header.level = 0l;
      predecessor = genesis_block;
      timestamp = Time.Protocol.epoch;
      fitness = Proto.Fitness_repr.from_int64 0L;
      operations_hash = Operation_list_list_hash.zero;
      proto_level = 0;
      validation_passes = 0;
      context = Context_hash.zero
    }
  in
  let json =
    Data_encoding.Json.construct
      Proto.Parameters_repr.encoding
      Proto.Parameters_repr.
        { bootstrap_accounts;
          bootstrap_contracts;
          commitments = [];
          constants;
          security_deposit_ramp_up_cycles = None;
          no_reward_cycles = None
        }
  in
  let proto_params =
    Data_encoding.Binary.to_bytes_exn Data_encoding.json json
  in
  Tezos_protocol_environment.Context.(
    set Memory_context.empty ["version"] (MBytes.of_string "genesis"))
  >>= fun ctxt ->
  Tezos_protocol_environment.Context.(set ctxt protocol_param_key proto_params)
  >>= fun ctxt ->
  Proto.Main.init ctxt dummy_shell_header >|= Proto.Environment.wrap_error
  >>=? fun { context; _ } -> return context

let prepare_context ~(contracts : Contract.t list) boot_balances ~timestamp =
  initial_context contracts boot_balances >>=? fun context ->
  let timestamp = Parsers.parse_timestamp timestamp in
  Proto.Alpha_context.prepare
    context
    ~level:0l
    ~timestamp
    ~predecessor_timestamp:timestamp
    ~fitness:[]
  >|= Proto.Environment.wrap_error
  >>=? fun context ->
  let ctxt =
    Proto.Alpha_context.Contract.init_origination_nonce
      context
      Operation_hash.zero
  in
  return ctxt

(* This is built only once, and the typechecker needs
   not to worry about oth contracts *)
let empty_context =
  prepare_context ~contracts:[] bootstrap_balances ~timestamp:"now"

let originate ~(contracts : Contract.unbuilt list) ~timestamp boot_balances =
  Contract.build contracts >>=? fun contracts ->
  prepare_context ~contracts ~timestamp boot_balances >>=? fun context ->
  return @@ (context, contracts)
