module Proto = Tezos_protocol_006_PsCARTHA.Protocol
module Context = Proto.Environment.Context
module Script = Proto.Alpha_context.Script
module Alpha_context = Proto.Alpha_context
module Gas = Alpha_context.Gas
module Delegate = Alpha_context.Delegate
module Fees = Alpha_context.Fees
module Script_ir_translator = Proto.Script_ir_translator
module Micheline_parser = Micheline_parser
module Michelson_v1_parser = Michelson_v1_parser
module Apply_results = Proto.Apply_results

module Environment = struct
  include Proto.Environment
end
