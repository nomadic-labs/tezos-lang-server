module Proto = Tezos_protocol_006_PsCARTHA.Protocol
module Json = Data_encoding.Json
open Models

module Encodings = struct
  open Data_encoding

  let location =
    obj2
      (req "location" Micheline_parser.location_encoding)
      (req "expanded" bool)

  let typestack = list string

  let typemap =
    list
    @@ obj3
         (req "location" location)
         (req "before" typestack)
         (req "after" typestack)

  let typecheck_res = obj2 (req "indented" string) (req "typemap" typemap)

  let trace_loc_token =
    list
    @@ obj3
         (req "location" int31)
         (req "gas" Proto.Alpha_context.Gas.encoding)
         (req "stack" (list (obj2 (req "item" string) (opt "annot" string))))

  let trace =
    list
    @@ obj3
         (req "location" location)
         (req "gas" Proto.Alpha_context.Gas.encoding)
         (req "stack" (list (obj2 (req "item" string) (opt "annot" string))))

  let exec_result =
    obj4
      (req "trace" trace)
      (req "expanded" string)
      (req
         "operations"
         (list Proto.Alpha_context.Operation.internal_operation_encoding))
      (req "gas_left" Proto.Alpha_context.Gas.encoding)

  let traced_encoding =
    obj3 (req "kind" (constant "trace")) (req "addr" string) (req "trace" trace)

  let trace_error =
    obj3
      (req "kind" (constant "trace error"))
      (req "addr" string)
      (req "msg" string)

  let snapshot =
    obj1
      ( req "pending_operations"
      @@ list Proto.Alpha_context.Operation.internal_operation_encoding )

  let param_storage_tuple = obj2 (req "storage" string) (req "parameter" string)

  let typecheck_output =
    obj2
      (req "typemap" typemap)
      (req "entrypoints" (list (obj2 (req "name" string) (req "ty" string))))
end

module JsonHelpers = struct
  let add_field id content = function
    | `O members -> `O ((id, content) :: members)
    | otherwise ->
        print_endline "not an object" ;
        otherwise
end

let value_of_expr expr =
  Format.asprintf "%a" Michelson_v1_printer.print_expr expr

let update_trace (loc, gas, stack) =
  let stack =
    List.map (fun (expr, annot) -> (value_of_expr expr, annot)) stack
  in
  (loc, gas, stack)

let rec update_location ast = function
  | `O members ->
      members
      |> List.map (function
             | ("location", token_json) ->
                 let token =
                   Data_encoding.Json.destruct Data_encoding.int31 token_json
                 in
                 let json =
                   Data_encoding.Json.construct
                     Encodings.location
                     (ParseTools.point_of_token ast token)
                 in
                 ("location", json)
             | otherwise -> otherwise)
      |> fun members -> `O members
  | `A json -> `A (List.map (update_location ast) json)
  | otherwise -> otherwise

let stringify_param param =
  let lazy_expr =
    Data_encoding.Json.destruct
      Proto.Alpha_context.Script.lazy_expr_encoding
      param
  in
  let expr =
    match Proto.Script_repr.force_decode lazy_expr with
    | Ok (expr, _) -> expr
    | _ -> assert false
  in
  let params = Format.asprintf "%a" Michelson_v1_printer.print_expr expr in
  Json.construct Data_encoding.string params

let stringify_entrypoint = function
  | `O members ->
      List.map
        (function
          | ("value", param) -> ("value", stringify_param param)
          | otherwise -> otherwise)
        members
      |> fun mem -> `O mem
  | otherwise -> otherwise

let stringify_param = function
  | ("parameters", params) -> ("parameters", stringify_entrypoint params)
  | otherwise -> otherwise

(* This is an operation *)
let stringify_param_op : Json.json -> Json.json = function
  | `O members ->
      members |> List.map stringify_param |> fun members -> `O members
  | otherwise -> otherwise

(* In operations list *)
let stringify_param_ops = function
  | `A ops -> `A (List.map stringify_param_op ops)
  | otherwise -> otherwise

let stringify_params : Json.json -> Json.json = function
  | `O members ->
      members
      |> List.map (function
             | ("operations", ops) -> ("operations", stringify_param_ops ops)
             | otherwise -> otherwise)
      |> fun members -> `O members
  | otherwise -> otherwise

let construct_trace ast trace =
  trace |> List.map update_trace
  |> Json.construct Encodings.trace_loc_token
  |> update_location ast
  |> Json.destruct Encodings.trace

let construct_exec_result ast trace operations expanded =
  let gas_left = (fun (_, gas, _) -> gas) (List.last_exn trace) in
  let trace = construct_trace ast trace in
  let exec_result =
    Json.construct Encodings.exec_result (trace, expanded, operations, gas_left)
  in
  (* exec_result *)
  stringify_params exec_result

let construct_call_info ~contracts call_info =
  let ast =
    List.find
      (fun (c : Contract.t) -> c.addr = call_info.OperationResult.addr)
      contracts
    |> fun c -> c.ast
  in
  let trace = construct_trace ast call_info.trace in
  Data_encoding.Json.construct Encodings.trace trace

let construct_operation_result ~contracts result =
  let open OperationResult in
  match result with
  | RevealResult res ->
      Data_encoding.Json.construct
        Encodings.reveal_result_encoding
        ((), res.consumed_gas)
  | DelegationResult res ->
      Data_encoding.Json.construct
        Encodings.delegation_result_encoding
        ((), res.consumed_gas)
  | OriginationResult res ->
      Data_encoding.Json.construct
        Encodings.origination_result_encoding
        ( (),
          res.balance_updates,
          res.originated_contracts,
          res.consumed_gas,
          res.storage_size,
          res.paid_storage_size_diff )
  | TransactionResult res -> (
      let obj =
        Data_encoding.Json.construct
          Encodings.transaction_result_encoding
          ( (),
            res.balance_updates,
            res.consumed_gas,
            res.paid_storage_size_diff,
            res.storage_size )
      in
      match res.call_info with
      | None -> obj
      | Some call_info ->
          let open Data_encoding in
          let trace = construct_call_info ~contracts call_info in
          let storage =
            Format.asprintf
              "%a"
              Michelson_v1_printer.print_expr
              call_info.storage
            |> Data_encoding.Json.construct Data_encoding.string
          in
          JsonHelpers.add_field "trace" trace obj
          |> JsonHelpers.add_field "storage" storage )

let construct_operation_snapshot ~contracts snapshot =
  let open Snapshot in
  let op =
    Data_encoding.Json.construct
      Alpha_context.Operation.internal_operation_encoding
      snapshot.op
    |> stringify_param_op
  in
  let status =
    match (snapshot.status : Snapshot.status) with
    | Skipped ->
        Data_encoding.Json.construct Snapshot.Encodings.skipped_encoding ()
        |> JsonHelpers.add_field
             "status"
             (Data_encoding.Json.construct Data_encoding.string "skipped")
    | Failed errs ->
        let json =
          List.map Error_monad.json_of_error errs
          |> List.fold_left (fun acc json -> json :: acc) []
          |> fun json -> `A json
        in
        let formatfun fmt =
          Michelson_v1_error_reporter.report_errors
            fmt
            ~details:false
            ~show_source:true
        in
        let msg =
          Format.asprintf "%a@." formatfun errs
          |> Data_encoding.Json.construct Data_encoding.string
        in
        `O [("errs", json); ("msg", msg)]
        |> JsonHelpers.add_field
             "status"
             (Data_encoding.Json.construct Data_encoding.string "failed")
    | Success result ->
        construct_operation_result ~contracts result
        |> JsonHelpers.add_field
             "status"
             (Data_encoding.Json.construct Data_encoding.string "success")
  in
  `O [("op", op); ("result", status)]

let construct_snapshot ~contracts (snapshot : Snapshot.t) =
  let current_op = construct_operation_snapshot ~contracts snapshot in
  `O [("receipt", current_op)]

let construct_history ~contracts (history : Snapshot.t list) =
  print_int @@ List.length history ;
  let open Data_encoding.Json in
  ( List.map (construct_snapshot ~contracts) (List.rev history) |> fun json ->
    `A json )
  |> fun json -> `O [("history", json)]

let update_typestack ast (token, (before, after)) =
  let location = ParseTools.point_of_token ast token in
  let f (expr, annot) =
    let expr = Format.asprintf "%a" Michelson_v1_printer.print_expr expr in
    let (pref, suf) = if List.length annot <> 0 then ("(", ")") else ("", "") in
    Format.asprintf "%s%s %s%s" pref (String.concat " " annot) expr suf
  in
  let before = List.map f before in
  let after = List.map f after in

  (location, before, after)

let update_typemap ast typemap = List.map (update_typestack ast) typemap

let update_typemap_json ast typemap =
  let typemap = update_typemap ast typemap in
  Data_encoding.Json.construct Encodings.typemap typemap

let typecheck_output ast typemap entrypoints =
  let typemap = List.map (update_typestack ast) typemap in
  Data_encoding.Json.construct Encodings.typecheck_output (typemap, entrypoints)

let report_err ast errl =
  Json.construct Data_encoding.string
  @@ Format.asprintf
       "%a"
       (Michelson_v1_error_reporter.report_errors
          ~details:false
          ~show_source:false
          ~parsed:ast)
       errl

let json_of_error ast err =
  let msg = report_err ast [err] in
  err |> Error_monad.json_of_error |> update_location ast |> function
  | `O members -> `O (("msg", msg) :: members)
  | otherwise -> otherwise

let json_of_errors ast errl : Json.json =
  let typemap =
    List.map
      (function
        | Proto.Environment.Ecoproto_error
            (Proto.Script_tc_errors.Ill_typed_contract (_, type_map)) ->
            Some type_map
        | _ -> None)
      errl
    |> List.filter (function Some _ -> true | None -> false)
    |> function
    | [Some typemap] -> typemap
    | _ ->
        print_endline "typemap not found" ;
        []
  in
  let msg = report_err ast errl in
  let typemap = update_typemap_json ast typemap in
  ( errl |> List.map (json_of_error ast) |> fun (errs : Json.json list) ->
    `A errs )
  |> fun json -> `O [("msg", msg); ("errors", json); ("typemap", typemap)]
