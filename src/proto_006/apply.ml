module Proto = Proto
module Alpha_context = Proto.Alpha_context
open Alpha_context
open Proto.Apply_results
open Proto.Environment.Error_monad
open Models
open OperationResult
include Proto.Apply
module Apply = Proto.Apply

(* lang-server FIXME: This is imported from the 005 
  apply wihout modifications. *)

let apply_manager_operation_content :
    type kind.
    Alpha_context.t ->
    Proto.Script_ir_translator.unparsing_mode ->
    payer:Alpha_context.Contract.t ->
    source:Alpha_context.Contract.t ->
    chain_id:Chain_id.t ->
    internal:bool ->
    kind manager_operation ->
    ( context
    * kind successful_manager_operation_result
    * packed_internal_operation list
    * OperationResult.t )
    tzresult
    Lwt.t =
 fun ctxt mode ~payer ~source ~chain_id ~internal operation ->
  let before_operation =
    (* This context is not used for backtracking. Only to compute
         gas consumption and originations for the operation result. *)
    ctxt
  in
  Alpha_context.Contract.must_exist ctxt source >>=? fun () ->
  Lwt.return (Gas.consume ctxt Proto.Michelson_v1_gas.Cost_of.manager_operation)
  >>=? fun ctxt ->
  match operation with
  | Reveal _ ->
      let snapshot_result =
        RevealResult
          { consumed_gas =
              Alpha_context.Gas.consumed ~since:before_operation ~until:ctxt
              |> Z.to_int64
          }
      in
      return
        (* No-op: action already performed by . *)
        ( ctxt,
          ( Reveal_result
              { consumed_gas =
                  Alpha_context.Gas.consumed ~since:before_operation ~until:ctxt
              }
            : kind successful_manager_operation_result ),
          [],
          snapshot_result )
  | Transaction { amount; parameters; destination; entrypoint } -> (
      Alpha_context.Contract.spend ctxt source amount >>=? fun ctxt ->
      ( match Alpha_context.Contract.is_implicit destination with
      | None -> return (ctxt, [], false)
      | Some _ -> (
          Alpha_context.Contract.allocated ctxt destination >>=? function
          | true -> return (ctxt, [], false)
          | false ->
              Alpha_context.Fees.origination_burn ctxt
              >>=? fun (ctxt, origination_burn) ->
              return
                ( ctxt,
                  [ ( Alpha_context.Delegate.Contract payer,
                      Alpha_context.Delegate.Debited origination_burn ) ],
                  true ) ) )
      >>=? fun (ctxt, maybe_burn_balance_update, allocated_destination_contract)
        ->
      Alpha_context.Contract.credit ctxt destination amount >>=? fun ctxt ->
      Alpha_context.Contract.get_script ctxt destination
      >>=? fun (ctxt, script) ->
      match script with
      | None ->
          ( ( match entrypoint with
            | "default" -> return ()
            | entrypoint ->
                fail (Proto.Script_tc_errors.No_such_entrypoint entrypoint) )
          >>=? fun () ->
            Script.force_decode ctxt parameters >>=? fun (arg, ctxt) ->
            (* see [note] *)
            (* [note]: for toplevel ops, cost is nil since the
                   lazy value has already been forced at precheck, so
                   we compute and consume the full cost again *)
            let cost_arg = Script.deserialized_cost arg in
            Lwt.return (Gas.consume ctxt cost_arg) >>=? fun ctxt ->
            match Micheline.root arg with
            | Prim (_, D_Unit, [], _) ->
                (* Allow [Unit] parameter to non-scripted contracts. *)
                return ctxt
            | _ ->
                fail
                  (Proto.Script_interpreter.Bad_contract_parameter destination)
          )
          >>=? fun ctxt ->
          let balance_updates =
            Delegate.cleanup_balance_updates
              ( [ (Delegate.Contract source, Delegate.Debited amount);
                  (Contract destination, Credited amount) ]
              @ maybe_burn_balance_update )
          in
          let consumed_gas = Gas.consumed ~since:before_operation ~until:ctxt in
          let result =
            Transaction_result
              { storage = None;
                big_map_diff = None;
                originated_contracts = [];
                balance_updates;
                consumed_gas;
                storage_size = Z.zero;
                paid_storage_size_diff = Z.zero;
                allocated_destination_contract
              }
          in
          let snapshot_result =
            OperationResult.TransactionResult
              { balance_updates;
                consumed_gas = Z.to_int64 consumed_gas;
                paid_storage_size_diff = Int64.zero;
                storage_size = Int64.zero;
                call_info = None
              }
          in
          return (ctxt, result, [], snapshot_result)
      | Some script ->
          Script.force_decode ctxt parameters >>=? fun (parameter, ctxt) ->
          let cost_parameter = Script.deserialized_cost parameter in
          Lwt.return (Gas.consume ctxt cost_parameter) >>=? fun ctxt ->
          let step_constants =
            let open Proto.Script_interpreter in
            { source; payer; self = destination; amount; chain_id }
          in
          Proto.Script_interpreter.trace
            ctxt
            mode
            step_constants
            ~script
            ~parameter
            ~entrypoint
          >>=? fun ( ({ ctxt; storage; big_map_diff; operations } as exec_res),
                     trace ) ->
          Alpha_context.Contract.update_script_storage
            ctxt
            destination
            storage
            big_map_diff
          >>=? fun ctxt ->
          Fees.record_paid_storage_space ctxt destination
          >>=? fun (ctxt, new_size, paid_storage_size_diff, fees) ->
          Alpha_context.Contract.originated_from_current_nonce
            ~since:before_operation
            ~until:ctxt
          >>=? fun originated_contracts ->
          let balance_updates =
            Delegate.cleanup_balance_updates
              [ (Contract payer, Debited fees);
                (Contract source, Debited amount);
                (Contract destination, Credited amount) ]
          in
          let consumed_gas = Gas.consumed ~since:before_operation ~until:ctxt in
          let result =
            Transaction_result
              { storage = Some storage;
                big_map_diff;
                originated_contracts;
                balance_updates;
                consumed_gas;
                storage_size = new_size;
                paid_storage_size_diff;
                allocated_destination_contract
              }
          in
          let call_info =
            OperationResult.
              { addr = destination |> Alpha_context.Contract.to_b58check;
                result = exec_res;
                storage;
                trace
              }
          in
          let snapshot_result =
            OperationResult.TransactionResult
              { balance_updates;
                consumed_gas = Z.to_int64 consumed_gas;
                paid_storage_size_diff = Z.to_int64 paid_storage_size_diff;
                storage_size = Z.to_int64 new_size;
                call_info = Some call_info
              }
          in
          return (ctxt, result, operations, snapshot_result)
      (* type Origination = {
           delegate : public_key_hash option;
           script : Script.t;
           credit : Tez.t;
           preorigination : Contract.t option;
         } *) )
  | Origination { delegate; script; preorigination; credit } ->
      Script.force_decode ctxt script.storage
      >>=? fun (unparsed_storage, ctxt) ->
      (* see [note] *)
      Lwt.return (Gas.consume ctxt (Script.deserialized_cost unparsed_storage))
      >>=? fun ctxt ->
      Script.force_decode ctxt script.code >>=? fun (unparsed_code, ctxt) ->
      (* see [note] *)
      Lwt.return (Gas.consume ctxt (Script.deserialized_cost unparsed_code))
      >>=? fun ctxt ->
      Proto.Script_ir_translator.parse_script ctxt ~legacy:false script
      >>=? fun (Ex_script parsed_script, ctxt) ->
      Proto.Script_ir_translator.collect_big_maps
        ctxt
        parsed_script.storage_type
        parsed_script.storage
      >>=? fun (to_duplicate, ctxt) ->
      let to_update = Proto.Script_ir_translator.no_big_map_id in
      Proto.Script_ir_translator.extract_big_map_diff
        ctxt
        Optimized
        parsed_script.storage_type
        parsed_script.storage
        ~to_duplicate
        ~to_update
        ~temporary:false
      >>=? fun (storage, big_map_diff, ctxt) ->
      Proto.Script_ir_translator.unparse_data
        ctxt
        Optimized
        parsed_script.storage_type
        storage
      >>=? fun (storage, ctxt) ->
      let storage = Script.lazy_expr (Micheline.strip_locations storage) in
      let script = { script with storage } in
      Alpha_context.Contract.spend ctxt source credit >>=? fun ctxt ->
      ( match preorigination with
      | Some contract ->
          assert internal ;
          (* The preorigination field is only used to early return
                 the address of an originated contract in Michelson.
                 It cannot come from the outside. *)
          return (ctxt, contract)
      | None -> Alpha_context.Contract.fresh_contract_from_current_nonce ctxt )
      >>=? fun (ctxt, contract) ->
      Alpha_context.Contract.originate
        ctxt
        contract
        ~delegate
        ~balance:credit
        ~script:(script, big_map_diff)
      >>=? fun ctxt ->
      Fees.origination_burn ctxt >>=? fun (ctxt, origination_burn) ->
      Fees.record_paid_storage_space ctxt contract
      >>=? fun (ctxt, size, paid_storage_size_diff, fees) ->
      let balance_updates =
        Delegate.cleanup_balance_updates
          [ (Contract payer, Debited fees);
            (Contract payer, Debited origination_burn);
            (Contract source, Debited credit);
            (Contract contract, Credited credit) ]
      in
      let result =
        Origination_result
          { big_map_diff;
            balance_updates;
            originated_contracts = [contract];
            consumed_gas = Gas.consumed ~since:before_operation ~until:ctxt;
            storage_size = size;
            paid_storage_size_diff
          }
      in
      let snapshost_result =
        OriginationResult
          { (* FIXME: add big_map_diff *)
            balance_updates;
            originated_contracts = [contract];
            consumed_gas =
              Gas.consumed ~since:before_operation ~until:ctxt |> Z.to_int64;
            storage_size = size |> Z.to_int64;
            paid_storage_size_diff = paid_storage_size_diff |> Z.to_int64
          }
      in
      return (ctxt, result, [], snapshost_result)
  | Delegation delegate ->
      Delegate.set ctxt source delegate >>=? fun ctxt ->
      let snapshot_result =
        DelegationResult
          { consumed_gas =
              Alpha_context.Gas.consumed ~since:before_operation ~until:ctxt
              |> Z.to_int64
          }
      in
      return
        ( ctxt,
          Delegation_result
            { consumed_gas = Gas.consumed ~since:before_operation ~until:ctxt },
          [],
          snapshot_result )

let apply_internal_manager_operations ctxt mode ~payer ~chain_id ops ~history =
  let rec apply ctxt applied worklist =
    let open Proto.Apply_results in
    let open Snapshot in
    match worklist with
    | [] -> return (ctxt, history)
    | (Internal_operation ({ source; operation; nonce } as op) as packed)
      :: rest -> (
        ( if internal_nonce_already_recorded ctxt nonce then
          fail (Internal_operation_replay (Internal_operation op))
        else
          let ctxt = record_internal_nonce ctxt nonce in
          let ctxt = Fees.start_counting_storage_fees ctxt in
          let t =
            apply_manager_operation_content
              ctxt
              mode
              ~source
              ~payer
              ~chain_id
              ~internal:true
              operation
          in
          t >>=? fun (ctxt, result, emitted, snapres) ->
          Fees.burn_storage_fees ctxt ~storage_limit:(Z.of_int 3000) ~payer
          >>=? fun ctxt -> return (ctxt, result, emitted, snapres) )
        >>= function
        | Error errors ->
            let errors =
              List.map (fun err -> Proto.Environment.Ecoproto_error err) errors
            in
            let rec skipped_loop = function
              | [] -> ()
              | packed :: tl ->
                  history := { op = packed; status = Skipped } :: !history ;
                  skipped_loop tl
            in
            history := { op = packed; status = Failed errors } :: !history ;
            skipped_loop rest ;
            return (ctxt, history)
        | Ok (ctxt, result, emitted, snapres) ->
            let pending_operations = rest @ emitted in
            history := { op = packed; status = Success snapres } :: !history ;
            apply
              ctxt
              (Internal_operation_result (op, Applied result) :: applied)
              pending_operations )
  in
  apply ctxt [] ops
