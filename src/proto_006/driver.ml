(* This is a way to call those module from outside the library *)
module ParseTools = ParseTools
module Michelson_v1_parser = Michelson_v1_parser
module Michelson_v1_printer = Michelson_v1_printer
module Michelson_primitives = Proto.Proto.Michelson_v1_primitives
module Michelson_v1_error_reporter = Michelson_v1_error_reporter
module Michelson_v1_json = Michelson_v1_json
module Proto = Proto
module Env_em = Proto.Environment.Error_monad
open Client_context
open Models

let ( >>=?$ ) = Proto.Environment.Error_monad.( >>=? )

let expand_expr ~expr =
  ( match Michelson_v1_parser.parse_expression expr with
  | (param, []) -> Error_monad.return param.expanded
  | (_, errl) -> Error_monad.fail @@ List.hd errl )
  >>=? fun expr ->
  return @@ Format.asprintf "%a" Michelson_v1_printer.print_expr expr

let indent ~script =
  let parsed = Michelson_v1_parser.parse_toplevel script in
  let ast = fst parsed in
  let unexpanded = ast.unexpanded in
  let indented =
    Format.asprintf "%a" Micheline_printer.print_expr
    @@ Micheline_printer.printable
         ~comment:(fun _ -> None)
         (fun token -> token)
         unexpanded
  in
  (* Need to unwrap the toplevel code*)
  String.split_on_char '\n' indented
  |> List.map (fun s -> String.sub s 2 (String.length s - 2))
  |> String.concat "\n"
  |> (fun s -> String.sub s 0 (String.length s - 2))
  |> return

(* Is a snapshot between two operations, or during one? *)
(* The answer is after *)

let apply ~context ~operation ~payer ~history ~chain_id =
  let open OperationResult in
  Apply.apply_internal_manager_operations
    context
    Proto.Script_ir_translator.Readable
    ~payer
    [operation]
    ~history
    ~chain_id
  >>=?$ fun (context, history) -> Env_em.return (context, !history)

let loop ~context ~contracts ~operations ~payer ~chain_id ~history =
  let open Snapshot in
  let open OperationResult in
  match operations with
  | [] -> return (contracts, context, !history)
  | [operation] ->
      (* Apply transaction here *)
      apply ~context ~operation ~payer ~chain_id ~history
      >|= Proto.Environment.wrap_error
      >>=? fun (context, history) -> return (contracts, context, history)
  | _ :: _ :: _ -> failwith "Multi operations are not supported yet"

let ( >>=| ) = Lwt.( >>= )

module Contrs = struct
  let create_account ~funds ~context =
    let addr = Gen.gen_addr () in
    let contract = Alpha_context.Contract.implicit_contract addr in
    StepConstants.tez_of_string funds >>=? fun balance ->
    Alpha_context.Contract.credit context contract balance
    >|= Proto.Environment.wrap_error
    >>=? fun ctx -> return (addr, ctx)

  let originate ~filename ~storage ~amount ~context =
    let code = Extdlib.File.read filename in
    Lwt.return
    @@ Alpha_context.Contract.of_b58check "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
    >|= Proto.Environment.wrap_error
    >>=? fun payer ->
    Operations.Origination.build ~code ~storage ~amount ~context
    >>=? fun (context, internal_op) ->
    apply
      ~context
      ~operation:internal_op
      ~payer
      ~chain_id:Chain_id.zero
      ~history:(ref [])
    >|= Proto.Environment.wrap_error

  module Cli = struct
    let create_account ~name ~funds ~context =
      create_account ~funds ~context >>=? fun (addr, context) ->
      let addr = Format.asprintf "%a" Signature.Public_key_hash.pp addr in
      Format.printf
        "%s is now registered as %s - credited %s tez.@."
        addr
        name
        (* Transfer to tez *)
        (Extdlib.String.remove_last_n funds 6) ;
      return (context, addr)

    let originate ~filename ~alias ~storage ~amount ~cctx =
      originate ~filename ~storage ~amount ~context:cctx.alpha
      >>=? fun (alpha, res) ->
      ( match res with
      | [snapshot] -> Snapshot.originated_contracts snapshot
      | _ -> failwith "%s" "More or less than one operation" )
      >>=? function
      | [addr] ->
          let addr = Format.asprintf "%a" Alpha_context.Contract.pp addr in
          Format.printf "contract %s originated at %s@." alias addr ;
          Client_context.add_account ~name:alias ~addr cctx >>=? fun cctx ->
          return { cctx with alpha }
      | _ -> failwith "%s" "More than one contract"

    let print_contracts ~cctx contracts =
      let open PrintBox in
      Env_em.map_s
        (fun ct ->
          Alpha_context.Contract.get_balance cctx.alpha ct >>=?$ fun balance ->
          let addr =
            Format.asprintf "%a" (Client_context.Accounts.pp cctx.accounts) ct
          in
          let balance = Format.asprintf "%a" Alpha_context.Tez.pp balance in
          let alias =
            Option.unopt_map
              ~f:(fun i -> i)
              ~default:""
              (Client_context.has_alias_opt ~t:cctx addr)
          in
          Env_em.return [alias; addr; balance])
        contracts
      >>=?$ fun grid ->
      let grid =
        grid_text_l ~pad:(fun v -> hpad 1 v |> center_hv) ~bars:false grid
        |> frame
      in

      Format.printf "%a@." PrintBox_text.pp grid ;
      Env_em.return_unit

    let print_accounts_proto ~cctx =
      Alpha_context.Contract.list cctx.alpha >>=| fun contracts ->
      print_contracts ~cctx contracts >>=?$ fun () -> Env_em.return cctx

    let print_accounts ~cctx =
      print_accounts_proto ~cctx >|= Proto.Environment.wrap_error >>=? fun _ ->
      return cctx
  end
end

module Timestamp = struct
  let set_timestamp ~context ~timestamp =
    let timestamp = Parsers.parse_timestamp timestamp in
    let context = ContextHelpers.to_raw_context ~context in
    let context =
      { context with predecessor_timestamp = timestamp; timestamp }
    in
    let context = ContextHelpers.to_alpha ~context in
    return context

  module Repr = struct
    let print_now ~context =
      let stamp = Alpha_context.Timestamp.current context in
      Alpha_context.Timestamp.to_notation stamp |> print_endline
  end

  module Cli = struct
    let set_timestamp ~context ~timestamp =
      set_timestamp ~context ~timestamp >>=? fun context ->
      Format.printf "timestamp set to %s" timestamp ;
      return context
  end
end

module Op = struct
  let transfer ~parameter ~amount ~source ~destination ~entrypoint ~context =
    Contract.parse_addr source >>=? fun source ->
    Contract.parse_addr destination >>=? fun destination ->
    Operations.Transaction.build_internal_operation
      ~amount
      ~source
      ~parameter
      ~destination
      ~entrypoint
      ~context
    >>=? fun (context, internal_op) ->
    apply
      ~context
      ~operation:internal_op
      ~payer:source
      ~chain_id:Chain_id.zero
      ~history:(ref [])
    >|= Proto.Environment.wrap_error

  module Cli = struct
    let transfer ~parameter ~amount ~source ~destination ~entrypoint ~cctx =
      let addr_source = Client_context.replace_if_alias ~addr:source cctx in
      let addr_dest = Client_context.replace_if_alias ~addr:destination cctx in
      transfer
        ~parameter
        ~amount
        ~source:addr_source
        ~destination:addr_dest
        ~entrypoint
        ~context:cctx.alpha
      >>=? fun (context, res) ->
      ( match res with
      | { status = Snapshot.Failed errs; _ } :: _ -> Lwt.return @@ Error errs
      | { status = Snapshot.Skipped; _ } :: _ -> failwith "skipped operation"
      | _ -> return res )
      >>=? fun res ->
      List.iter
        (Format.printf "%a@." (Snapshot.Repr.pp ~aliases:cctx.accounts))
        res ;
      let balance_updates = History.balance_updates res in
      let updates =
        List.map
          (fun (balance, update) ->
            let open Alpha_context.Delegate in
            let addr =
              match balance with
              | Contract c ->
                  Format.asprintf
                    "%a"
                    (Client_context.Accounts.pp cctx.accounts)
                    c
              | _ -> "nyi"
            in
            let update =
              match update with
              | Credited tez -> "+" ^ Alpha_context.Tez.to_string tez
              | Debited tez -> Format.asprintf "-%a" Alpha_context.Tez.pp tez
            in
            [addr; update])
          balance_updates
      in
      Format.printf
        "%a@."
        PrintBox_text.pp
        PrintBox.(
          grid_text_l ~pad:(fun v -> hpad 1 v |> center_hv) ~bars:false updates
          |> frame) ;
      return @@ Client_context.update context cctx
  end
end

let entrypoint ~(contracts : (string * string * string * int) list)
    ~(params : string * int * int * string * string * string) ~boot_balances
    ~timestamp =
  let history = ref [] in
  let contracts = List.map Contract.unbuilt_of_tuple contracts in
  (let params = Operations.Transaction.unbuilt_of_tuple params in
   Operations.Transaction.build contracts params
   (* This is the loop here *)
   >>=? fun transaction ->
   Chain_env.originate
     ~contracts
     (Array.map Int64.of_int boot_balances)
     ~timestamp
   >>=? fun (context, contracts) ->
   let gas =
     if params.gas < 8_000_000 then Z.of_int params.gas else Z.of_int 8_000_000
   in
   let context = Proto.Alpha_context.Gas.set_limit context gas in
   Lwt.return @@ Proto.Alpha_context.fresh_internal_nonce context
   >|= Proto.Environment.wrap_error
   >>=? fun (context, nonce) ->
   let internal_op =
     Operations.Transaction.to_internal_operation transaction nonce
   in
   loop
     ~context
     ~contracts
     ~operations:[internal_op]
     ~payer:transaction.payer
     ~chain_id:Chain_id.zero
     ~history)
  (* Hypothesis is that the driver should return here. *)
  >>= function
  | Ok (contracts, _, history) ->
      return @@ Michelson_v1_json.construct_history ~contracts history
  | Error errs ->
      List.iter (Format.printf "%a@." Error_monad.pp) errs ;
      let errs = List.map Error_monad.json_of_error errs in
      return @@ `A errs

let json_of_contract ~script =
  Michelson_v1_parser.parse_toplevel ~check:true script
  |> fst
  |> (fun { unexpanded; _ } -> unexpanded)
  |> Data_encoding.Json.construct
       (Micheline.canonical_encoding ~variant:"test" Data_encoding.string)
  |> return

(* Interface *)
let typecheck = Typecheck.typecheck

let typecheck_code_json = Typecheck.typecheck_code_json

let run ~storage ~parameter ~filename ~sender ~payer ~entrypoint ~self ~amount
    ~timestamp ~context =
  let _timestamp = Parsers.parse_timestamp timestamp in
  let code = Extdlib.File.read filename in
  let chain_id = Chain_id.to_string @@ Chain_id.zero in
  let step_constants =
    StepConstants.make_raw sender payer self amount chain_id
  in
  StepConstants.cook step_constants >>=? fun step_constants ->
  Run.run step_constants ~code ~storage ~parameter ~entrypoint ~context
  >>=? fun (ctxt, effect) ->
  print_endline @@ Shared_models.Models.RunEffect.Repr.pretty effect ;
  return () >>= function
  | Ok _ -> return ctxt
  | Error errs ->
      List.iter (Format.printf "%a@." Error_monad.pp) errs ;
      return ctxt

module Gen = struct
  let gen_val ty =
    Gen.gen_val ~ty >>=? fun value ->
    print_endline value ;
    return ()

  let gen_rand_val ty =
    Chain_env.empty_context >>=? fun context ->
    Gen.gen_rand_val ~ty ~context >>=? fun value ->
    print_endline value ;
    return ()

  let print_storage storage =
    print_endline "-- Storage --" ;
    print_endline storage

  let print_param param =
    print_endline "-- Parameter --" ;
    print_endline "@default:" ;
    print_endline param

  let gen_attributes ~filename =
    let code = Extdlib.File.read filename in
    Typecheck.get_entrypoints ~ast:(Typecheck.parse ~script:code)
    >>=? fun entrypoints ->
    let entrypoints =
      List.filter (function ("default", _) -> false | _ -> true) entrypoints
    in
    Gen.gen_param_storage ~code >>=? fun (storage, parameter) ->
    print_storage storage ;
    print_param parameter ;
    Error_monad.iter_s
      (fun (name, ty) ->
        Gen.gen_val ~ty >>=? fun value ->
        Format.printf "@%s:\n%s@." name value ;
        (* Format.print_flush () ; *)
        return_unit)
      entrypoints

  let gen_arg ~filename ~entrypoint =
    let code = Extdlib.File.read filename in
    match entrypoint with
    | None ->
        Gen.gen_param_storage ~code >>=? fun (_, parameter) ->
        print_param parameter ;
        return_unit
    | Some entrypoint -> (
        Typecheck.typecheck_code ~script:code >>=? fun (_, entrypoints) ->
        match List.assoc_opt entrypoint entrypoints with
        | None -> failwith "Entrypoint '%s' could not be found" entrypoint
        | Some ty -> gen_val ty )

  let gen_attributes_json ~code =
    Gen.gen_param_storage ~code >>=? fun (storage, parameter) ->
    return
    @@ Data_encoding.Json.construct
         Michelson_v1_json.Encodings.param_storage_tuple
         (storage, parameter)
end

module Serial = struct
  let write_context ~(context : Alpha_context.t) ~filename =
    let context = ContextHelpers.to_raw_context ~context in
    let json =
      Data_encoding.Json.construct Memory_context.encoding context.context
    in
    let contents = Json.to_string json in
    let filename =
      match filename with
      | Some filename -> filename
      | None -> Format.asprintf "/tmp/context-%x" (Hashtbl.hash contents)
    in
    Extdlib.File.write ~filename ~contents ;
    return filename

  let load_context ~filename =
    let json = Extdlib.File.read filename in
    ( match Json.parse json with
    | Ok json -> return json
    | Error err -> Lwt.return @@ Error_monad.generic_error "%s" err )
    >>=? fun json ->
    let context = Data_encoding.Json.destruct Memory_context.encoding json in
    ContextHelpers.alpha_of_context ~alpha:context

  module Cli = struct
    let write_context ~context ~filename =
      write_context ~context ~filename >>=? fun filename ->
      Format.printf "Context written at %s@." filename ;
      return_unit

    let load_context ~filename =
      load_context ~filename >>=? fun context ->
      Format.printf "Context loaded from %s@." filename ;
      return context

    let write_cctx ~cctx ~filename =
      Client_context.Serial.write ~cctx ~filename >>=? fun filename ->
      Format.printf "context written at %s@." filename ;
      return_unit

    let load_cctx ~filename =
      Client_context.Serial.load ~filename >>=? fun cctx -> return cctx
  end
end

let print_micheline ~filename =
  let open Micheline in
  let code = Extdlib.File.read filename in
  let parsed = Typecheck.parse ~script:code in
  let json = Json.construct Proto.Script_repr.expr_encoding parsed.expanded in
  Format.printf "%a" Data_encoding.Json.pp json ;
  return_unit

let gen_attributes_json = Gen.gen_attributes_json

let gen_val = Gen.gen_val

let gen_rand_val = Gen.gen_rand_val
