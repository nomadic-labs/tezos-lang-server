open Proto

let trace ctxt ~step_constants ~script ~parameter ~entrypoint =
  Proto.Script_interpreter.trace
    ctxt
    step_constants
    ~script
    ~parameter
    ~entrypoint

let run step_constants ~code ~storage ~parameter ~entrypoint ~context =
  Models.Contract.parse_script code storage >>=? fun script ->
  ParseTools.parse_parameter parameter >>=? fun parameter ->
  let gas_alloted = Z.of_string "2000000" in
  let ctxt = Alpha_context.Gas.set_limit context gas_alloted in
  Proto.Script_interpreter.trace
    ctxt
    Script_ir_translator.Readable
    step_constants
    ~script
    ~entrypoint
    ~parameter
  >|= Proto.Environment.wrap_error
  >>=? fun ({ storage; ctxt; _ }, trace) ->
  return @@ (ctxt, Translator.trace_result storage trace gas_alloted)
