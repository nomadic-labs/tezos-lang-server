open Proto
module Int_set = Set.Make (Compare.Int)

type context =
  { context : Context.t;
    constants : Proto.Constants_repr.parametric;
    first_level : Proto.Raw_level_repr.t;
    level : Proto.Level_repr.t;
    predecessor_timestamp : Proto.Environment.Time.t;
    timestamp : Proto.Environment.Time.t;
    fitness : Int64.t;
    deposits : Proto.Tez_repr.t Signature.Public_key_hash.Map.t;
    included_endorsements : int;
    allowed_endorsements :
      (Signature.Public_key.t * int list * bool) Signature.Public_key_hash.Map.t;
    fees : Proto.Tez_repr.t;
    rewards : Proto.Tez_repr.t;
    block_gas : Z.t;
    operation_gas : Proto.Gas_limit_repr.t;
    internal_gas : Proto.Gas_limit_repr.internal_gas;
    storage_space_to_pay : Z.t option;
    allocated_contracts : int option;
    origination_nonce : Proto.Contract_repr.origination_nonce option;
    temporary_big_map : Z.t;
    internal_nonce : int;
    internal_nonces_used : Int_set.t
  }

let to_raw_context ~context =
  let context : context = Obj.magic context in
  context

let to_alpha ~context =
  let context : Alpha_context.t = Obj.magic context in
  context

let alpha_of_context ~alpha =
  let timestamp = Parsers.parse_timestamp "now" in
  Proto.Alpha_context.prepare
    alpha
    ~level:0l
    ~timestamp
    ~predecessor_timestamp:timestamp
    ~fitness:[]
  >|= Proto.Environment.wrap_error
  >>=? fun context ->
  let context =
    Proto.Alpha_context.Contract.init_origination_nonce
      context
      Operation_hash.zero
  in
  return context
