open Shared_models
module Gas = Proto.Alpha_context.Gas

module Encodings = struct
  open Data_encoding

  let trace =
    list
    @@ obj2
         (req "location" int31)
         (req "stack" (list (obj2 (req "item" string) (opt "annot" string))))
end

let trace_result storage trace gas_alloted =
  let storage = Format.asprintf "%a" Michelson_v1_printer.print_expr storage in
  (* We want to do an encode decode here *)
  let (_, gas_left, _) = List.last_exn trace in
  let gas_left =
    match gas_left with
    | Gas.Unaccounted -> Z.zero
    | Limited { remaining } -> remaining
  in
  let gas = Z.sub gas_alloted gas_left |> Z.to_string in
  Models.RunEffect.{ storage; gas }

let run_effect storage =
  let storage = Format.asprintf "%a" Michelson_v1_printer.print_expr storage in
  Models.RunEffect.{ storage; gas = "Unaccounted" }
