open Proto
open Shared_models
open Models

let parse_parameter param =
  match Michelson_v1_parser.parse_expression param with
  | (param, []) -> return param.expanded
  | (_, errl) -> Lwt.return @@ Error errl

let string_of_prim = Proto.Michelson_v1_primitives.string_of_prim

let point_of_token ast token =
  let open Michelson_v1_parser in
  List.assoc_opt token ast.unexpansion_table
  |> Option.apply ~f:(fun unexp_token ->
         List.assoc_opt unexp_token ast.expansion_table)
  |> function
  | None -> (Micheline_parser.location_zero, true)
  | Some (location, deps) ->
      let last = List.last_exn deps in
      let expanded = last <> token in
      (location, expanded)

let getloc ast l = Location.Decode.from_micheline @@ point_of_token ast l

let tuple a b = (a, b)

module Repr = struct
  let expr ty =
    Format.asprintf "%a" Michelson_v1_printer.print_expr_unwrapped ty

  let prim prim = string_of_prim prim

  let rec stack_ty = function
    | [] -> "[]"
    | (ty, _annot) :: tl -> expr ty ^ " : " ^ stack_ty tl
end

let get_msg ast err =
  let open Proto.Script_tc_errors in
  let open Proto.Michelson_v1_primitives in
  match err with
  (* structure errors *)
  | Invalid_arity (loc, prim, expected, got) ->
      ( getloc ast loc,
        Format.asprintf
          "Primitive %s expects %d arguments but is given %d."
          (string_of_prim prim)
          expected
          got )
  | Invalid_primitive (loc, _, prim) ->
      ( getloc ast loc,
        Format.asprintf "%s takes additional parameters" (Repr.prim prim) )
  | Duplicate_field (loc, prim) ->
      (getloc ast loc, Format.asprintf "Duplicate field: %s" (Repr.prim prim))
  | Unexpected_big_map loc ->
      (getloc ast loc, Format.asprintf "Unexpected bigmap.")
  | Unexpected_operation loc ->
      (getloc ast loc, Format.asprintf "Unexpected operation")
  | Unexpected_contract loc ->
      (getloc ast loc, Format.asprintf "Unexpected contract")
  (* Instruction typing errors *)
  | Fail_not_in_tail_position loc ->
      (getloc ast loc, Format.asprintf "Fail not in tail position.")
  | Undefined_unop (loc, prim, _) ->
      (getloc ast loc, Format.asprintf "Undefined unop: %s" (Repr.prim prim))
  | Undefined_binop (loc, prim, _, _) ->
      (getloc ast loc, Format.asprintf "Undefined binop: %s" (Repr.prim prim))
  | Bad_stack (loc, prim, _, stack_ty) ->
      ( getloc ast loc,
        Format.asprintf
          "Bad stack for %s: %s"
          (Repr.prim prim)
          (Repr.stack_ty stack_ty) )
  | Unmatched_branches (loc, left, right) ->
      ( getloc ast loc,
        Format.asprintf
          "Unmatched branches: %s & %s"
          (Repr.stack_ty left)
          (Repr.stack_ty right) )
  | Self_in_lambda loc -> (getloc ast loc, Format.asprintf "Self in lambda.")
  | Inconsistent_type_annotations (loc, a, b) ->
      ( getloc ast loc,
        Format.asprintf
          "Annotation %s is incompatible with %s"
          (Repr.expr a)
          (Repr.expr b) )
  | Invalid_map_body (loc, stack) ->
      ( getloc ast loc,
        Format.asprintf "Invalid map body: %s" (Repr.stack_ty stack) )
  | Invalid_map_block_fail loc ->
      (getloc ast loc, Format.asprintf "Invalid map block fail.")
  | Invalid_iter_body (loc, a, b) ->
      ( getloc ast loc,
        Format.asprintf
          "Invalid iter body: %s | %s"
          (Repr.stack_ty a)
          (Repr.stack_ty b) )
  | Invalid_constant (loc, exp, ty) ->
      ( getloc ast loc,
        Format.asprintf
          "Value %s is invalid for type %s"
          (Repr.expr exp)
          (Repr.expr ty) )
  | Invalid_contract (loc, _contract) ->
      (getloc ast loc, Format.asprintf "Invalid contract.")
  | Invalid_big_map (loc, _id) ->
      (getloc ast loc, Format.asprintf "Invalid big map.")
  | Comparable_type_expected (loc, ty) ->
      ( getloc ast loc,
        Format.asprintf "Comparable type expected in lieu of %s" (Repr.expr ty)
      )
  | Unordered_map_keys (loc, expr) ->
      ( getloc ast loc,
        Format.asprintf
          "Keys in a map should be ordered, which is not the\n    case in %s"
          (Repr.expr expr) )
  | Unordered_set_values (loc, expr) ->
      ( getloc ast loc,
        Format.asprintf
          "Values in a set should be ordered, which is not the\n    case in %s"
          (Repr.expr expr) )
  | Duplicate_map_keys (loc, expr) ->
      ( getloc ast loc,
        Format.asprintf "Duplicate map keys in %s" (Repr.expr expr) )
  | Duplicate_set_values (loc, expr) ->
      ( getloc ast loc,
        Format.asprintf "Duplicate set values in %s" (Repr.expr expr) )
  | Unexpected_annotation loc ->
      (getloc ast loc, Format.asprintf "Unexpected annotations.")
  | Ungrouped_annotations loc ->
      (getloc ast loc, Format.asprintf "Ungrouped annotations.")
  | Bad_return (loc, got, _exp) ->
      ( getloc ast loc,
        Format.asprintf
          "Doesn't respect calling conventions: got %s@,"
          (Repr.stack_ty got) )
  | Invalid_primitive_name (_, loc) ->
      (getloc ast loc, Format.asprintf "Invalid primitive name")
  | err ->
      let msg =
        Format.asprintf
          "%a"
          (Michelson_v1_error_reporter.report_errors
             ~details:false
             ~show_source:false
             ~parsed:ast)
          [Proto.Environment.Ecoproto_error err]
      in
      (Location.max, msg)

(* | e -> Log.log  (Location.max , "Unknown error, please open an issue.") *)

module Priority = struct
  let prioritize err =
    let open Proto.Script_tc_errors in
    match err with Ill_typed_contract _ -> 1000 | _ -> 0

  let prioritize_errs errs =
    List.sort (fun a b -> compare (prioritize a) (prioritize b)) errs

  let prioritize_proto_errs errs =
    errs
    |> List.filter_map (function
           | Proto.Environment.Ecoproto_error err -> Some err
           | _ -> None)
    |> prioritize_errs
end
