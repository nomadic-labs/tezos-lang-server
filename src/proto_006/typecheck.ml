open Proto
open Models

let parse ~script =
  let parsed = Michelson_v1_parser.parse_toplevel ~check:true script in
  let ast = fst parsed in
  ast

let get_entrypoints ~(ast : Michelson_v1_parser.parsed) =
  Chain_env.empty_context >>=? fun context ->
  Lwt.return @@ Script_ir_translator.parse_toplevel ~legacy:false ast.expanded
  >|= Proto.Environment.wrap_error
  >>=? fun (arg_type, _, _, root_name) ->
  Lwt.return
  @@ Script_ir_translator.parse_ty
       context
       ~legacy:false
       ~allow_big_map:true
       ~allow_operation:false
       ~allow_contract:true
       arg_type
  >|= Proto.Environment.wrap_error
  >>=? fun (Ex_ty arg_type, _) ->
  Lwt.return
  @@ Script_ir_translator.list_entrypoints ~root_name arg_type context
  >|= Proto.Environment.wrap_error
  >>=? fun (_, entrypoint_map) ->
  let entrypoints =
    Script_ir_translator.Entrypoints_map.fold
      (fun name (_, ty) acc ->
        let stripped_ty = Micheline.strip_locations ty in
        let ty =
          Format.asprintf "%a" Michelson_v1_printer.print_expr stripped_ty
        in
        (name, ty) :: acc)
      entrypoint_map
      []
  in
  return entrypoints

let typecheck_from_ast ~ast =
  Chain_env.empty_context >>=? fun context ->
  Proto.Script_ir_translator.typecheck_code
    context
    ast.Michelson_v1_parser.expanded
  >|= Proto.Environment.wrap_error
  >>=? fun (typemap, _) ->
  get_entrypoints ~ast >>=? fun entrypoints -> return (typemap, entrypoints)

let typecheck_code ~script =
  let ast = parse ~script in
  typecheck_from_ast ~ast

let encode ast res =
  res >>= function
  | Error errors ->
      (* Format.printf "KO@."; *)
      let json = Michelson_v1_json.json_of_errors ast errors in
      return json
  | Ok (typemap, entrypoints) ->
      (* Format.printf "OK@."; *)
      let json = Michelson_v1_json.typecheck_output ast typemap entrypoints in
      return json

let typecheck_code_json ~script =
  let ast = parse ~script in
  typecheck_from_ast ~ast |> encode ast

let typecheck_storage ~script ~storage ~context =
  Contract.parse_script script storage >>=? fun script ->
  Proto.Script_ir_translator.parse_script ~legacy:false context script
  >|= Proto.Environment.wrap_error

let typecheck ~script ~storage =
  let ast = parse ~script in
  Chain_env.empty_context
  >>=? (fun context ->
         typecheck_storage ~script ~storage ~context >>=? fun _ ->
         typecheck_from_ast ~ast >>=? fun (typemap, entrypoints) ->
         return (typemap, entrypoints))
  |> encode ast
