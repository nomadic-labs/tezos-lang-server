module Proto = Tezos_protocol_006_PsCARTHA.Protocol

module Rand = struct
  let ascii_byte () : char =
    let (min, max) = (32, 126) in
    Char.chr @@ ((Random.int @@ (max - min)) + min)

  let gen_string ~size =
    let (min, max) = size in
    let len = (Random.int @@ (max - min)) + min in
    String.init len (fun _ -> ascii_byte ())
end

module Base_sampler : Randgen.Base_samplers_S = struct
  open Randgen

  let int = int

  let nat = nat

  let string = Rand.gen_string

  let bytes = bytes

  let signature _ =
    Signature.of_string_exn
      "edsigtkpiSSschcaCt9pUVrpNPf7TTcgvgDEDD6NCEHMy8NNQJCGnMfLZzYoQj74yLjo9wx6MPVV29CvVzgi7qEcEUok3k7AuMg"

  let tez = tez

  let timestamp = timestamp

  let bool = bool

  let key_pool_size = 100
end

type ret =
  | Leaf of string
  | Single of string * ret list * string
  | Branch of ret list

let rec to_string = function
  | Leaf str -> str
  | Single (pre, retl, suf) ->
      pre ^ (List.map to_string retl |> String.concat " ") ^ suf
  | Branch retl -> List.map to_string retl |> String.concat "\n"

let rec dbg = function
  | Leaf str -> str
  | Single (pre, retl, _suf) ->
      Format.asprintf "S%s, [%s])" pre (List.map dbg retl |> String.concat "; ")
  | Branch retl ->
      Format.asprintf "B[%s]" (List.map dbg retl |> String.concat "; ")

let counter = ref 0

let is_branch = function Branch _ -> true | _ -> false

let rec expand ret =
  (* let cnt = !counter in *)
  (* let dot = 1 in *)
  (* Format.printf "in[%d]: %s\n" cnt @@ dbg ret ; *)
  (* incr counter ; *)
  let after =
    match ret with
    | Leaf str -> Leaf str
    | Single (pre, [Branch [la; ra]; Branch [lb; rb]], suf) ->
        (* print_endline "branch-branch" ; *)
        let la = expand la in
        let ra = expand ra in
        let lb = expand lb in
        let rb = expand rb in
        Branch
          [ Single (pre, [la; lb], suf);
            Single (pre, [la; rb], suf);
            Single (pre, [ra; lb], suf);
            Single (pre, [ra; rb], suf) ]
    | Single (pre, [Branch [left; right]; r], suf) ->
        (* print_endline "branch-single" ; *)
        let left = expand left in
        let right = expand right in
        let r = expand r in
        Branch [Single (pre, [left; r], suf); Single (pre, [right; r], suf)]
    | Single (pre, [l; Branch [left; right]], suf) ->
        (* print_endline "single-branch" ; *)
        let l = expand l in
        let left = expand left in
        let right = expand right in
        Branch [Single (pre, [l; left], suf); Single (pre, [l; right], suf)]
    | Single (pre, [Branch [left; right]], suf) ->
        (* print_endline "branch" ; *)
        let left_heart = expand left in
        let left = Single (pre, [left_heart], suf) in
        let left = if is_branch left_heart then expand left else left in
        (* Format.printf "left[%d.%d]: %s\n" cnt dot @@ dbg left ; *)
        let right_core = expand right in
        let right = Single (pre, [right_core], suf) in
        let right = if is_branch right_core then expand right else right in
        (* Format.printf "right[%d.%d]: %s\n" cnt dot @@ dbg right ; *)
        Branch [left; right]
    | Single (pre, l, suf) ->
        (* print_endline "single-no-branch" ; *)
        let l = List.map expand l in
        if List.exists (function Branch _ -> true | _ -> false) l then
          (* print_endline "recurse" ; *)
          expand @@ Single (pre, l, suf)
        else Single (pre, l, suf)
    | Branch l -> Branch (List.map expand l)
  in
  (* Format.printf "out[%d]: %s\n" cnt @@ dbg after ; *)
  after

let split_at_branch l =
  let rec split_at_branch pre l =
    match l with
    | [] -> None
    | (Branch _ as b) :: l -> Some (pre, b, l)
    | x :: l -> split_at_branch (pre @ [x]) l
  in
  split_at_branch [] l

let rec value_str_ret : type a. a Proto.Script_typed_ir.ty -> ret =
  let open Proto.Script_typed_ir in
  function
  | Unit_t _ -> Leaf "Unit"
  | Int_t _ -> Leaf "-0"
  | Nat_t _ -> Leaf "0"
  | Bool_t _ -> Leaf "True"
  | String_t _ -> Leaf "\"hello\""
  | Signature_t _ ->
      Leaf
        "edsigtkpiSSschcaCt9pUVrpNPf7TTcgvgDEDD6NCEHMy8NNQJCGnMfLZzYoQj74yLjo9wx6MPVV29CvVzgi7qEcEUok3k7AuMg"
  | Bytes_t _ -> Leaf "0xdeadc0de"
  | Mutez_t _ -> Leaf "0"
  | Key_hash_t _ -> Leaf "\"tz1MSetqa3qdt3W7NTiqnrF8NY6ZhAPo5DAd\""
  | Key_t _ -> Leaf "\"edpkuc7UZcMf8Xyjzwg4FWApQF7TYjeQ9d3fKJTv2bvs6W4CxrB2SE\""
  | Timestamp_t _ -> Leaf "\"1970-01-01T03:41:46Z\""
  | Address_t _ -> Leaf "\"tz1ZdZDr5pLFbNLzgWQBCpm27ZgMSQPUYJN5\""
  | Pair_t ((left_t, _, _), (right_t, _, _), _, _) ->
      let left_v = value_str_ret left_t in
      let right_v = value_str_ret right_t in
      Single ("(Pair ", [left_v; right_v], ")")
  | Union_t ((left_t, _), (right_t, _), _, _) ->
      let left_v = value_str_ret left_t in
      let right_v = value_str_ret right_t in
      Branch
        [Single ("(Left ", [left_v], ")"); Single ("(Right ", [right_v], ")")]
  | Lambda_t (_arg_ty, _ret_ty, _) -> Leaf "{ PUSH nat 1 }"
  | Option_t (ty, _, _) -> Single ("(Some ", [value_str_ret ty], ")")
  | List_t (elt_ty, _, _) ->
      let elt = value_str_ret elt_ty in
      Single
        ("{ ", [Single ("Elt ", [elt], "; "); Single ("Elt ", [elt], "")], "}")
  | Set_t (elt_ty, _) ->
      let ety = Randgen.comparable_downcast elt_ty in
      let elt = value_str_ret ety in
      Single
        ("{ ", [Single ("Elt ", [elt], "; "); Single ("Elt ", [elt], "")], "}")
  | Big_map_t (key_ty, val_ty, _) ->
      let kty = Randgen.comparable_downcast key_ty in
      let key = value_str_ret kty in
      let data = value_str_ret val_ty in
      let l = [key; data] in
      Single ("{ ", [Single ("Elt ", l, "; "); Single ("Elt ", l, "")], "}")
  | Map_t (key_ty, val_ty, _, _) ->
      let kty = Randgen.comparable_downcast key_ty in
      let key = value_str_ret kty in
      let data = value_str_ret val_ty in
      let l = [key; data] in
      Single ("{ ", [Single ("Elt ", l, "; "); Single ("Elt ", l, "")], "}")
  | Contract_t (_, _) -> Leaf "\"KT1BEqzn5Wx8uJrZNvuS9DVHmLvG9td3fDLi\""
  | Operation_t _ ->
      Leaf
        "0x0000db450d08fda244dfb22abf4d877d990b09648a090000018190d1fac18a84c038011d23c1d3d2f8a4ea5e8784b8f7ecf2ad304c0fe600ff00000000090100000004c90ad161"
  | Chain_id_t _ -> Leaf "0x00000000"

let rec value_str : type a. a Proto.Script_typed_ir.ty -> string =
  let open Proto.Script_typed_ir in
  function
  | Unit_t _ -> "()"
  | Int_t _ -> "-0"
  | Nat_t _ -> "0"
  | Signature_t _ ->
      "edsigtkpiSSschcaCt9pUVrpNPf7TTcgvgDEDD6NCEHMy8NNQJCGnMfLZzYoQj74yLjo9wx6MPVV29CvVzgi7qEcEUok3k7AuMg"
  | String_t _ -> "\"hello\""
  | Bytes_t _ -> "0xdeadc0de"
  | Mutez_t _ -> "0"
  | Key_hash_t _ -> "\"tz1MSetqa3qdt3W7NTiqnrF8NY6ZhAPo5DAd\""
  | Key_t _ -> "\"edpkuc7UZcMf8Xyjzwg4FWApQF7TYjeQ9d3fKJTv2bvs6W4CxrB2SE\""
  | Timestamp_t _ -> "\"1970-01-01T03:41:46Z\""
  | Bool_t _ -> "True"
  | Address_t _ -> "\"tz1ZdZDr5pLFbNLzgWQBCpm27ZgMSQPUYJN5\""
  | Pair_t ((left_t, _, _), (right_t, _, _), _, _) ->
      let left_v = value_str left_t in
      let right_v = value_str right_t in
      Format.asprintf "(Pair %s %s)" left_v right_v
  | Union_t ((left_t, _), (_right_t, _), _, _) ->
      Format.asprintf "(Left %s)" (value_str left_t)
  | Lambda_t (_arg_ty, _ret_ty, _) -> "{ PUSH nat 1 }"
  | Option_t (ty, _, _) -> Format.asprintf "(Some %s)" (value_str ty)
  | List_t (elt_ty, _, _) ->
      let elt = value_str elt_ty in
      Format.asprintf "{ Elt %s ; Elt %s }" elt elt
  | Set_t (elt_ty, _) ->
      let ety = Randgen.comparable_downcast elt_ty in
      let elt = value_str ety in
      Format.asprintf "{ Elt %s ; Elt %s }" elt elt
  | Big_map_t (key_ty, val_ty, _) ->
      let kty = Randgen.comparable_downcast key_ty in
      let key = value_str kty in
      let data = value_str val_ty in
      Format.asprintf "{ Elt %s %s ; Elt %s %s }" key data key data
  | Map_t (key_ty, val_ty, _, _) ->
      let kty = Randgen.comparable_downcast key_ty in
      let key = value_str kty in
      let data = value_str val_ty in
      Format.asprintf "{ Elt %s %s ; Elt %s %s }" key data key data
  | Contract_t (_, _) -> "\"KT1BEqzn5Wx8uJrZNvuS9DVHmLvG9td3fDLi\""
  | Operation_t _ ->
      "0x0000db450d08fda244dfb22abf4d877d990b09648a090000018190d1fac18a84c038011d23c1d3d2f8a4ea5e8784b8f7ecf2ad304c0fe600ff00000000090100000004c90ad161"
  | Chain_id_t _ -> "0x00000000"

module Snoop = Randgen.Make (Base_sampler)

let _ = Random.self_init ()

let gen_val ~(ty : string) =
  Chain_env.empty_context >>=? fun context ->
  let ast =
    Michelson_v1_parser.parse_expression ty |> fst |> fun a -> a.expanded
  in
  let node = Micheline.root ast in
  Lwt.return
  @@ Proto.Script_ir_translator.parse_ty
       context
       ~legacy:false
       ~allow_big_map:true
       ~allow_operation:true
       ~allow_contract:true
       node
  >|= Proto.Environment.wrap_error
  >>=? fun (Ex_ty ty, _) ->
  let value = value_str_ret ty |> expand |> to_string in
  (* let value = value_str ty in *)
  return value

let gen_addr () = Snoop.sample_pkh ()

let gen_rand_val ~(ty : string) ~context =
  let sampler_parameters =
    Randgen.
      { int_size = (1, 3);
        string_size = (3, 7);
        bytes_size = (4, 5);
        stack_size = (0, 4);
        michelson_type_depth = (1, 1);
        list_size = (2, 4);
        set_size = (2, 4);
        map_size = (2, 4)
      }
  in
  let ast =
    Michelson_v1_parser.parse_expression ty |> fst |> fun a -> a.expanded
  in
  let node = Micheline.root ast in
  Lwt.return
  @@ Proto.Script_ir_translator.parse_ty
       context
       ~legacy:false
       ~allow_big_map:true
       ~allow_operation:true
       ~allow_contract:true
       node
  >|= Proto.Environment.wrap_error
  >>=? fun (Ex_ty ty, _) ->
  let randval = Snoop.Random_value.value sampler_parameters ty in
  Proto.Script_ir_translator.unparse_data
    context
    Proto.Script_ir_translator.Readable
    ty
    randval
  >|= Proto.Environment.wrap_error
  >>=? fun (node, _) ->
  let node = Micheline.strip_locations node in
  let node =
    Micheline_printer.printable
      Proto.Michelson_v1_primitives.string_of_prim
      node
  in
  let value = Format.asprintf "%a" Micheline_printer.print_expr node in
  return value

let wrap s = "(" ^ s ^ ")"

let rec get_node_names node =
  let open Micheline in
  match node with
  | Prim (_, name, nodes, _) ->
      (List.map get_node_names nodes |> String.concat " " |> function
       | "" -> ""
       | otherwise -> otherwise)
      |> fun s -> wrap @@ name ^ " " ^ s
  | _ -> ""

type tys = { storage_ty : string; param_ty : string }

let rec traverse_nodes acc node =
  let open Micheline in
  match node with
  | Prim (_, name, nodes, _) -> (
      match name with
      | "parameter" ->
          let ty =
            List.fold_left
              (fun s node -> s ^ " " ^ get_node_names node)
              ""
              nodes
          in
          { acc with param_ty = wrap ty }
      | "storage" ->
          let ty =
            List.fold_left
              (fun s node -> s ^ " " ^ get_node_names node)
              ""
              nodes
          in
          { acc with storage_ty = wrap ty }
      | _ -> acc )
  | Seq (_, nodes) -> List.fold_left traverse_nodes acc nodes
  | _ -> acc

let gen_param_storage ~code =
  let parsed = Michelson_v1_parser.parse_toplevel ~check:true code in
  let ast = fst parsed in
  let node = ast.expanded in
  let node = Micheline.root node in
  let node =
    Micheline.map_node
      (fun a -> a)
      Proto.Michelson_v1_primitives.string_of_prim
      node
  in
  let acc = traverse_nodes { storage_ty = ""; param_ty = "" } node in
  gen_val ~ty:acc.storage_ty >>=? fun storage ->
  gen_val ~ty:acc.param_ty >>=? fun parameter -> return (storage, parameter)
