open Proto

module Origination = struct
  let build ~code ~storage ~amount ~context =
    let open Proto.Apply_results in
    let open Models in
    let delegate = None in
    let preorigination = None in
    Lwt.return
    @@ Alpha_context.Contract.of_b58check "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
    >|= Proto.Environment.wrap_error
    >>=? fun source ->
    Contract.parse_script code storage >>=? fun script ->
    StepConstants.tez_of_string amount >>=? fun credit ->
    let open Alpha_context in
    Lwt.return @@ fresh_internal_nonce context >|= Proto.Environment.wrap_error
    >>=? fun (context, nonce) ->
    let operation = Origination { delegate; script; credit; preorigination } in
    let internal_op = { source; operation; nonce } in
    return (context, Internal_operation internal_op)
end

module Transaction = struct
  type t =
    { script : Script.t;
      self : Proto.Alpha_context.Contract.t;
      source : Alpha_context.Contract.t;
      payer : Alpha_context.Contract.t;
      parameter : Script.expr;
      amount : Proto.Alpha_context.Tez.t;
      entrypoint : string
    }

  type unbuilt =
    { param : string;
      gas : int;
      amount : int;
      self : string;
      payer : string;
      entrypoint : string
    }

  let unbuilt_of_tuple (param, gas, amount, self, payer, entrypoint) =
    { param; gas; amount; self; payer; entrypoint }

  let build (contracts : Models.Contract.unbuilt list) unbuilt =
    let open Models in
    Contract.parse_addr unbuilt.self >>=? fun self ->
    let (code, storage) =
      List.find (fun (c : Contract.unbuilt) -> c.addr = unbuilt.self) contracts
      |> fun (c : Contract.unbuilt) -> (c.code, c.storage)
    in
    Contract.parse_script code storage >>=? fun script ->
    Contract.parse_addr unbuilt.payer >>=? fun payer ->
    ParseTools.parse_parameter unbuilt.param >>=? fun parameter ->
    Models.StepConstants.parse_amount unbuilt.amount >>=? fun amount ->
    return
      { script;
        self;
        source = payer;
        payer;
        parameter;
        amount;
        entrypoint = unbuilt.entrypoint
      }

  (* | Transaction { amount; parameters; destination; entrypoint } -> ( *)
  (* | Transaction : {
       amount : Tez_repr.tez;
       parameters : Script_repr.lazy_expr;
       entrypoint : string;
       destination : Contract_repr.contract;
     }
  *)
  let build_internal_operation ~amount ~source ~parameter ~entrypoint
      ~destination ~context =
    let open Alpha_context in
    let open Models in
    Models.StepConstants.tez_of_string amount >>=? fun amount ->
    ParseTools.parse_parameter parameter >>=? fun parameter ->
    let parameters = Script.lazy_expr parameter in
    Lwt.return @@ fresh_internal_nonce context >|= Proto.Environment.wrap_error
    >>=? fun (context, nonce) ->
    let operation =
      Transaction { amount; parameters; entrypoint; destination }
    in
    let internal_op = { source; operation; nonce } in
    return (context, Internal_operation internal_op)

  let to_internal_operation (t : t) nonce =
    let open Alpha_context in
    let operation =
      Transaction
        { amount = t.amount;
          parameters = Script.lazy_expr t.parameter;
          destination = t.self;
          entrypoint = t.entrypoint
        }
    in
    let internal_op = { source = t.source; operation; nonce } in
    Internal_operation internal_op
end
