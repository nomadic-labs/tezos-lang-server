(* open Protocol *)
module Proto = Tezos_protocol_006_PsCARTHA.Protocol
open Proto

let uniform_byte () : char = Char.chr (Random.int 256)

let uniform_bits (nbytes : int) : string =
  String.init nbytes (fun _ -> uniform_byte ())

let string ~size =
  let (min, max) = size in
  let len = (Random.int @@ (max - min)) + min in
  uniform_bits len

(* ------------------------------------------------------------------------- *)
(* Helpers. *)

(* This function is in the protocol but is not exported. *)
let rec comparable_downcast :
    type a b. (a, b) Script_typed_ir.comparable_struct -> a Script_typed_ir.ty =
  let open Script_typed_ir in
  fun (type a b) (comp_ty : (a, b) comparable_struct) ->
    ( match comp_ty with
      | Int_key annot -> Int_t annot
      | Nat_key annot -> Nat_t annot
      | String_key annot -> String_t annot
      | Bytes_key annot -> Bytes_t annot
      | Mutez_key annot -> Mutez_t annot
      | Bool_key annot -> Bool_t annot
      | Key_hash_key annot -> Key_hash_t annot
      | Timestamp_key annot -> Timestamp_t annot
      | Address_key annot -> Address_t annot
      | Pair_key ((l, _), (r, _), _) ->
          let lty = comparable_downcast l in
          let rty = comparable_downcast r in
          let has_bigmaps = (* TODO *) false in
          Pair_t ((lty, None, None), (rty, None, None), None, has_bigmaps)
      : a ty )

(* ------------------------------------------------------------------------- *)
(* Module types & types used as input of the RMG *)

module type Base_samplers_S = sig
  val int :
    size:int * int -> Alpha_context.Script_int.z Alpha_context.Script_int.num

  val nat :
    size:int * int -> Alpha_context.Script_int.n Alpha_context.Script_int.num

  val signature : unit -> Signature.t

  val string : size:int * int -> String.t

  val bytes : size:int * int -> MBytes.t

  val tez : unit -> Alpha_context.Tez.tez

  val timestamp : size:int * int -> Alpha_context.Script_timestamp.t

  val bool : unit -> bool

  val key_pool_size : int
end

type sampler_parameters =
  { int_size : int * int;
    string_size : int * int;
    bytes_size : int * int;
    stack_size : int * int;
    michelson_type_depth : int * int;
    list_size : int * int;
    set_size : int * int;
    map_size : int * int
  }

(* -------------------------------------------------------------------------- *)
(* Type names. *)

type type_name =
  [ `TUnit
  | `TInt
  | `TNat
  | `TSignature
  | `TString
  | `TBytes
  | `TMutez
  | `TKey_hash
  | `TKey
  | `Timestamp
  | `TAddress
  | `TBool
  | `TPair
  | `TUnion
  | `TLambda
  | `TOption
  | `TList
  | `TSet
  | `TMap
  | `TBig_map
  | `TContract
  | `TOperation ]

let all_type_names : type_name array =
  [| `TUnit;
     `TInt;
     `TNat;
     `TSignature;
     `TString;
     `TBytes;
     `TMutez;
     `TKey_hash;
     `TKey;
     `Timestamp;
     `TAddress;
     `TBool;
     `TPair;
     `TUnion;
     `TLambda;
     `TOption;
     `TList;
     `TSet;
     `TMap;
     `TBig_map;
     `TContract;
     `TOperation
  |]

type atomic_type_name =
  [ `TUnit
  | `TInt
  | `TNat
  | `TSignature
  | `TString
  | `TBytes
  | `TMutez
  | `TKey_hash
  | `TKey
  | `Timestamp
  | `TAddress
  | `TBool ]

let all_atomic_type_names : atomic_type_name array =
  [| `TUnit;
     `TInt;
     `TNat;
     `TSignature;
     `TString;
     `TBytes;
     `TMutez;
     `TKey_hash;
     `TKey;
     `Timestamp;
     `TAddress;
     `TBool
  |]

type comparable_type_name =
  [ `TInt
  | `TNat
  | `TString
  | `TBytes
  | `TMutez
  | `TKey_hash
  | `Timestamp
  | `TAddress
  | `TBool ]

let all_comparable_type_names : comparable_type_name array =
  [| `TInt;
     `TNat;
     `TString;
     `TBytes;
     `TMutez;
     `TKey_hash;
     `Timestamp;
     `TAddress;
     `TBool
  |]

let type_names_count = Array.length all_type_names

let atomic_type_names_count = Array.length all_atomic_type_names

let comparable_type_names_count = Array.length all_comparable_type_names

(* -------------------------------------------------------------------------- *)
(* Uniform type name generators *)

let uniform_type_name : unit -> type_name =
 fun () ->
  let i = Random.int type_names_count in
  all_type_names.(i)

let uniform_atomic_type_name : unit -> atomic_type_name =
 fun () ->
  let i = Random.int atomic_type_names_count in
  all_atomic_type_names.(i)

let uniform_comparable_type_name : unit -> comparable_type_name =
 fun () ->
  let i = Random.int comparable_type_names_count in
  all_comparable_type_names.(i)

(* -------------------------------------------------------------------------- *)
(* Existentially packed typed value. *)

type ex_value = Ex_value : { typ : 'a Script_typed_ir.ty; v : 'a } -> ex_value

(* -------------------------------------------------------------------------- *)
(* Random generation functor. *)

module Make (V : Base_samplers_S) = struct
  (* Random generation of crypto-related values cannot be done blindly. *)
  (* We sample successively pk/pkh/skeys from a finite list. *)
  let (key_pool, current) =
    let array =
      Array.init V.key_pool_size (fun _i -> Signature.generate_key ())
    in
    (array, ref 0)

  let sample_pk () =
    let (_, pk, _) = key_pool.(!current) in
    current := (!current + 1) mod Array.length key_pool ;
    pk

  let sample_pkh () =
    let (pkh, _, _) = key_pool.(!current) in
    current := (!current + 1) mod Array.length key_pool ;
    pkh

  let sample_sk () =
    let (_, _, sk) = key_pool.(!current) in
    current := (!current + 1) mod Array.length key_pool ;
    sk

  (* Random generation of Michelson types. *)
  module Random_type = struct
    let type_of_atomic_type_name (at_tn : atomic_type_name) :
        Script_ir_translator.ex_ty =
      match at_tn with
      | `TString -> Ex_ty (String_t None)
      | `TNat -> Ex_ty (Nat_t None)
      | `TKey -> Ex_ty (Key_t None)
      | `TBytes -> Ex_ty (Bytes_t None)
      | `TBool -> Ex_ty (Bool_t None)
      | `TAddress -> Ex_ty (Address_t None)
      | `Timestamp -> Ex_ty (Timestamp_t None)
      | `TKey_hash -> Ex_ty (Key_hash_t None)
      | `TMutez -> Ex_ty (Mutez_t None)
      | `TSignature -> Ex_ty (Signature_t None)
      | `TUnit -> Ex_ty (Unit_t None)
      | `TInt -> Ex_ty (Int_t None)

    let comparable_type_of_comparable_type_name (cmp_tn : comparable_type_name)
        : Script_ir_translator.ex_comparable_ty =
      match cmp_tn with
      | `TString -> Ex_comparable_ty (String_key None)
      | `TNat -> Ex_comparable_ty (Nat_key None)
      | `TBytes -> Ex_comparable_ty (Bytes_key None)
      | `TBool -> Ex_comparable_ty (Bool_key None)
      | `TAddress -> Ex_comparable_ty (Address_key None)
      | `Timestamp -> Ex_comparable_ty (Timestamp_key None)
      | `TKey_hash -> Ex_comparable_ty (Key_hash_key None)
      | `TMutez -> Ex_comparable_ty (Mutez_key None)
      | `TInt -> Ex_comparable_ty (Int_key None)

    let rec m_type ~max_depth : Script_ir_translator.ex_ty =
      let open Script_ir_translator in
      if max_depth = 0 then
        let at_tn = uniform_atomic_type_name () in
        type_of_atomic_type_name at_tn
      else
        let tn = uniform_type_name () in
        match tn with
        | #atomic_type_name as at_tn -> type_of_atomic_type_name at_tn
        | `TPair -> (
            let left = m_type ~max_depth:(max_depth - 1) in
            let right = m_type ~max_depth:(max_depth - 1) in
            match (left, right) with
            | (Ex_ty left_ty, Ex_ty right_ty) ->
                let has_bigmaps = (* TODO *) false in
                Ex_ty
                  (Pair_t
                     ( (left_ty, None, None),
                       (right_ty, None, None),
                       None,
                       has_bigmaps )) )
        | `TLambda -> (
            let domain = m_type ~max_depth:(max_depth - 1) in
            let codomain = m_type ~max_depth:(max_depth - 1) in
            match (domain, codomain) with
            | (Ex_ty domain_ty, Ex_ty codomain_ty) ->
                Ex_ty (Lambda_t (domain_ty, codomain_ty, None)) )
        | `TUnion -> (
            let left = m_type ~max_depth:(max_depth - 1) in
            let right = m_type ~max_depth:(max_depth - 1) in
            match (left, right) with
            | (Ex_ty left_ty, Ex_ty right_ty) ->
                let has_bigmaps = (* TODO *) false in
                Ex_ty
                  (Union_t ((left_ty, None), (right_ty, None), None, has_bigmaps))
            )
        | `TOption -> (
            let opt = m_type ~max_depth:(max_depth - 1) in
            match opt with
            | Ex_ty opt_ty ->
                let has_bigmaps = (* TODO *) false in
                Ex_ty (Option_t (opt_ty, None, has_bigmaps)) )
        | `TMap -> (
            let key = m_comparable_type () in
            let elt = m_type ~max_depth:(max_depth - 1) in
            match (key, elt) with
            | (Ex_comparable_ty key_ty, Ex_ty elt_ty) ->
                let has_bigmaps = (* TODO *) false in
                Ex_ty (Map_t (key_ty, elt_ty, None, has_bigmaps)) )
        | `TSet -> (
            let key = m_comparable_type () in
            match key with
            | Ex_comparable_ty key_ty -> Ex_ty (Set_t (key_ty, None)) )
        | `TList -> (
            let elt = m_type ~max_depth:(max_depth - 1) in
            match elt with
            | Ex_ty elt_ty ->
                let has_bigmaps = (* TODO *) false in
                Ex_ty (List_t (elt_ty, None, has_bigmaps)) )
        | `TContract | `TOperation | `TBig_map ->
            (* Don't know what to do with theses. Redraw. *)
            m_type ~max_depth

    and m_comparable_type () : Script_ir_translator.ex_comparable_ty =
      let cmp_tn = uniform_comparable_type_name () in
      comparable_type_of_comparable_type_name cmp_tn
  end

  (* Type-directed generation of random values. *)
  module rec Random_value : sig
    val value : sampler_parameters -> 'a Script_typed_ir.ty -> 'a

    val stack :
      sampler_parameters ->
      'a Script_typed_ir.stack_ty ->
      'a Script_interpreter.stack
  end = struct
    let rec value : type a. sampler_parameters -> a Script_typed_ir.ty -> a =
      let open Script_typed_ir in
      fun sp typ ->
        match typ with
        | Unit_t _ -> (() : a)
        | Int_t _ -> V.int ~size:sp.int_size
        | Nat_t _ -> V.nat ~size:sp.int_size
        | Signature_t _ -> V.signature ()
        | String_t _ -> V.string ~size:sp.string_size
        | Bytes_t _ -> V.bytes ~size:sp.bytes_size
        | Mutez_t _ -> V.tez ()
        | Key_hash_t _ -> sample_pkh ()
        | Key_t _ -> sample_pk ()
        | Timestamp_t _ -> V.timestamp ~size:sp.int_size
        | Bool_t _ -> V.bool ()
        | Address_t _ ->
            (Alpha_context.Contract.implicit_contract (sample_pkh ()), "default")
        | Pair_t ((left_t, _, _), (right_t, _, _), _, _) ->
            let left_v = value sp left_t in
            let right_v = value sp right_t in
            (left_v, right_v)
        | Union_t ((left_t, _), (right_t, _), _, _) ->
            if V.bool () then L (value sp left_t) else R (value sp right_t)
        | Lambda_t (arg_ty, ret_ty, _) -> generate_lambda sp arg_ty ret_ty
        | Option_t (ty, _, _) -> if V.bool () then None else Some (value sp ty)
        | List_t (elt_ty, _, _) -> generate_list sp elt_ty
        | Set_t (elt_ty, _) -> generate_set sp elt_ty
        | Map_t (key_ty, val_ty, _, _) -> generate_map sp key_ty val_ty
        | Contract_t (arg_ty, _) -> generate_contract sp arg_ty
        | Operation_t _ -> generate_operation sp
        | Big_map_t (key_ty, val_ty, _) -> generate_big_map sp key_ty val_ty
        | Chain_id_t _ -> Chain_id.zero

    and generate_lambda :
        type arg ret.
        sampler_parameters ->
        arg Script_typed_ir.ty ->
        ret Script_typed_ir.ty ->
        (arg, ret) Script_typed_ir.lambda =
     fun sp _arg_ty _ret_ty ->
      ignore sp ;
      (* Script_typed_ir.Lam ((_arg_ty, ()), (_ret_ty, ())) *)
      assert false

    and generate_list :
        type elt. sampler_parameters -> elt Script_typed_ir.ty -> elt list =
     fun sp elt_type ->
      let (min, max) = sp.list_size in
      let size = (Random.int @@ (max - min)) + min in
      (* Generate empty lists on what we don't know how to generate *)
      let size =
        match elt_type with Lambda_t _ | Operation_t _ -> 0 | _ -> size
      in
      List.init size (fun _ -> value sp elt_type)

    and generate_set :
        type elt.
        sampler_parameters ->
        elt Script_typed_ir.comparable_ty ->
        elt Script_typed_ir.set =
     fun sp elt_ty ->
      let (min, max) = sp.set_size in
      let size = (Random.int @@ (max - min)) + min in
      let ety = comparable_downcast elt_ty in
      let list = List.init size (fun _ -> value sp ety) in
      List.fold_left
        (fun set x -> Script_ir_translator.set_update x true set)
        (Script_ir_translator.empty_set elt_ty)
        list

    and generate_map :
        type key elt.
        sampler_parameters ->
        key Script_typed_ir.comparable_ty ->
        elt Script_typed_ir.ty ->
        (key, elt) Script_typed_ir.map =
     fun sp key_ty elt_ty ->
      let (min, max) = sp.map_size in
      let size = (Random.int @@ (max - min)) + min in
      let kty = comparable_downcast key_ty in
      let keys = List.init size (fun _ -> value sp kty) in
      let elts = List.init size (fun _ -> value sp elt_ty) in
      List.fold_left2
        (fun map key elt -> Script_ir_translator.map_update key (Some elt) map)
        (Script_ir_translator.empty_map key_ty)
        keys
        elts

    and generate_big_map :
        type key elt.
        sampler_parameters ->
        key Script_typed_ir.comparable_ty ->
        elt Script_typed_ir.ty ->
        (key, elt) Script_typed_ir.big_map =
      let open Script_typed_ir in
      fun sp key_ty elt_ty ->
        let has_big_maps = false in
        (* Cannot have big maps under big maps *)
        let opt_elt_ty = Option_t (elt_ty, None, has_big_maps) in
        let diff = generate_map sp key_ty opt_elt_ty in
        let key_type = comparable_downcast key_ty in
        { id = None; diff; key_type; value_type = elt_ty }

    and generate_contract :
        type arg.
        sampler_parameters ->
        arg Script_typed_ir.ty ->
        arg Script_typed_ir.typed_contract =
     fun sp arg_ty ->
      ignore sp ;
      let op_hash = Operation_hash.zero in
      let nonce = Alpha_context.Contract.initial_origination_nonce op_hash in
      let address = Alpha_context.Contract.originated_contract nonce in
      (arg_ty, (address, "default"))

    and generate_operation :
        sampler_parameters ->
        Alpha_context.packed_internal_operation
        * Alpha_context.Contract.big_map_diff option =
     fun sp ->
      (* TODO
         legit ops:
         transfer_token
         origination: create_contract/create_account
         set_delegate *)
      (generate_transfer_tokens sp, None)

    and generate_transfer_tokens :
        sampler_parameters -> Alpha_context.packed_internal_operation =
     fun sp ->
      ignore sp ;
      (* Generate a random source. *)
      let source = Alpha_context.Contract.implicit_contract (sample_pkh ()) in
      (* Generate a random amount. *)
      let amount = V.tez () in
      (* A default destination. TODO: make this more random? *)
      let destination =
        let op_hash = Operation_hash.zero in
        let nonce = Alpha_context.Contract.initial_origination_nonce op_hash in
        Alpha_context.Contract.originated_contract nonce
      in
      (* We bound the maximum depth of micheline expressions to 2. This should maybe
         be a parameter. *)
      let script = Micheline.(String ("lol", string ~size:(4, 5))) in
      let entrypoint = "default" in
      let open Alpha_context in
      let operation =
        Transaction
          { amount;
            destination;
            parameters = Script.lazy_expr (Micheline.strip_locations script);
            entrypoint
          }
      in
      Internal_operation { source; operation; nonce = 0 }

    (* Random stack generation. *)
    let rec stack :
        type a.
        sampler_parameters ->
        a Script_typed_ir.stack_ty ->
        a Script_interpreter.stack =
      let open Script_typed_ir in
      fun sp stack_ty ->
        match stack_ty with
        | Item_t (ty, tl, _) ->
            let elt = value sp ty in
            let rest = stack sp tl in
            (Script_interpreter.Item (elt, rest) : a Script_interpreter.stack)
        | Empty_t -> Empty
  end

  (* Existentially pack random values. *)
  (* and Ex_random_value :
   * sig
   *   val value : max_depth:int -> sampler_parameters -> ex_value
   * end =
   * struct
   * 
   *   module Atomic =
   *   struct
   *     let int sp = Ex_value { typ = Int_t None ; v = V.int ~size:sp.int_size }
   *     let nat sp = Ex_value { typ = Nat_t None ; v = V.nat ~size:sp.int_size }
   *     let signature () = Ex_value { typ = Signature_t None ;
   *                                   v   = V.signature () }
   *     let string sp = Ex_value { typ = String_t None ;
   *                                v   = V.string ~size:sp.int_size }
   *     let bytes sp = Ex_value { typ = Bytes_t None ;
   *                               v   = V.bytes ~size:sp.int_size }
   *     let mutez () = Ex_value { typ = Mutez_t None ;
   *                               v   = V.tez () }
   *     let key_hash () = Ex_value { typ = Key_hash_t None ;
   *                                  v   = sample_pkh () }
   *     let key () = Ex_value { typ = Key_t None ;
   *                             v   = sample_pk () }
   *     let timestamp sp = Ex_value { typ = Timestamp_t None ;
   *                                   v   = V.timestamp ~size:sp.int_size }
   *     let bool () = Ex_value { typ = Bool_t None ;
   *                              v   = V.bool () }
   *     let address () =
   *       let v = Alpha_context.Contract.implicit_contract (sample_pkh ()) in
   *       let c = "default" in
   *       Ex_value { typ = Address_t None ; v = (v, c) }
   * 
   *     let unit =
   *       (\* Don't reallocate pointlessly. Could do the same for bool. *\)
   *       let res = Ex_value { typ = Unit_t None ;
   *                            v   = () } in
   *       fun () -> res
   *   end
   * 
   *   (\* Atomic/comparable value generators *\)
   *   let random_atomic (sp : sampler_parameters) (n : atomic_type_name) =
   *     match n with
   *     | `TString -> Atomic.string sp
   *     | `TNat -> Atomic.nat sp
   *     | `TKey -> Atomic.signature ()
   *     | `TBytes -> Atomic.bytes sp
   *     | `TBool -> Atomic.bool ()
   *     | `TAddress -> Atomic.address ()
   *     | `Timestamp -> Atomic.timestamp sp
   *     | `TKey_hash -> Atomic.key_hash ()
   *     | `TMutez -> Atomic.mutez ()
   *     | `TSignature -> Atomic.signature ()
   *     | `TUnit -> Atomic.unit ()
   *     | `TInt -> Atomic.int sp
   * 
   *   (\* Atomic/comparable value generators *\)
   *   let random_comparable (sp : sampler_parameters) (n : comparable_type_name) =
   *     (match n with
   *      | `TString -> Atomic.string sp
   *      | `TNat -> Atomic.nat sp
   *      | `TBytes -> Atomic.bytes sp
   *      | `TBool -> Atomic.bool ()
   *      | `TAddress -> Atomic.address ()
   *      | `Timestamp -> Atomic.timestamp sp
   *      | `TKey_hash -> Atomic.key_hash ()
   *      | `TMutez ->  Atomic.mutez ()
   *      | `TInt -> Atomic.int sp)
   * 
   *   (\* Atomic/comparable value generators *\)
   *   let rec value : max_depth:int -> sampler_parameters -> ex_value =
   *     fun ~max_depth sp ->
   *       if max_depth = 0 then
   *         let at_tn = uniform_atomic_type_name () in
   *         random_atomic sp at_tn
   *       else
   *         let tn = uniform_type_name () in
   *         match tn with
   *         | (#atomic_type_name as at_tn) -> random_atomic sp at_tn
   *         | `TPair -> generate_pair ~max_depth sp
   *         | `TLambda -> generate_lambda ~max_depth sp
   *         | `TUnion -> generate_union ~max_depth sp
   *         | `TMap -> generate_map ~max_depth sp
   *         | `TSet -> generate_set ~max_depth sp
   *         | `TList -> generate_list ~max_depth sp
   *         | `TBig_map -> generate_big_map ~max_depth sp
   *         | `TOption -> generate_option ~max_depth sp
   *         | `TOperation -> generate_operation sp
   *         | `TContract ->
   *             (\* TODO generate contract values. For now, resample. *\)
   *             value ~max_depth sp
   * 
   *   and generate_pair ~max_depth sp =
   *     let left  = value ~max_depth:(max_depth - 1) sp in
   *     let right = value ~max_depth:(max_depth - 1) sp in
   *     let has_bigmaps = (\* TODO *\) false in
   *     (match left, right with
   *      | Ex_value { typ = lt ; v = lv }, Ex_value { typ = rt ; v = rv } ->
   *          Ex_value { typ = Pair_t ((lt, None, None), (rt, None, None), None, has_bigmaps) ;
   *                     v   = (lv, rv) })
   * 
   *   and generate_lambda ~max_depth sp =
   *     ignore (max_depth, sp) ;
   *     assert false
   * 
   *   and generate_union ~max_depth sp =
   *     if V.bool () then
   *       let left  = value ~max_depth:(max_depth - 1) sp in
   *       let ex_rt = Random_type.m_type ~max_depth:(max_depth - 1) in
   *       let has_bigmaps = (\* TODO *\) false in
   *       (match left, ex_rt with
   *        | Ex_value { typ = lt ; v = lv }, Script_ir_translator.Ex_ty rt ->
   *            Ex_value { typ = Union_t ((lt, None), (rt, None), None, has_bigmaps) ;
   *                       v   = L lv })
   *     else
   *       let right = value ~max_depth:(max_depth - 1) sp in
   *       let ex_lt = Random_type.m_type ~max_depth:(max_depth - 1)  in
   *       let has_bigmaps = (\* TODO *\) false in
   *       (match right, ex_lt with
   *        | Ex_value { typ = rt ; v = rv }, Script_ir_translator.Ex_ty lt ->
   *            Ex_value { typ = Union_t ((lt, None), (rt, None), None, has_bigmaps) ;
   *                       v   = R rv })
   * 
   *   and generate_operation sp =
   *     (\* TODO: tweak [sp] to bound the size of the contents of the op *\)
   *     let typ = Script_typed_ir.Operation_t None in
   *     let v = Random_value.value sp typ in
   *     Ex_value { typ ; v }
   * 
   *   and generate_option ~max_depth sp =
   *     let ex_ty = Random_type.m_type ~max_depth in
   *     match ex_ty with
   *     | Script_ir_translator.Ex_ty ty ->
   *         let has_bigmaps = (\* TODO *\) false in
   *         let typ = Script_typed_ir.Option_t (ty, None, has_bigmaps) in
   *         Ex_value { typ ; v = Random_value.value sp typ }
   * 
   *   and generate_map ~max_depth sp =
   *     let open Script_ir_translator in
   *     let open Script_typed_ir in
   *     let key_ty = Random_type.m_comparable_type () in
   *     let val_ty = Random_type.m_type ~max_depth in
   *     match key_ty, val_ty with
   *     | Ex_comparable_ty key_ty, Ex_ty val_ty ->
   *         let has_bigmaps = (\* TODO *\) false in
   *         let typ = Map_t (key_ty, val_ty, None, has_bigmaps) in
   *         let v   = Random_value.value sp typ in
   *         Ex_value { typ ; v }
   * 
   *   and generate_set ~max_depth sp =
   *     let open Script_ir_translator in
   *     let open Script_typed_ir in
   *     let elt_ty = Random_type.m_comparable_type () in
   *     match elt_ty with
   *     | Ex_comparable_ty elt_ty ->
   *         let typ = Set_t (elt_ty, None) in
   *         (\* TODO: tweak [sp] to bound size of values in set via [max_depth] *\)
   *         ignore max_depth ;
   *         let v   = Random_value.value sp typ in
   *         Ex_value { typ ; v }
   * 
   *   and generate_big_map ~max_depth sp =
   *     let open Script_ir_translator in
   *     let open Script_typed_ir in
   *     let key_ty = Random_type.m_comparable_type () in
   *     let val_ty = Random_type.m_type ~max_depth in
   *     match key_ty, val_ty with
   *     | Ex_comparable_ty key_ty, Ex_ty val_ty ->
   *         let typ = Big_map_t (key_ty, val_ty, None) in
   *         let v   = Random_value.value sp typ in
   *         Ex_value { typ ; v }
   * 
   *   and generate_list ~max_depth sp =
   *     let ex_elt_ty = Random_type.m_type ~max_depth:(max_depth - 1) in
   *     match ex_elt_ty with
   *     | Script_ir_translator.Ex_ty elt_ty ->
   *         let has_bigmaps = (\* TODO *\) false in
   *         let list_ty = Script_typed_ir.List_t (elt_ty, None, has_bigmaps) in
   *         let v = Random_value.value sp list_ty in
   *         Ex_value { typ = list_ty ; v } *)
  (* end *)
end

(* -------------------------------------------------------------------------- *)
(* Some basic samplers. *)

(* Some functions to sample uniform bit sequences of fixed length *)

let uniform_byte () : char = Char.chr (Random.int 256)

let uniform_partial_byte (nbits : int) : char =
  if nbits < 1 || nbits > 8 then
    Stdlib.failwith "uniform_partial_byte: invalid argument"
  else
    let i = Random.int 256 in
    Char.chr (i lsr (8 - nbits))

let uniform_bits (nbytes : int) : string =
  String.init nbytes (fun _ -> uniform_byte ())

let uniform_nat (nbytes : int) : Z.t = Z.of_bits (uniform_bits nbytes)

let z ~size =
  let (min, max) = size in
  let nbytes = (Random.int @@ (max - min)) + min in
  uniform_nat nbytes

let int ~size =
  let z = z ~size in
  let s = Random.bool () in
  if s then Alpha_context.Script_int.of_zint z
  else Alpha_context.Script_int.of_zint Z.(neg z)

let nat ~size =
  let z = Alpha_context.Script_int.of_zint (z ~size) in
  match Alpha_context.Script_int.is_nat z with
  | Some res -> res
  | None -> assert false

let string ~size =
  let (min, max) = size in
  let len = (Random.int @@ (max - min)) + min in
  uniform_bits len

let bytes ~size =
  let (min, max) = size in
  let len = (Random.int @@ (max - min)) + min in
  MBytes.of_string (uniform_bits len)

let tez () =
  let i = Random.int64 (Int64.of_int max_int) in
  match Alpha_context.Tez.of_mutez i with
  | Some res -> res
  | None -> assert false

let timestamp ~size = Alpha_context.Script_timestamp.of_zint (z ~size)

let bool () = Random.bool ()

(* -------------------------------------------------------------------------- *)
(* Random Micheline generator. *)

let uniform_prim : unit -> Michelson_v1_primitives.prim =
 fun () ->
  let i = Random.int 112 in
  match i with
  | 0 -> K_parameter
  | 1 -> K_storage
  | 2 -> K_code
  | 3 -> D_False
  | 4 -> D_Elt
  | 5 -> D_Left
  | 6 -> D_None
  | 7 -> D_Pair
  | 8 -> D_Right
  | 9 -> D_Some
  | 10 -> D_True
  | 11 -> D_Unit
  | 12 -> I_PACK
  | 13 -> I_UNPACK
  | 14 -> I_BLAKE2B
  | 15 -> I_SHA256
  | 16 -> I_SHA512
  | 17 -> I_ABS
  | 18 -> I_ADD
  | 19 -> I_AMOUNT
  | 20 -> I_AND
  | 21 -> I_BALANCE
  | 22 -> I_CAR
  | 23 -> I_CDR
  | 24 -> I_CHECK_SIGNATURE
  | 25 -> I_COMPARE
  | 26 -> I_CONCAT
  | 27 -> I_CONS
  | 28 -> I_CREATE_ACCOUNT
  | 29 -> I_CREATE_CONTRACT
  | 30 -> I_IMPLICIT_ACCOUNT
  | 31 -> I_DIP
  | 32 -> I_DROP
  | 33 -> I_DUP
  | 34 -> I_EDIV
  | 35 -> I_EMPTY_MAP
  | 36 -> I_EMPTY_SET
  | 37 -> I_EQ
  | 38 -> I_EXEC
  | 39 -> I_FAILWITH
  | 40 -> I_GE
  | 41 -> I_GET
  | 42 -> I_GT
  | 43 -> I_HASH_KEY
  | 44 -> I_IF
  | 45 -> I_IF_CONS
  | 46 -> I_IF_LEFT
  | 47 -> I_IF_NONE
  | 48 -> I_INT
  | 49 -> I_LAMBDA
  | 50 -> I_LE
  | 51 -> I_LEFT
  | 52 -> I_LOOP
  | 53 -> I_LSL
  | 54 -> I_LSR
  | 55 -> I_LT
  | 56 -> I_MAP
  | 57 -> I_MEM
  | 58 -> I_MUL
  | 59 -> I_NEG
  | 60 -> I_NEQ
  | 61 -> I_NIL
  | 62 -> I_NONE
  | 63 -> I_NOT
  | 64 -> I_NOW
  | 65 -> I_OR
  | 66 -> I_PAIR
  | 67 -> I_PUSH
  | 68 -> I_RIGHT
  | 69 -> I_SIZE
  | 70 -> I_SOME
  | 71 -> I_SOURCE
  | 72 -> I_SENDER
  | 73 -> I_SELF
  | 74 -> I_SLICE
  | 75 -> I_STEPS_TO_QUOTA
  | 76 -> I_SUB
  | 77 -> I_SWAP
  | 78 -> I_TRANSFER_TOKENS
  | 79 -> I_SET_DELEGATE
  | 80 -> I_UNIT
  | 81 -> I_UPDATE
  | 82 -> I_XOR
  | 83 -> I_ITER
  | 84 -> I_LOOP_LEFT
  | 85 -> I_ADDRESS
  | 86 -> I_CONTRACT
  | 87 -> I_ISNAT
  | 88 -> I_CAST
  | 89 -> I_RENAME
  | 90 -> T_bool
  | 91 -> T_contract
  | 92 -> T_int
  | 93 -> T_key
  | 94 -> T_key_hash
  | 95 -> T_lambda
  | 96 -> T_list
  | 97 -> T_map
  | 98 -> T_big_map
  | 99 -> T_nat
  | 100 -> T_option
  | 101 -> T_or
  | 102 -> T_pair
  | 103 -> T_set
  | 104 -> T_signature
  | 105 -> T_string
  | 106 -> T_bytes
  | 107 -> T_mutez
  | 108 -> T_timestamp
  | 109 -> T_unit
  | 110 -> T_operation
  | 111 -> T_address
  | _ -> assert false

(** Random Micheline generator functor. *)

(* module RandoMicheline (X : MichelineSamplerParamS) =
 * struct
 *
 *   let rec micheline depth =
 *     if depth = 0 then
 *       let case = Random.int 3 in
 *       let l = X.label_gen () in
 *       begin match case with
 *         | 0 ->
 *             let i = X.int () in
 *             Micheline.Int (l, i)
 *         | 1 ->
 *             let s = X.string () in
 *             Micheline.String (l, s)
 *         | 2 ->
 *             let b = X.bytes () in
 *             Micheline.Bytes (l, b)
 *         | _ ->
 *             assert false
 *       end
 *     else
 *       let case = Random.int 5 in
 *       let l = X.label_gen () in
 *       begin match case with
 *         | 0 ->
 *             let i = X.int () in
 *             Micheline.Int (l, i)
 *         | 1 ->
 *             let s = X.string () in
 *             Micheline.String (l, s)
 *         | 2 ->
 *             let b = X.bytes () in
 *             Micheline.Bytes (l, b)
 *         | 3 ->
 *             let prim  = X.prim_gen () in
 *             let nargs = X.max_width ~depth in
 *             let args = List.init nargs (fun _ -> micheline (depth - 1)) in
 *             Micheline.Prim (l, prim, args, [])
 *         | 4 ->
 *             let nargs = X.max_width ~depth in
 *             let args = List.init nargs (fun _ -> micheline (depth - 1)) in
 *             Micheline.Seq (l, args)
 *         | _ ->
 *             assert false
 *       end
 *
 * end *)
