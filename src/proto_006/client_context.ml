module Alpha_context = Proto.Alpha_context
module Account_map = Map.Make (String)
open Tezos_error_monad.Error_monad
open Extdlib

type error += Alias_already_used of string

let () =
  register_error_kind
    `Permanent
    ~id:"alias_already_used"
    ~title:"Alias already used."
    ~description:
      "User tried to create an account with an handle that previously existed"
    ~pp:(fun ppf s -> Format.fprintf ppf "Account %s already exists." s)
    Data_encoding.(obj1 (req "value" string))
    (function Alias_already_used s -> Some s | _ -> None)
    (fun s -> Alias_already_used s)

type t = { alpha : Alpha_context.t; accounts : string Account_map.t }

module Encodings = struct
  open Data_encoding

  let bindings = list (obj2 (req "alias" string) (req "addr" string))

  let from_bindings bindings =
    bindings
    |> List.fold_left
         (fun acc (key, data) -> Account_map.add key data acc)
         Account_map.empty

  let accounts = conv Account_map.bindings from_bindings bindings

  let tuple_t =
    obj2 (req "alpha" Memory_context.encoding) (req "accounts" accounts)

  let project t =
    ((ContextHelpers.to_raw_context ~context:t.alpha).context, t.accounts)

  let inject (raw_context, accounts) =
    let t =
      Lwt_main.run
        ( ContextHelpers.alpha_of_context ~alpha:raw_context >>=? fun alpha ->
          return { alpha; accounts } )
    in
    match t with Ok t -> t | _ -> assert false

  let t = conv project inject tuple_t
end

module Accounts = struct
  let pp accounts ppf ct =
    let addr = Format.asprintf "%a" Alpha_context.Contract.pp ct in
    let addr =
      Option.unopt ~default:addr (Account_map.find_opt addr accounts)
    in
    Format.fprintf ppf "%s" addr

  let empty = Account_map.empty
end

let make ~alpha = { alpha; accounts = Account_map.empty }

let update alpha t = { t with alpha }

let check_alias ~name t =
  match Account_map.mem name t.accounts with
  | true -> fail @@ Alias_already_used name
  | false -> return name

let add_account ~name ~addr t =
  let accounts = Account_map.add addr name t.accounts in
  return { t with accounts }

let replace_addr_with_alias addr ~t =
  match Account_map.find_opt addr t.accounts with None -> addr | Some v -> v

let get_addr ~alias t = Account_map.rev_find_opt alias t.accounts

let replace_if_alias ~addr t =
  match get_addr ~alias:addr t with None -> addr | Some addr -> addr

let has_alias_opt addr ~t = Account_map.find_opt addr t.accounts

let list_known_accounts t =
  Account_map.iter
    (fun alias acc -> Format.printf "%s : %s @." acc alias)
    t.accounts

module Serial = struct
  let write ~cctx ~filename =
    let json = Data_encoding.Json.construct Encodings.t cctx in
    let contents = Json.to_string json in
    let filename =
      match filename with
      | Some filename -> filename
      | None -> Format.asprintf "/tmp/context-%x" (Hashtbl.hash contents)
    in
    Extdlib.File.write ~filename ~contents ;
    return filename

  let load ~filename =
    let json = Extdlib.File.read filename in
    ( match Json.parse json with
    | Ok json -> return json
    | Error err -> Lwt.return @@ Error_monad.generic_error "%s" err )
    >>=? fun json ->
    let (cctx : t) = Data_encoding.Json.destruct Encodings.t json in
    return cctx
end
