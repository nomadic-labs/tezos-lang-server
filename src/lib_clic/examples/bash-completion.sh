_auto_complete()
{
    local cur_word prev_word type_list

    cur_word="${COMP_WORDS[COMP_CWORD]}"
    prev_word="${COMP_WORDS[COMP_CWORD-1]}"

    # Tezos script
    script=${COMP_WORDS[0]}

    reply=$($script bash_autocomplete "$prev_word" "$cur_word" ${COMP_WORDS[@]} 2>/dev/null)

    COMPREPLY=($(compgen -W "$reply" -- $cur_word))

    return 0
}

# Register _auto_complete to provide completion for the following commands
complete -F _auto_complete clictest
