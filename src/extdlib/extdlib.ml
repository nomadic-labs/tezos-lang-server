module Option = struct
  include Option

  let is_none = function None -> true | _ -> false

  let is_some = function None -> false | _ -> true
end

module File = struct
  let read_lines filename =
    let lines = ref [] in
    let chan = open_in filename in
    try
      while true do
        lines := input_line chan :: !lines
      done ;
      !lines
    with End_of_file ->
      close_in chan ;
      List.rev !lines

  let read filename = String.concat "\n" @@ read_lines filename

  let write ~filename ~contents =
    let chan = open_out filename in
    output_string chan contents ;
    close_out chan
end

module List = struct
  include List

  let rev_assoc_opt b l =
    let rec assoc b = function
      | [] -> None
      | (x, y) :: tl -> if y = b then Some x else assoc b tl
      [@@tailcall]
    in
    assoc b l

  let rec map2_opt f a b =
    match (a, b) with
    | ([], []) -> []
    | (hd :: tl, []) -> f (Some hd) None :: map2_opt f tl b
    | ([], hd :: tl) -> f None (Some hd) :: map2_opt f a tl
    | (ha :: ta, hb :: tb) -> f (Some ha) (Some hb) :: map2_opt f ta tb

  let rec assoc_third cmp elt = function
    | [] -> None
    | (a, b, c) :: tl ->
        if cmp elt a then Some (b, c) else assoc_third cmp elt tl
end

module Map = struct
  include Map

  module Make (Ord : Map.OrderedType) = struct
    include Map.Make (Ord)

    let rev_find_opt data map = bindings map |> List.rev_assoc_opt data
  end
end

module String = struct
  include String

  let sub_pos str start stop = String.sub str start (stop - start)

  let remove_last_n str i =
    let len = String.length str in
    String.sub str 0 (len - i)

  (* FIXME: include carriage return *)
  let width str =
    let rec width str count i =
      match str.[i] with
      | exception _ -> count
      | '\n' -> width str (count + 1) (i + 1)
      | _ -> width str count (i + 1)
    in
    width str 0 0

  let is_ident c =
    let c = Char.code c in
    (c >= 48 && c <= 57)
    || (c >= 65 && c <= 90)
    || (c >= 97 && c <= 122)
    || c = 95

  let is_allowed c =
    is_ident c || c = '-' || c = '_' || c = '/' || c = '.' || c = ':'

  let rec word_end_index str i =
    match str.[i] with
    | s when is_ident s -> word_end_index str (i - 1)
    | _ -> i + 1

  let word i str =
    let rec from_space i =
      match str.[i] with
      | s when is_ident s -> from_space (i - 1)
      | _ -> i + 1
      | exception _ -> 0
    in
    let rec to_space i =
      match str.[i] with
      | s when is_ident s -> to_space (i + 1)
      | _ -> i
      | exception _ -> i - 1
    in
    let len = to_space i - from_space (i - 1) in
    let len = if len = 0 then 1 else len in
    String.sub str (from_space (i - 1)) len

  let line i str =
    let lines = String.split_on_char '\n' str in
    List.nth lines (i - 1)
end
