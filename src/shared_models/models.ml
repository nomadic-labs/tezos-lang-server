module StackElt = struct
  type t =
    | Stable of string
    | Removed of string
    | Added of string
    | Changed of string
end

module Typemap = struct
  type dual = { loc : Location.t; before : string list; after : string list }

  type delta = { loc : Location.t; stack : StackElt.t list }

  type t = delta list

  type old = dual list

  let at line col t =
    match
      List.find (fun { loc; _ } -> Location.is_inside line col loc) (List.rev t)
    with
    | exception _ -> None
    | transition -> Some transition

  let from_micheline =
    List.map (fun (loc, before, after) ->
        { loc = Location.Decode.from_micheline loc; before; after })
end

module Trace = struct
  type trans = { gas_left : int }

  type t = trans list
end

module RunEffect = struct
  type t = { storage : string; gas : string }

  module Repr = struct
    let pretty t = "New storage: " ^ t.storage ^ "\nGas consumed: " ^ t.gas
  end
end

module TraceResult = struct
  type t = { storage : string; trace : Trace.t }

  module Decode = struct end
end
