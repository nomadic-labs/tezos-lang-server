type point = { line : int; col : int (* point: int; *) }

type range = { start : point; stop : point }

type t = Original of range | Expanded of range

let point_zero = { line = 0; col = 0 }

let range_max =
  { start = point_zero; stop = { line = 100000000; col = 10000000 } }

let max = Original range_max

let filter_expanded l =
  List.filter
    (fun (loc, _, _) -> match loc with Original _ -> true | _ -> false)
    l

module Lens = struct
  let range = function Original range | Expanded range -> range

  let start t = (range t).start

  let stop t = (range t).stop
end

let is_inside line col t =
  (Lens.start t).line <= line
  && (Lens.stop t).line >= line && (Lens.start t).col <= col
  && (Lens.stop t).col >= col

module Repr = struct
  let point p = Format.asprintf "(%d, %d)" p.line p.col

  let range r =
    Format.asprintf "start: %s | end: %s" (point r.start) (point r.stop)

  let t t = Lens.range t |> range
end

module Encode = struct
  open Data_encoding

  let point_encoding = obj2 (req "line" int31) (req "character" int31)

  let encoding = obj2 (req "start" point_encoding) (req "end" point_encoding)
end

let to_json t =
  let range = Lens.range t in
  Data_encoding.Json.construct
    Encode.encoding
    ((range.start.line, range.start.col), (range.stop.line, range.stop.col))

module Decode = struct
  let point (p : Micheline_parser.point) = { line = p.line - 1; col = p.column }

  let range (l : Micheline_parser.location) =
    { start = point l.start; stop = point l.stop }

  let from_micheline = function
    | (location, true) -> Expanded (range location)
    | (location, false) -> Original (range location)

  let point json =
    { line =
        Json.get_force "line" json |> Json.int
        |> Option.unopt_exn (Failure "not an int @ line")
        |> ( + ) 1;
      col =
        Json.get_force "character" json
        |> Json.int
        |> Option.unopt_exn (Failure "not a  int @ col")
        |> ( + ) 1
    }

  let range json =
    { start = Json.get_force "start" json |> point;
      stop = Json.get_force "stop" json |> point
    }

  let point_proto json =
    { line =
        Json.get_force "line" json |> Json.int
        |> Option.unopt_exn (Failure "not an int @ line");
      col =
        Json.get_force "character" json
        |> Json.int
        |> Option.unopt_exn (Failure "not a  int @ col")
    }

  let range_with_end json =
    { start = Json.get_force "start" json |> point_proto;
      stop = Json.get_force "end" json |> point_proto
    }
end
