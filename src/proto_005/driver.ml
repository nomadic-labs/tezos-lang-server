module Proto = Tezos_protocol_005_PsBabyM1.Protocol
module Context = Proto.Environment.Context
module Script = Proto.Alpha_context.Script
module Alpha_context = Proto.Alpha_context
module Gas = Alpha_context.Gas
module Delegate = Alpha_context.Delegate
module Fees = Alpha_context.Fees
module Apply = Apply
module Script_ir_translator = Proto.Script_ir_translator
module Michelson_v1_parser = Michelson_v1_parser
module Michelson_v1_printer = Michelson_v1_printer
module Michelson_primitives = Proto.Michelson_v1_primitives
module Michelson_v1_error_reporter = Michelson_v1_error_reporter
open Models

let genesis_block =
  Block_hash.of_b58check_exn
    "BLockGenesisGenesisGenesisGenesisGenesisf79b5d1CoW2"

let protocol_param_key = ["protocol_parameters"]

let bootstrap_balances = Array.make 5 1L

let make_accounts bootstrap_balances =
  let open Proto.Parameters_repr in
  [ { public_key_hash =
        Signature.Public_key_hash.of_b58check_exn
          "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx";
      public_key =
        Some
          (Signature.Public_key.of_b58check_exn
             "edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav");
      amount = Proto.Tez_repr.of_mutez_exn bootstrap_balances.(0)
    };
    { public_key_hash =
        Signature.Public_key_hash.of_b58check_exn
          "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN";
      public_key =
        Some
          (Signature.Public_key.of_b58check_exn
             "edpktzNbDAUjUk697W7gYg2CRuBQjyPxbEg8dLccYYwKSKvkPvjtV9");
      amount = Proto.Tez_repr.of_mutez_exn bootstrap_balances.(1)
    };
    { public_key_hash =
        Signature.Public_key_hash.of_b58check_exn
          "tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU";
      public_key =
        Some
          (Signature.Public_key.of_b58check_exn
             "edpkuTXkJDGcFd5nh6VvMz8phXxU3Bi7h6hqgywNFi1vZTfQNnS1RV");
      amount = Proto.Tez_repr.of_mutez_exn bootstrap_balances.(2)
    };
    { public_key_hash =
        Signature.Public_key_hash.of_b58check_exn
          "tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv";
      public_key =
        Some
          (Signature.Public_key.of_b58check_exn
             "edpkuFrRoDSEbJYgxRtLx2ps82UdaYc1WwfS9sE11yhauZt5DgCHbU");
      amount = Proto.Tez_repr.of_mutez_exn bootstrap_balances.(3)
    };
    { public_key_hash =
        Signature.Public_key_hash.of_b58check_exn
          "tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv";
      public_key =
        Some
          (Signature.Public_key.of_b58check_exn
             "edpkv8EUUH68jmo3f7Um5PezmfGrRF24gnfLpH3sVNwJnV5bVCxL2n");
      amount = Proto.Tez_repr.of_mutez_exn 8_000_000_000L
    } ]

module Helpers = struct
  let mutez_of_int amount =
    match Proto.Tez_repr.of_mutez @@ Int64.of_int amount with
    | Some amount -> return amount
    (* This seems sketchy *)
    | None -> Lwt.return @@ Error []
end

let initial_context (scripts : Contract.t list) boot_balances =
  let bootstrap_accounts = make_accounts boot_balances in
  let bootstrap_contract_of_script (script : Contract.t) =
    let bootstrap = List.hd bootstrap_accounts in
    Format.printf "%d@." script.balance ;
    Helpers.mutez_of_int script.balance >>=? fun amount ->
    let script =
      Data_encoding.Binary.to_bytes_exn Script.encoding script.script
      |> Data_encoding.Binary.of_bytes_exn Proto.Script_repr.encoding
    in
    return
    @@ Proto.Parameters_repr.
         { delegate = bootstrap.public_key_hash; amount; script }
  in
  map_s bootstrap_contract_of_script scripts >>=? fun bootstrap_contracts ->
  let constants =
    { Proto.Constants_repr.preserved_cycles = 5;
      blocks_per_cycle = 4096l;
      blocks_per_commitment = 32l;
      blocks_per_roll_snapshot = 256l;
      blocks_per_voting_period = 32768l;
      time_between_blocks = List.map Proto.Period_repr.of_seconds_exn [60L; 75L];
      endorsers_per_block = 32;
      hard_gas_limit_per_operation = Z.of_int 800_000;
      hard_gas_limit_per_block = Z.of_int 8_000_000;
      proof_of_work_threshold = Int64.(sub (shift_left 1L 46) 1L);
      tokens_per_roll = Proto.Tez_repr.(mul_exn one 8_000);
      michelson_maximum_type_size = 1000;
      seed_nonce_revelation_tip =
        ( match Proto.Tez_repr.(one /? 8L) with
        | Ok c -> c
        | Error _ -> assert false );
      origination_size = 257;
      block_security_deposit = Proto.Tez_repr.(mul_exn one 512);
      endorsement_security_deposit = Proto.Tez_repr.(mul_exn one 64);
      block_reward = Proto.Tez_repr.(mul_exn one 16);
      endorsement_reward = Proto.Tez_repr.(mul_exn one 2);
      hard_storage_limit_per_operation = Z.of_int 60_000;
      cost_per_byte = Proto.Tez_repr.of_mutez_exn 1_000L;
      test_chain_duration = Int64.mul 32768L 60L;
      (* Those are bogus data - We should not need true values for our purposes *)
      quorum_min = Int32.zero;
      quorum_max = Int32.one;
      min_proposal_quorum = Int32.one;
      delay_per_missing_endorsement = Proto.Period_repr.one_minute;
      initial_endorsers = 0
    }
  in
  let dummy_shell_header =
    { Tezos_base.Block_header.level = 0l;
      predecessor = genesis_block;
      timestamp = Time.Protocol.epoch;
      fitness = Proto.Fitness_repr.from_int64 0L;
      operations_hash = Operation_list_list_hash.zero;
      proto_level = 0;
      validation_passes = 0;
      context = Context_hash.zero
    }
  in
  let json =
    Data_encoding.Json.construct
      Proto.Parameters_repr.encoding
      Proto.Parameters_repr.
        { bootstrap_accounts;
          bootstrap_contracts;
          commitments = [];
          constants;
          security_deposit_ramp_up_cycles = None;
          no_reward_cycles = None
        }
  in
  let proto_params =
    Data_encoding.Binary.to_bytes_exn Data_encoding.json json
  in
  Tezos_protocol_environment.Context.(
    set Memory_context.empty ["version"] (MBytes.of_string "genesis"))
  >>= fun ctxt ->
  Tezos_protocol_environment.Context.(set ctxt protocol_param_key proto_params)
  >>= fun ctxt ->
  Proto.Main.init ctxt dummy_shell_header >|= Proto.Environment.wrap_error
  >>=? fun { context; _ } -> return context

let prepare_context ~(contracts : Contract.t list) boot_balances ~timestamp =
  initial_context contracts boot_balances >>=? fun context ->
  let timestamp =
    match Time.Protocol.of_notation timestamp with
    | Some stamp -> stamp
    | None -> Time.System.to_protocol (Tezos_stdlib_unix.Systime_os.now ())
  in
  Proto.Alpha_context.prepare
    context
    ~level:0l
    ~timestamp
    ~predecessor_timestamp:timestamp
    ~fitness:[]
  >|= Proto.Environment.wrap_error
  >>=? fun context ->
  let ctxt =
    Proto.Alpha_context.Contract.init_origination_nonce
      context
      Operation_hash.zero
  in
  return ctxt

let originate ~(contracts : Contract.unbuilt list) ~timestamp boot_balances =
  Contract.build contracts >>=? fun contracts ->
  prepare_context ~contracts ~timestamp boot_balances >>=? fun context ->
  return @@ (context, contracts)

let expand_expr ~expr =
  ( match Michelson_v1_parser.parse_expression expr with
  | (param, []) -> Error_monad.return param.expanded
  | (_, errl) -> Error_monad.fail @@ List.hd errl )
  >>=? fun expr ->
  return @@ Format.asprintf "%a" Michelson_v1_printer.print_expr expr

let indent ~script =
  let parsed = Michelson_v1_parser.parse_toplevel script in
  let ast = fst parsed in
  let unexpanded = ast.unexpanded in
  let indented =
    Format.asprintf "%a" Micheline_printer.print_expr
    @@ Micheline_printer.printable
         ~comment:(fun _ -> None)
         (fun token -> token)
         unexpanded
  in
  (* Need to unwrap the toplevel code*)
  String.split_on_char '\n' indented
  |> List.map (fun s -> String.sub s 2 (String.length s - 2))
  |> String.concat "\n"
  |> (fun s -> String.sub s 0 (String.length s - 2))
  |> return

module Transaction = struct
  type t =
    { script : Script.t;
      self : Proto.Alpha_context.Contract.t;
      source : Alpha_context.Contract.t;
      payer : Alpha_context.Contract.t;
      parameter : Script.expr;
      amount : Proto.Alpha_context.Tez.t;
      entrypoint : string
    }

  type unbuilt =
    { param : string;
      gas : int;
      amount : int;
      self : string;
      payer : string;
      entrypoint : string
    }

  let unbuilt_of_tuple (param, gas, amount, self, payer, entrypoint) =
    { param; gas; amount; self; payer; entrypoint }

  let build (contracts : Contract.unbuilt list) unbuilt =
    Contract.parse_addr unbuilt.self >>=? fun self ->
    let (code, storage) =
      List.find (fun (c : Contract.unbuilt) -> c.addr = unbuilt.self) contracts
      |> fun (c : Contract.unbuilt) -> (c.code, c.storage)
    in
    Contract.parse_script code storage >>=? fun script ->
    Contract.parse_addr unbuilt.payer >>=? fun payer ->
    ( match Michelson_v1_parser.parse_expression unbuilt.param with
    | (param, []) -> return param.expanded
    | (_, errl) -> Lwt.return @@ Error errl )
    >>=? fun parameter ->
    ( match Alpha_context.Tez.of_mutez @@ Int64.of_int unbuilt.amount with
    | Some amount -> return amount
    | None -> Lwt.return @@ Error [] )
    >>=? fun amount ->
    return
      { script;
        self;
        source = payer;
        payer;
        parameter;
        amount;
        entrypoint = unbuilt.entrypoint
      }

  let to_internal_operation (t : t) nonce =
    let open Alpha_context in
    let operation =
      Transaction
        { amount = t.amount;
          parameters = Script.lazy_expr t.parameter;
          destination = t.self;
          entrypoint = t.entrypoint
        }
    in
    let internal_op = { source = t.source; operation; nonce } in
    Internal_operation internal_op
end

type t =
  { script : Script.t;
    self : Proto.Alpha_context.Contract.t;
    source : Alpha_context.Contract.t;
    payer : Alpha_context.Contract.t;
    parameter : Script.expr;
    amount : Proto.Alpha_context.Tez.t
  }

type status =
  | Success of Proto.Alpha_context.t * OperationResult.t
  | Failed of error list
  | Backtracked of error list option
  | Skipped

(* [@@@ocaml.warning "-26"] *)
(* [@@@ocaml.warning "-27"] *)
(* [@@@ocaml.warning "-21"] *)
(* [@@@ocaml.warning "-8"] *)
module Operation = struct
  type kind =
    | Call of Alpha_context.t * Transaction.t
    | Other of Alpha_context.t

  let build_transaction ~context ~operation ~contracts ~payer ~entrypoint =
    match operation with
    | Alpha_context.Internal_operation internal_op -> (
        match internal_op.operation with
        | Transaction trans ->
            let parameter = trans.parameters in
            let addr = Alpha_context.Contract.to_b58check trans.destination in
            if String.has_prefix ~prefix:"KT1" addr then
              let source = internal_op.source in
              let amount = trans.amount in
              let contract =
                List.find (fun (c : Contract.t) -> c.addr = addr) contracts
              in
              Alpha_context.Script.force_decode context parameter
              >|= Proto.Environment.wrap_error
              >>=? fun (parameter, context) ->
              return
              @@ Call
                   ( context,
                     { script = contract.script;
                       self = contract.self;
                       source;
                       payer;
                       parameter;
                       amount;
                       entrypoint
                     } )
            else return @@ Other context
        | _ -> return @@ Other context )
end

(* Is a snapshot between two operations, or during one? *)
(* The answer is after *)

let apply ~context ~operation ~payer ~history =
  let open OperationResult in
  Apply.apply_internal_manager_operations
    context
    Proto.Script_ir_translator.Readable
    ~payer
    [operation]
    ~history

let loop ~context ~contracts ~operations ~payer ~chain_id ~history =
  let open Snapshot in
  let open OperationResult in
  match operations with
  | [] -> return (contracts, history)
  | operation :: _ ->
      (* Apply transaction here *)
      apply ~context ~operation ~payer ~chain_id ~history
      >|= Proto.Environment.wrap_error
      >>=? fun res -> return (contracts, res)

open Proto.Apply_results

let entrypoint ~(contracts : (string * string * string * int) list)
    ~(params : string * int * int * string * string * string) ~boot_balances
    ~timestamp =
  let history = ref [] in
  let contracts = List.map Contract.unbuilt_of_tuple contracts in
  (let params = Transaction.unbuilt_of_tuple params in
   Transaction.build contracts params
   (* This is the loop here *)
   >>=? fun transaction ->
   originate ~contracts (Array.map Int64.of_int boot_balances) ~timestamp
   >>=? fun (context, contracts) ->
   let gas =
     if params.gas < 8_000_000 then Z.of_int params.gas else Z.of_int 8_000_000
   in
   let context = Proto.Alpha_context.Gas.set_limit context gas in
   Lwt.return @@ Proto.Alpha_context.fresh_internal_nonce context
   >|= Proto.Environment.wrap_error
   >>=? fun (context, nonce) ->
   let internal_op = Transaction.to_internal_operation transaction nonce in
   loop
     ~context
     ~contracts
     ~operations:[internal_op]
     ~payer:transaction.payer
     ~chain_id:Chain_id.zero
     ~history)
  >>= function
  | Ok (contracts, history) ->
      (* Format.printf "%s@." "All went fine."; *)
      return @@ Michelson_v1_json.construct_history ~contracts history
  | Error errs ->
      (* Format.printf "%s@." "Whoops, it all went wrong"; *)
      List.iter (Format.printf "%a@." Error_monad.pp) errs ;
      (* return msg *)
      (* let formatfun fmt  = (Michelson_v1_error_reporter.report_errors fmt ~details:false ~show_source:true) in
         Format.printf "%a@."
           formatfun
           errs; *)
      let errs = List.map Error_monad.json_of_error errs in
      return @@ `A errs

(* return @@ Data_encoding.Json.construct Data_encoding.int31 14 *)
(* Format.printf "%a@."Error_monad.pp_print_error errs; *)

let constant_context =
  prepare_context ~contracts:[] bootstrap_balances ~timestamp:"none"

let parse ~script =
  let parsed = Michelson_v1_parser.parse_toplevel ~check:true script in
  let ast = fst parsed in
  ast

let typecheck_from_ast ~ast =
  (* prepare_context ~contracts:[] boot_balances ~timestamp:"none" *)
  constant_context >>=? fun context ->
  Proto.Script_ir_translator.typecheck_code
    context
    ast.Michelson_v1_parser.expanded
  >|= Proto.Environment.wrap_error
  >>=? fun (typemap, _) ->
  let typemap = Michelson_v1_json.update_typemap ast typemap in
  return typemap

let typecheck_code ~script =
  let ast = parse ~script in
  typecheck_from_ast ~ast

let typecheck_code_json ~script =
  let ast = parse ~script in
  typecheck_from_ast ~ast >>= function
  | Error errors ->
      (* Format.printf "KO@."; *)
      let json = Michelson_v1_json.json_of_errors ast errors in
      return json
  | Ok typemap ->
      (* Format.printf "OK@."; *)
      let json =
        Data_encoding.Json.construct Michelson_v1_json.Encodings.typemap typemap
      in
      return json

let typecheck ~script ~storage =
  let parsed = Michelson_v1_parser.parse_toplevel ~check:true script in
  let ast = fst parsed in
  (let boot_balances = bootstrap_balances in
   prepare_context ~contracts:[] boot_balances ~timestamp:"none"
   >>=? fun context ->
   Contract.parse_script script storage >>=? fun script ->
   Proto.Script_ir_translator.parse_script ~legacy:false context script
   >|= Proto.Environment.wrap_error
   >>=? fun _ ->
   Proto.Script_ir_translator.typecheck_code context ast.expanded
   >|= Proto.Environment.wrap_error
   >>=? fun (typemap, _context) ->
   Lwt.return @@ Script_ir_translator.parse_toplevel ~legacy:false ast.expanded
   >|= Proto.Environment.wrap_error
   >>=? fun (arg_type, _, _, root_name) ->
   Lwt.return
   @@ Script_ir_translator.parse_ty
        context
        ~legacy:false
        ~allow_big_map:true
        ~allow_operation:false
        ~allow_contract:true
        arg_type
   >|= Proto.Environment.wrap_error
   >>=? fun (Ex_ty arg_type, _) ->
   Lwt.return
   @@ Script_ir_translator.list_entrypoints ~root_name arg_type context
   >|= Proto.Environment.wrap_error
   >>=? fun (unreachable_entrypoint, map) ->
   let entrypoints =
     Script_ir_translator.Entrypoints_map.fold
       (fun entry (_, ty) acc -> (entry, Micheline.strip_locations ty) :: acc)
       map
       []
   in
   let entrypoints = List.map fst entrypoints in
   let unreachable_entrypoint =
     List.flatten unreachable_entrypoint
     |> List.map Proto.Michelson_v1_primitives.string_of_prim
   in
   return @@ (typemap, unreachable_entrypoint, entrypoints))
  >>= function
  | Error errors ->
      Format.printf "KO@." ;
      let json = Michelson_v1_json.json_of_errors ast errors in
      return json
  | Ok (type_map, _unreachable_entrypoint, entrypoints) ->
      Format.printf "OK@." ;
      Format.printf "entrypoints:@." ;
      List.iter print_endline entrypoints ;
      Format.printf "unreachable entrypoints:@." ;
      List.iter print_endline _unreachable_entrypoint ;
      let json = Michelson_v1_json.typecheck_output ast type_map entrypoints in
      return json

let json_of_contract ~script =
  Michelson_v1_parser.parse_toplevel ~check:true script
  |> fst
  |> (fun { unexpanded; _ } -> unexpanded)
  |> Data_encoding.Json.construct
       (Micheline.canonical_encoding ~variant:"test" Data_encoding.string)
  |> return

module Rand = struct
  let ascii_byte () : char =
    let (min, max) = (32, 126) in
    Char.chr @@ ((Random.int @@ (max - min)) + min)

  let gen_string ~size =
    let (min, max) = size in
    let len = (Random.int @@ (max - min)) + min in
    String.init len (fun _ -> ascii_byte ())
end

module Base_sampler : Randgen.Base_samplers_S = struct
  open Randgen

  let int = int

  let nat = nat

  let string = Rand.gen_string

  let bytes = bytes

  let signature _ =
    Signature.of_string_exn
      "edsigtkpiSSschcaCt9pUVrpNPf7TTcgvgDEDD6NCEHMy8NNQJCGnMfLZzYoQj74yLjo9wx6MPVV29CvVzgi7qEcEUok3k7AuMg"

  let tez = tez

  let timestamp = timestamp

  let bool = bool

  let key_pool_size = 100
end

module Gen = struct
  module Snoop = Randgen.Make (Base_sampler)

  let _ = Random.self_init ()

  let gen_val ~(ty : string) =
    let sampler_parameters =
      Randgen.
        { int_size = (1, 3);
          string_size = (3, 7);
          bytes_size = (8, 8);
          stack_size = (0, 4);
          michelson_type_depth = (1, 1);
          list_size = (2, 4);
          set_size = (2, 4);
          map_size = (2, 4)
        }
    in
    print_endline ty ;
    prepare_context ~contracts:[] bootstrap_balances ~timestamp:"none"
    >>=? fun context ->
    let ast =
      Michelson_v1_parser.parse_expression ty |> fst |> fun a -> a.expanded
    in
    let node = Micheline.root ast in
    Lwt.return
    @@ Proto.Script_ir_translator.parse_ty
         context
         ~legacy:false
         ~allow_big_map:true
         ~allow_operation:true
         ~allow_contract:true
         node
    >|= Proto.Environment.wrap_error
    >>=? fun (Ex_ty ty, _) ->
    let randval = Snoop.Random_value.value sampler_parameters ty in
    Proto.Script_ir_translator.unparse_data
      context
      Proto.Script_ir_translator.Readable
      ty
      randval
    >|= Proto.Environment.wrap_error
    >>=? fun (node, _) ->
    let node = Micheline.strip_locations node in
    let node =
      Micheline_printer.printable
        Proto.Michelson_v1_primitives.string_of_prim
        node
    in
    let value = Format.asprintf "%a" Micheline_printer.print_expr node in
    print_endline value ;
    return value

  let wrap s = "(" ^ s ^ ")"

  let rec get_node_names node =
    let open Micheline in
    match node with
    | Prim (_, name, nodes, _) ->
        (List.map get_node_names nodes |> String.concat " " |> function
         | "" -> ""
         | otherwise -> otherwise)
        |> fun s -> wrap @@ name ^ " " ^ s
    | _ -> ""

  type tys = { storage_ty : string; param_ty : string }

  let rec traverse_nodes acc node =
    let open Micheline in
    match node with
    | Prim (_, name, nodes, _) -> (
        match name with
        | "parameter" ->
            let ty =
              List.fold_left
                (fun s node -> s ^ " " ^ get_node_names node)
                ""
                nodes
            in
            { acc with param_ty = wrap ty }
        | "storage" ->
            let ty =
              List.fold_left
                (fun s node -> s ^ " " ^ get_node_names node)
                ""
                nodes
            in
            { acc with storage_ty = wrap ty }
        | _ -> acc )
    | Seq (_, nodes) -> List.fold_left traverse_nodes acc nodes
    | _ -> acc

  let gen_param_storage ~code =
    let parsed = Michelson_v1_parser.parse_toplevel ~check:true code in
    let ast = fst parsed in
    let node = ast.expanded in
    let node = Micheline.root node in
    let node =
      Micheline.map_node
        (fun a -> a)
        Proto.Michelson_v1_primitives.string_of_prim
        node
    in
    let acc = traverse_nodes { storage_ty = ""; param_ty = "" } node in
    gen_val ~ty:acc.storage_ty >>=? fun storage ->
    gen_val ~ty:acc.param_ty >>=? fun parameter ->
    return
    @@ Data_encoding.Json.construct
         Michelson_v1_json.Encodings.param_storage_tuple
         (storage, parameter)
end
