module Proto = Tezos_protocol_005_PsBabyM1.Protocol
module Context = Proto.Environment.Context
module Alpha_context = Proto.Alpha_context
module Script = Alpha_context.Script
module Michelson_v1_parser = Tezos_client_005_PsBabyM1.Michelson_v1_parser

module Contract = struct
  type t = {
    script : Script.t;
    self : Proto.Alpha_context.Contract.t;
    balance : int;
    ast : Michelson_v1_parser.parsed;
    addr : string;
  }

  type unbuilt = {
    addr : string;
    code : string;
    storage : string;
    balance : int;
  }

  let unbuilt_of_tuple (addr, code, storage, balance) =
    { addr; code; storage; balance }

  let parse_code code =
    match Michelson_v1_parser.parse_toplevel code with
    | ast, [] -> return ast
    | _, errl -> Lwt.return @@ Error errl

  let parse_storage storage =
    match Michelson_v1_parser.parse_expression storage with
    | storage, [] -> return storage.expanded
    | _, errl -> Lwt.return @@ Error errl

  let parse_addr addr =
    match Proto.Alpha_context.Contract.of_b58check addr with
    | Ok self -> return self
    | Error _ -> assert false

  let parse_script code storage =
    parse_code code >>=? fun code ->
    parse_storage storage >>=? fun storage ->
    return
      {
        Script.code = Script.lazy_expr code.expanded;
        storage = Script.lazy_expr storage;
      }

  let parse_contract unbuilt =
    parse_script unbuilt.code unbuilt.storage >>=? fun script ->
    parse_addr unbuilt.addr >>=? fun self ->
    parse_code unbuilt.code >>=? fun ast ->
    return { script; balance = unbuilt.balance; addr = unbuilt.addr; self; ast }

  let build contracts = map_s parse_contract contracts
end

module OperationResult = struct
  type call_info = {
    addr : string;
    result : Proto.Script_interpreter.execution_result;
    trace : Proto.Script_interpreter.execution_trace;
    storage : Script.expr;
  }

  type t =
    | RevealResult of { consumed_gas : int64 }
    | TransactionResult of {
        balance_updates : Alpha_context.Delegate.balance_updates;
        consumed_gas : int64;
        paid_storage_size_diff : int64;
        storage_size : int64;
        call_info : call_info option;
      }
    | OriginationResult of {
        balance_updates : Alpha_context.Delegate.balance_updates;
        originated_contracts : Alpha_context.Contract.t list;
        consumed_gas : int64;
        storage_size : int64;
        paid_storage_size_diff : int64;
      }
    | DelegationResult of { consumed_gas : int64 }

  type trace_result =
    | CallSuccess of
        Contract.t
        * Proto.Script_interpreter.execution_result
        * Proto.Script_interpreter.execution_trace
    | CallFailed of Contract.t * error list

  type call_result = Call of trace_result | Other

  module Encodings = struct
    open Data_encoding

    let reveal_result_encoding =
      obj2 (req "kind" (constant "reveal")) (req "consumed_gas" int64)

    let transaction_result_encoding =
      obj5
        (req "kind" (constant "transaction"))
        (req "balance_updates" Alpha_context.Delegate.balance_updates_encoding)
        (req "consumed_gas" int64)
        (req "paid_storage_size_diff" int64)
        (req "storage_size" int64)

    let origination_result_encoding =
      obj6
        (req "kind" (constant "origination"))
        (req "balance_updates" Alpha_context.Delegate.balance_updates_encoding)
        (req "originated_contracts" @@ list Alpha_context.Contract.encoding)
        (req "consumed_gas" int64) (req "storage_size" int64)
        (req "paid_storage_size_diff" int64)

    let delegation_result_encoding =
      obj2 (req "kind" (constant "delegation")) (req "consumed_gas" int64)
  end
end

module Snapshot = struct
  type status = Success of OperationResult.t | Failed of error list | Skipped

  type operation_snapshot =
    | Traced of OperationResult.trace_result
    | UntraceableOp of status

  type t = {
    pending_operations : Alpha_context.packed_internal_operation list;
    current : Alpha_context.packed_internal_operation * status;
  }

  module Encodings = struct
    open Data_encoding

    let skipped_encoding = obj1 (req "status" (constant "skipped"))

    let failed_encoding =
      obj2 (req "status" (constant "failed")) (req "errs" string)
  end
end
