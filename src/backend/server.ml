let log = print_endline

module Log = struct
  let log service fmt =
    Format.printf "call on %s@." service;
    Format.printf fmt
end

module type DriverSig = sig
  val entrypoint :
    contracts:(string * string * string * int) list ->
    params:string * int * int * string * string * string ->
    boot_balances:int array ->
    timestamp:string ->
    Data_encoding.Json.t tzresult Lwt.t

  val indent : script:string -> string tzresult Lwt.t

  val json_of_contract : script:string -> Data_encoding.Json.t tzresult Lwt.t

  val typecheck :
    script:string -> storage:string -> Data_encoding.Json.t tzresult Lwt.t

  val typecheck_code_json : script:string -> Data_encoding.Json.t tzresult Lwt.t

  val gen_attributes_json : code:string -> Data_encoding.Json.t tzresult Lwt.t
end

module Drivers = struct
  let drivers = [("006", (module Driver_006.Driver : DriverSig))]

  let get protocol =
    match protocol with
    | None ->
        log "no protocol found, using carthage";
        (module Driver_006.Driver : DriverSig)
    | Some protocol -> (
        match List.assoc_opt protocol drivers with
        | Some driver -> driver
        | None ->
            log "no protocol found, using carthage";
            (module Driver_006.Driver))
end

module Service = struct
  module Encodings = struct
    open Data_encoding

    let trace_request_encoding =
      merge_objs
        (obj4
           (req "boot_balances" @@ array ~max_length:4 int31)
           (req "timestamp" string)
           (opt "protocol" string)
           (dft "entrypoint" string "default"))
        (obj10
           (req "amount" int31)
           (req "balance" int31)
           (req "code" string)
           (req "gas" int31)
           (req "parameter" string)
           (req "storage" string)
           (req "self" string)
           (req "source" string)
           (req "payer" string)
           (req
              "contracts"
              (list
              @@ obj4
                   (req "addr" string)
                   (req "code" string)
                   (req "storage" string)
                   (req "balance" int31))))

    let typecheck_request =
      obj3 (req "code" string) (req "storage" string) (opt "protocol" string)

    let typecheck_code = obj2 (req "code" string) (opt "protocol" string)

    let jsonify = obj2 (req "code" string) (opt "protocol" string)

    let random_value = obj1 (req "ty" string)

    (* LSP encodings *)

    let request_msg =
      obj3 (req "jsonrpc" string) (req "id" int64) (req "method" string)

    let initialize =
      merge_objs
        request_msg
        (obj1 (req "params" (obj1 (req "processId" int64))))
  end

  let hello =
    RPC_service.get_service
      ~description:"Say hello"
      ~output:Data_encoding.unit
      ~query:RPC_query.empty
      RPC_path.(root / "hello")

  let expand =
    RPC_service.post_service
      ~description:"Expand expression"
      ~input:Data_encoding.string
      ~output:Data_encoding.string
      ~query:RPC_query.empty
      RPC_path.(root / "expand")

  let jsonify =
    RPC_service.post_service
      ~description:"Get the Json corresponding to a contract"
      ~input:Encodings.jsonify
      ~output:Data_encoding.Json.encoding
      ~query:RPC_query.empty
      RPC_path.(root / "jsonify")

  let indent =
    RPC_service.post_service
      ~description:"Indent code"
      ~input:Data_encoding.string
      ~output:Data_encoding.string
      ~query:RPC_query.empty
      RPC_path.(root / "indent")

  let random_parameter_storage =
    RPC_service.post_service
      ~description:"Generate a random value for parameter and storage"
      ~input:Data_encoding.string
      ~output:Data_encoding.Json.encoding
      ~query:RPC_query.empty
      RPC_path.(root / "rand_param_storage")

  let random_value =
    RPC_service.post_service
      ~description:"Generate a random value of a given type"
      ~input:Data_encoding.string
      ~output:Data_encoding.string
      ~query:RPC_query.empty
      RPC_path.(root / "random_value")

  let trace =
    RPC_service.post_service
      ~description:"Trace execution of a contract"
      ~input:Encodings.trace_request_encoding
      ~output:Data_encoding.Json.encoding
      ~query:RPC_query.empty
      RPC_path.(root / "get_trace")

  let typecheck =
    RPC_service.post_service
      ~description:"Typecheck contract. @returns a typemap and indented code"
      ~input:Encodings.typecheck_request
      ~output:Data_encoding.Json.encoding
      ~query:RPC_query.empty
      RPC_path.(root / "typecheck")

  let typecheck_code =
    RPC_service.post_service
      ~description:"Typecheck the code of a contract."
      ~input:Encodings.typecheck_code
      ~output:Data_encoding.Json.encoding
      ~query:RPC_query.empty
      RPC_path.(root / "typecheck_code")

  let lsp =
    RPC_service.post_service
      ~input:Data_encoding.Json.encoding
      ~output:Data_encoding.Json.encoding
      ~query:RPC_query.empty
      RPC_path.(root / "lsp")
end

module Directory = RPC_directory

module Dir = struct
  let dir = Directory.empty

  let test =
    Directory.register0 dir Service.lsp (fun () json ->
        Log.log "test/" "%a" Data_encoding.Json.pp json;
        return json)

  let hello =
    Directory.register0 dir Service.hello (fun () () ->
        Log.log "hello/" "";
        return ())

  let expand =
    Directory.register0 hello Service.expand (fun () expr ->
        Log.log "expand/" "%s" expr;
        Driver_006.Driver.expand_expr ~expr)

  let jsonify =
    Directory.register0 expand Service.jsonify (fun () (script, protocol) ->
        Log.log
          "jsonify/"
          "protocol:%s@.script:%s@."
          (Option.unopt ~default:"None" protocol)
          script;
        let (module Driver) = Drivers.get protocol in
        Driver.json_of_contract ~script)

  let random_parameter_storage =
    Directory.register0 jsonify Service.random_parameter_storage (fun () code ->
        Log.log "random_parameter_storage/" "%s" code;
        let (module Driver) = Drivers.get None in
        Driver.gen_attributes_json ~code)

  let random_value =
    Directory.register0
      random_parameter_storage
      Service.random_value
      (fun () ty ->
        Log.log "random_value/" "%s" ty;
        Driver_006.Gen.gen_val ~ty)

  let trace =
    Directory.register0
      random_value
      Service.trace
      (fun
        ()
        ( (boot_balances, timestamp, protocol, entrypoint),
          ( amount,
            _balance,
            _code,
            gas,
            param,
            _storage,
            self,
            _source,
            payer,
            contracts ) )
      ->
        let string_of_contract (a, b, c, d) =
          Format.asprintf "%s %s %s %d" a b c d
        in
        Log.log
          "trace/"
          "boot_balances: %s@.timestamp: %s@.protocol: %s@. entrypoint: %s@. \
           amount: %d@. gas: %d@. param: %s@. self: %s@. payer: %s@. \
           contracts:@.  %s"
          (Array.map string_of_int boot_balances
          |> Array.to_list |> String.concat " | ")
          timestamp
          (Option.unopt ~default:"None" protocol)
          entrypoint
          amount
          gas
          param
          self
          payer
          (contracts |> List.map string_of_contract |> String.concat "\n  ");
        let (module Driver) = Drivers.get protocol in
        Driver.entrypoint
          ~contracts
          ~params:(param, gas, amount, self, payer, entrypoint)
          ~boot_balances
          ~timestamp)

  let typecheck =
    Directory.register0
      trace
      Service.typecheck
      (fun () (script, storage, protocol) ->
        Log.log
          "typecheck/"
          "script: %s@. storage: %s@. protocol: %s@."
          script
          storage
          (Option.unopt ~default:"None" protocol);
        let (module Driver) = Drivers.get protocol in
        Driver.typecheck ~script ~storage)

  let typecheck_code =
    Directory.register0
      typecheck
      Service.typecheck_code
      (fun () (script, protocol) ->
        Log.log
          "typecheck_code/"
          "%s %s"
          script
          (Option.unopt ~default:"None" protocol);
        let (module Driver) = Drivers.get protocol in
        Driver.typecheck_code_json ~script)

  let indent =
    Directory.register0 typecheck_code Service.indent (fun () script ->
        Log.log "indent/" "%s" script;
        Driver_006.Driver.indent ~script)
end

let launch () =
  log "launching server";
  let port = 8282 in
  let cors =
    RPC_server.{allowed_headers = ["content-type"]; allowed_origins = ["*"]}
  in
  let mode = `TCP (`Port port) in
  Format.printf "Running on port %d@." port;
  RPC_server.launch
    ~host:"0.0.0.0"
    ~media_types:Media_type.all_media_types
    ~cors
    mode
    Dir.indent
  >>= fun _server -> fst (Lwt.wait ())

let lwt_launch () = Lwt_main.run (launch ())
