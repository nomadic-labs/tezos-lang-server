open Extdlib
module Em = Tezos_error_monad.Error_monad

type Em.error += Unterminated_string

type Em.error += Lexing_failure of string

let ( >>=? ) = Em.( >>=? )

let () =
  let open Em in
  register_error_kind
    `Permanent
    ~id:"unterminated_string"
    ~title:"String was not terminated."
    ~description:"While parsing a command line, a line was not terminated"
    ~pp:(fun ppf _ -> Format.fprintf ppf "Unterminated string.")
    Data_encoding.empty
    (function Unterminated_string -> Some () | _ -> None)
    (fun () -> Unterminated_string) ;
  register_error_kind
    `Permanent
    ~id:"lexing-failure"
    ~title:"Lexing failure"
    ~description:
      "While lexing a command line, something unexpected was encountered."
    ~pp:(fun ppf s ->
      Format.fprintf ppf "Lexing Failure: %s was not expected" s)
    Data_encoding.(obj1 (req "value" string))
    (function Lexing_failure s -> Some s | _ -> None)
    (fun s -> Lexing_failure s)

exception Unterminated_string_exc

exception Lexing_failure_exc of string

type token = Space | Word of string | String of string

let string_of_token = function
  | Space -> "SPACE"
  | Word w -> "WORD [" ^ w ^ "]"
  | String s -> "STRING [" ^ s ^ "]"

type lex_info =
  { start : int;
    current : int;
    line : string;
    char : char;
    tokens : token list
  }

let at_end lex_info = lex_info.current >= String.length lex_info.line

let advance lex_info =
  let char = lex_info.line.[lex_info.current] in
  let current = lex_info.current + 1 in
  { lex_info with current; char }

let peek lex_info =
  if at_end lex_info then '\000' else lex_info.line.[lex_info.current]

let match_ lex_info expected =
  if at_end lex_info then (lex_info, false)
  else if lex_info.line.[lex_info.current] <> expected then (lex_info, false)
  else (advance lex_info, true)

let rec lex_string lex_info =
  match peek lex_info with
  | '\\' -> lex_string @@ advance @@ advance lex_info
  | '"' ->
      let str = String.sub_pos lex_info.line lex_info.start lex_info.current in
      let str = Stringext.replace_all str ~pattern:"\\\"" ~with_:"\"" in
      advance { lex_info with tokens = String str :: lex_info.tokens }
  | '\000' -> raise Unterminated_string_exc
  | _ -> lex_string @@ advance lex_info

let rec lex_ident lex_info =
  match peek lex_info with
  | c when String.is_allowed c -> lex_ident @@ advance lex_info
  | _ ->
      let payload =
        String.sub_pos lex_info.line lex_info.start lex_info.current
      in
      { lex_info with tokens = Word payload :: lex_info.tokens }

let lex input =
  let rec lex lex_info =
    if at_end lex_info then lex_info
    else
      let lex_info = advance lex_info in
      match lex_info.char with
      | '"' -> lex @@ lex_string { lex_info with start = lex_info.current }
      | ' ' -> lex { lex_info with tokens = Space :: lex_info.tokens }
      | c when String.is_allowed c ->
          lex @@ lex_ident { lex_info with start = lex_info.current - 1 }
      | '\000' -> lex_info
      | _ as c -> raise (Lexing_failure_exc (Format.asprintf "%c" c))
  in
  let lex_info =
    lex { start = 0; current = 0; line = input; char = ' '; tokens = [] }
  in
  List.rev lex_info.tokens

let split line =
  match lex line with
  | tokens ->
      (* List.iter (fun tk -> Format.printf "%s @." (string_of_token tk)) tokens ; *)
      Em.return
      @@ List.filter_map
           (function Space -> None | Word w | String w -> Some w)
           tokens
  | exception Lexing_failure_exc s -> Lwt.return @@ Em.error @@ Lexing_failure s
  | exception Unterminated_string_exc ->
      Lwt.return @@ Em.error Unterminated_string
