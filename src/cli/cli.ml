open Clic
module CurrDriver = Driver_006
module Em = Tezos_error_monad.Error_monad
module Driver = Driver_006.Driver
module Alpha_context = Tezos_raw_protocol_006_PsCARTHA__Alpha_context
module Context = Driver_006.Client_context
open Context

let ( >>= ) = Em.( >>= )

let ( >>=? ) = Em.( >>=? )

let return ctx v = v >>=? fun _ -> Em.return ctx

let wrap t v = v >>=? fun context -> Em.return @@ Context.update context t

let global_options = no_options

let group = { Clic.name = "mode"; title = "Mode commands" }

let sandbox_group = { Clic.name = "sandbox"; title = "Sandbox commands" }

let language_group = { Clic.name = "language"; title = "Language commands" }

let repl_group = { Clic.name = "repl"; title = "Repl commands" }

let string_param = Clic.parameter (fun _ s -> Em.return s)

let ustring_param = Clic.parameter (fun _ s -> Em.return s)

let default_addr = "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"

let filename_param =
  param ~name:"file" ~desc:"filename of the contract" string_param

let amount_param = param ~name:"amount" ~desc:"amount transferred" string_param

type alpha_context = Tezos_raw_protocol_006_PsCARTHA__Alpha_context.context

let arg_arg =
  default_arg
    ~doc:"Parameter of the contract call"
    ~long:"arg"
    ~placeholder:"value"
    ~default:"Unit"
    string_param

let storage_arg =
  default_arg
    ~doc:"Storage of the contract"
    ~long:"storage"
    ~placeholder:"value"
    ~default:"Unit"
    string_param

let entrypoint_arg =
  default_arg
    ~doc:"Entrypoint on which to call the contract on"
    ~long:"entrypoint"
    ~placeholder:"value"
    ~default:"default"
    string_param

let uentrypoint_arg =
  default_arg
    ~doc:"Entrypoint on which to call the contract on"
    ~long:"entrypoint"
    ~placeholder:"value"
    ~default:"default"
    ustring_param

let timestamp_arg =
  default_arg
    ~doc:"Set the current timestamp"
    ~long:"timestamp"
    ~placeholder:"value"
    ~default:"now"
    string_param

let set_timestamp =
  command
    ~group:sandbox_group
    ~desc:"Set NOW to specified timestamp"
    no_options
    ( prefixes ["set"; "timestamp"]
    @@ param ~name:"timestamp" ~desc:"timestamp to set now to" string_param
    @@ stop )
    (fun () timestamp cctx ->
      Driver.Timestamp.Cli.set_timestamp ~timestamp ~context:cctx.alpha
      |> wrap cctx)

let now =
  command
    ~group:sandbox_group
    ~desc:"prints the value of now"
    no_options
    (fixed ["now"])
    (fun () cctx ->
      Driver.Timestamp.Repr.print_now ~context:cctx.alpha ;
      Em.return cctx)

let create_account =
  command
    ~group:sandbox_group
    ~desc:"Creates an account of alias <name> and <funds> tokens"
    ( args1
    @@ default_arg
         ~doc:"Specify the funds of the account. Defaults to 8000"
         ~short:'f'
         ~default:"8000"
         ~long:"funds"
         ~placeholder:"value"
         string_param )
    ( prefixes ["create"; "account"]
    @@ param ~name:"name" ~desc:"account alias" string_param
    @@ stop )
    (fun funds name cctx ->
      Context.check_alias ~name cctx >>=? fun name ->
      (* make it tez *)
      let funds = funds ^ "000000" in
      Driver.Contrs.Cli.create_account ~name ~funds ~context:cctx.alpha
      >>=? fun (alpha, addr) ->
      let cctx = update alpha cctx in
      Context.add_account ~name ~addr cctx)

let list_accounts =
  command
    ~group:sandbox_group
    ~desc:"List known accounts"
    no_options
    (fixed ["list"; "accounts"])
    (fun () cctx ->
      Context.list_known_accounts cctx ;
      Em.return cctx)

let list_contracts =
  command
    ~group:sandbox_group
    ~desc:"List contracts"
    no_options
    (fixed ["list"; "contracts"])
    (fun () cctx -> Driver.Contrs.Cli.print_accounts ~cctx)

let transfer =
  command
    ~group:sandbox_group
    ~desc:"Transfer tokens from <source> to <destination>"
    (args2 arg_arg entrypoint_arg)
    ( prefixes ["transfer"]
    @@ amount_param
    @@ prefixes ["from"]
    @@ param ~name:"source" ~desc:"Spent contract" string_param
    @@ prefixes ["to"]
    @@ param ~name:"destination" ~desc:"Credited contract" string_param
    @@ stop )
    (fun (arg, entrypoint) amount source destination cctx ->
      Driver.Op.Cli.transfer
        ~parameter:arg
        ~amount
        ~source
        ~destination
        ~entrypoint
        ~cctx)

let originate =
  command
    ~group:sandbox_group
    ~desc:
      "originate a contract from <filename> putting <value> tokens on it wich \
       will be known as <name>"
    (args2
       storage_arg
       (default_arg
          ~doc:"Amount put in the balance of the originated contract"
          ~long:"amount"
          ~placeholder:"value"
          ~default:"0"
          string_param))
    ( prefixes ["originate"; "contract"]
    @@ param ~name:"name" ~desc:"alias of the contract" string_param
    @@ filename_param @@ stop )
    (fun (storage, amount) alias filename cctx ->
      print_endline storage ;
      Driver.Contrs.Cli.originate ~filename ~alias ~storage ~amount ~cctx)

let gen_value =
  command
    ~group:language_group
    ~desc:"Generate value of given type"
    no_options
    ( prefixes ["gen"; "value"; "of"; "type"]
    @@ param ~name:"ty" ~desc:"type" (parameter (fun _ s -> Em.return s))
    @@ stop )
    (fun () ty ctx -> Driver.gen_val ty |> return ctx)

let gen_sample_parameter =
  command
    ~group:language_group
    ~desc:"Get sample parameter for script"
    (args1 uentrypoint_arg)
    ( prefixes ["gen"; "sample"; "parameter"; "of"]
    @@ param ~name:"script" ~desc:"file location" ustring_param
    @@ stop )
    (fun entrypoint filename ctx ->
      let entrypoint =
        match entrypoint with "default" -> None | s -> Some s
      in
      Driver.Gen.gen_arg ~filename ~entrypoint |> return ctx)

let generate_contract_attributes =
  command
    ~group:language_group
    ~desc:
      "Generate contract attributes (possible values of storage and parameter)"
    no_options
    ( prefixes ["gen"; "attributes"; "of"; "script"]
    @@ param ~name:"script" ~desc:"file location" ustring_param
    @@ stop )
    (fun () filename ctx -> Driver.Gen.gen_attributes ~filename |> return ctx)

let gen_rand_value =
  command
    ~group:language_group
    ~desc:"Generate random value of a given type"
    no_options
    ( prefixes ["gen"; "rand"; "value"; "of"; "type"]
    @@ param ~name:"ty" ~desc:"type" (parameter (fun _ s -> Em.return s))
    @@ stop )
    (fun () ty ctx -> Driver.Gen.gen_rand_val ty |> return ctx)

let run_script =
  command
    ~group:language_group
    ~desc:"Runs a script"
    (args9
       (default_arg
          ~doc:"Parameter of the contract call"
          ~long:"arg"
          ~placeholder:"value"
          ~default:"Unit"
          ustring_param)
       (default_arg
          ~doc:"Value of the contract's storage"
          ~long:"storage"
          ~placeholder:"value"
          ~default:"Unit"
          ustring_param)
       uentrypoint_arg
       (default_arg
          ~doc:"Timestamp at which the transaction happens"
          ~long:"timestamp"
          ~placeholder:"timestamp"
          ~default:"now"
          ustring_param)
       (default_arg
          ~doc:"Sender (if none is specified, will be bootstrap1)"
          ~long:"sender"
          ~placeholder:"value"
          ~default:default_addr
          ustring_param)
       (default_arg
          ~doc:"Payer. Defaults to bootstrap1."
          ~long:"payer"
          ~placeholder:"value"
          ~default:default_addr
          ustring_param)
       (default_arg
          ~doc:"Address of the contract. Defaults to bootstrap1."
          ~long:"self"
          ~placeholder:"value"
          ~default:default_addr
          ustring_param)
       (default_arg
          ~doc:
            "The amount transferred when the contract is called. Defaults to 0"
          ~long:"amount"
          ~placeholder:"value"
          ~default:"0"
          ustring_param)
       (arg
          ~doc:"Context filename on which to run the script"
          ~long:"context"
          ~placeholder:"filename"
          ustring_param))
    ( prefixes ["run"; "script"]
    @@ param ~name:"value" ~desc:"filename of the contract" ustring_param
    @@ stop )
    (fun ( parameter,
           storage,
           entrypoint,
           timestamp,
           sender,
           payer,
           self,
           amount,
           cctx_filename )
         filename
         cctx ->
      ( match cctx_filename with
      | Some filename -> Driver.Serial.load_context ~filename
      | None -> CurrDriver.Chain_env.empty_context )
      >>=? fun alpha ->
      Driver.run
        ~storage
        ~parameter
        ~filename
        ~timestamp
        ~sender
        ~payer
        ~entrypoint
        ~self
        ~amount
        ~context:alpha
      |> return cctx)

let save_context =
  command
    ~group:sandbox_group
    ~desc:"Save the context in a file on-disk"
    ( args1
    @@ arg
         ~doc:"filename"
         ~long:"filename"
         ~placeholder:"value"
         (parameter (fun _ s -> Em.return s)) )
    (fixed ["save"; "context"])
    (fun filename cctx ->
      Driver.Serial.Cli.write_cctx ~cctx ~filename |> return cctx)

let load_context =
  command
    ~group:sandbox_group
    ~desc:"Load the context from a file"
    no_options
    (prefixes ["load"; "context"; "from"] @@ filename_param @@ stop)
    (fun () filename _ -> Driver.Serial.Cli.load_cctx ~filename)

let sandbox_commands () =
  [ create_account;
    transfer;
    originate;
    list_accounts;
    list_contracts;
    now;
    set_timestamp;
    save_context;
    load_context ]

let language_commands () =
  [ gen_value;
    gen_rand_value;
    gen_sample_parameter;
    generate_contract_attributes;
    run_script ]

let repl_commands () =
  [ command
      ~group:repl_group
      ~desc:"Exit the REPL"
      no_options
      (fixed ["exit"])
      (fun () _ -> exit 0) ]

let sandbox_commands_with_man =
  Clic.add_manual
    ~executable_name:(Filename.basename Sys.executable_name)
    ~global_options
    (if Unix.isatty Unix.stdout then Clic.Ansi else Clic.Plain)
    Format.std_formatter
  @@ sandbox_commands ()

let el_cmd =
  command
    ~group:repl_group
    ~desc:"Run commands located in <filename> on the current context"
    no_options
    (prefixes ["run"; "commands"; "from"] @@ filename_param @@ stop)
    (fun () filename cctx ->
      let cctx_backup = cctx in
      let rec el cmds cctx =
        match cmds with
        | [] -> Em.return cctx
        | cmd :: tl ->
            Repl.split cmd >>=? fun args ->
            Clic.dispatch sandbox_commands_with_man cctx args >>=? fun cctx ->
            el tl cctx
      in
      let commands = Extdlib.File.read_lines filename in
      el commands cctx >>= function
      | Ok cctx -> Em.return cctx
      | Error _ ->
          Format.printf "Errors during application of commands.@. Reverting.@." ;
          Em.return cctx_backup)

let repl_commands_with_man =
  Clic.add_manual
    ~executable_name:(Filename.basename Sys.executable_name)
    ~global_options
    (if Unix.isatty Unix.stdout then Clic.Ansi else Clic.Plain)
    Format.std_formatter
  @@ (el_cmd :: repl_commands ())
  @ sandbox_commands ()

let usage () =
  Clic.usage
    Format.std_formatter
    ~executable_name:(Filename.basename Sys.executable_name)
    ~global_options
    repl_commands_with_man

let repl () =
  let rec repl context =
    try
      print_string "> " ;
      let line = read_line () in
      Repl.split line >>=? fun args ->
      match args with
      | [] ->
          usage () ;
          repl context
      | args -> (
          Clic.dispatch repl_commands_with_man context args >>= function
          | Ok context -> repl context
          | Error errs ->
              Clic.pp_cli_errors
                Format.err_formatter
                ~executable_name:""
                ~global_options
                ~default:Em.pp
                errs ;
              Format.eprintf "@." ;
              repl context )
    with End_of_file ->
      Format.printf "@.Thanks for using Michelson REPL 4.3.@." ;
      Em.return context
  in
  Driver_006.Chain_env.empty_context >>=? fun alpha ->
  let context = Context.make ~alpha in
  repl context
  >>= (function
        | Ok _ -> Em.return_unit
        | Error errs ->
            Clic.pp_cli_errors
              Format.err_formatter
              ~executable_name:""
              ~global_options
              ~default:Em.pp
              errs ;
            Format.eprintf "@." ;
            Em.return_unit)
  |> ignore ;
  Em.return_unit

(* that can't be called a REPL at this point *)
let cli_commands () =
  [ command
      ~group
      ~desc:"Launch the language server"
      no_options
      (fixed ["launch"; "lang"; "server"])
      (fun () ctx ->
        Lsp.Main.launch () ;
        Em.return ctx);
    command
      ~group
      ~desc:"Launch the try michelson backend"
      no_options
      (fixed ["launch"; "backend"])
      (fun () _ -> Backend.Server.launch ());
    command
      ~group
      ~desc:"Launch the REPL"
      no_options
      (fixed ["launch"; "repl"])
      (fun () _ -> repl ());
    command
      ~group:language_group
      ~desc:"Translate to micheline"
      no_options
      ( prefixes ["micheline"; "this"]
      @@ param ~name:"file" ~desc:"filename of the contract" ustring_param
      @@ stop )
      (fun () filename ctx -> Driver.print_micheline ~filename |> return ctx) ]

let commands_with_man =
  Clic.add_manual
    ~executable_name:(Filename.basename Sys.executable_name)
    ~global_options
    (if Unix.isatty Unix.stdout then Clic.Ansi else Clic.Plain)
    Format.std_formatter
  @@ cli_commands () @ language_commands ()

let usage () =
  Clic.usage
    Format.std_formatter
    ~executable_name:(Filename.basename Sys.executable_name)
    ~global_options
    commands_with_man

let main () =
  Lwt_main.run
  @@
  match Array.to_list Sys.argv with
  | [] | [_] ->
      usage () ;
      Em.return ()
  | _ :: args -> (
      (* Driver_006.Chain_env.empty_context >>=? fun context -> *)
      Clic.dispatch commands_with_man () args
      >>= function
      | Ok _context -> Em.return ()
      | Error errs ->
          Clic.pp_cli_errors
            Format.err_formatter
            ~executable_name:""
            ~global_options
            ~default:Em.pp
            errs ;
          Em.return () )
