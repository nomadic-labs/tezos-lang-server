open Data_encoding.Json

type t = Data_encoding.Json.t

let get_force id = function
  | `O elts -> List.assoc id elts
  | _ -> Stdlib.failwith "get force: Not an object"

let get id = function `O elts -> List.assoc_opt id elts | _ -> None

let string = function `String str -> Some str | _ -> None

let int = function `Float i -> Some (int_of_float i) | _ -> None

let array = function `A l -> Some l | _ -> None

let to_string = to_string

let construct = construct

let parse = from_string

module Short = struct
  let s = construct Data_encoding.string

  let i i = Int32.of_int i |> construct Data_encoding.int32

  let o l = `O l

  let l l = `A l

  let n = `Null

  let t = construct Data_encoding.bool true

  let f = construct Data_encoding.bool false
end
