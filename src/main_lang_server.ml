let _ = Printexc.record_backtrace true

(* We will use manual parsing as we just need to find if a flag is active. *)
let _ = Cli.main ()
