let capabilities =
  Json.Short.(
    o
      [ ("textDocumentSync", i 1);
        ("hoverProvider", t);
        ("completionProvider", o []);
        (* ("codeActionProvider", t); *)
        ("executeCommandProvider", o [("commands", l [s "showStack"])]) ])

let launch () =
  Log.log "launching_server" ;
  Basic_server.run
    ~tick:(fun a -> a)
    ~log:Log.log
    ~message_handlers:Message_handlers.handlers
    ~notification_handlers:Notification_handlers.handlers
    ~stdin_desc:(Unix.descr_of_in_channel stdin)
    ~capabilities
