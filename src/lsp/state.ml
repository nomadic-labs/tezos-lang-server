open Shared_models

type file_path = string

type uri = string

type document_info =
  { contents : string;
    (* Those two typing information belong together *)
    type_map : int list;
    typechecks : bool
  }

type type_info_micheline =
  ((Micheline_parser.location * bool) * string list * string list) list tzresult

type type_info = (Location.t * string list * string list) list tzresult

type state =
  { (* This is not needed for our use case *)
    (* root_path: file_path; *)
    (* root_uri: uri; *)
    (* settings: settings; *)
    document_text : (uri, string) Hashtbl.t;
    type_info : (uri, type_info) Hashtbl.t;
    document_timers : (uri, float * (unit -> state)) Hashtbl.t
  }

let empty () =
  { (* root_path = "- uninitialized -"; *)
    (* root_uri = "- uninitialized -"; *)
    document_text = Hashtbl.create 5;
    type_info = Hashtbl.create 5;
    document_timers = Hashtbl.create 10
  }

let get_contents uri state =
  match Hashtbl.find_opt state.document_text uri with
  | Some text -> text
  | None -> Stdlib.failwith @@ "I didn't load " ^ uri

let get_type_info uri state =
  match Hashtbl.find_opt state.type_info uri with
  | Some info ->
      Log.log "Memoised info" ;
      info
  | None ->
      let script = get_contents uri state in
      Log.log @@ "typechecking " ^ script ;
      let res =
        Lwt_main.run
        @@ Driver_005.Driver.typecheck_code ~script:(get_contents uri state)
      in
      let res =
        match res with
        | Error err -> Error err
        | Ok l ->
            let l =
              List.map
                (fun (mich_loc, before, after) ->
                  (Location.Decode.from_micheline mich_loc, before, after))
                l
            in
            Ok l
      in
      Hashtbl.replace state.type_info uri res ;
      res

(* Triggers a new typechecking *)
let set_text uri state text =
  Hashtbl.remove state.type_info uri ;
  Hashtbl.replace state.document_text uri text ;
  Language.typecheck uri state text get_type_info

let forget uri state =
  Hashtbl.remove state.document_text uri ;
  Hashtbl.remove state.type_info uri
