open State
open Rpc
open Rresult
module Protocol = Protocol_decrypter

let handlers : (string * (state -> Json.t -> (state, string) result)) list =
  [ ( "textDocument/didOpen",
      fun state params ->
        let open Json.Short in
        Log.log "got some params" ;
        Log.log @@ Json.to_string params ;
        let (uri, _, _, text) = Protocol.get_text_document_item params in
        Log.log @@ "add text" ^ text ;
        State.set_text uri state text ;
        Ok state );
    ( "textDocument/didChange",
      fun state params ->
        let open Json.Short in
        let uri = Protocol.uri_of_params params in
        let new_text =
          Json.get_force "contentChanges" params
          |> Json.array
          |> Option.unopt_exn (Failure "wasn't an array")
          |> List.hd |> Json.get_force "text" |> Json.string
          |> Option.unopt_exn (Failure "no content change")
        in
        State.set_text uri state new_text ;
        (* Hashtbl.add state.document_timers (0.3, Language.typecheck uri state); *)
        Ok state );
    ( "textDocument/didClose",
      fun state params ->
        let uri = Protocol.uri_of_params params in
        State.forget uri state ;
        Ok state ) ]
