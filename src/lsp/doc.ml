open Extdlib

let filter_doc json =
  let open Rpc in
  let open Rpc.Infix in
  let instrs = Json.get_force "instructions" json in
  let instrs =
    match Json.array instrs with
    | None -> Stdlib.failwith "bad doc"
    | Some a -> a
  in
  let f obj =
    let members =
      match obj with
      | `O members -> members
      | _ -> Stdlib.failwith "filterdoc: Not an object"
    in
    `O
      (List.filter_map
         (function
           | ("op", json) -> Some ("label", json)
           | ("documentation_short", json) -> Some ("documentation", json)
           | ("ty", json) ->
               Some
                 ( "detail",
                   Json.array json |! "" |> List.hd
                   |> Json.get_force "conclusion" )
           | _ -> None)
         members)
  in
  List.map f instrs

let json () =
  (* FIXME: this won't work anywhere else *)
  let path = Filename.dirname Sys.executable_name in
  Log.log path ;
  let contents = File.read @@ path ^ "/res/michelson.json" in
  match Json.parse contents with
  | Error _ -> Caml.failwith @@ "Couldn't open documentation"
  | Ok json -> filter_doc json

(* TODO: give type of element *)
let descr_of_op op =
  Log.log @@ "finding doc for \"" ^ op ^ "\"" ;
  let open Rpc in
  let open Rpc.Infix in
  let descr =
    List.find_opt
      (function
        | `O members ->
            List.exists
              (function
                | ("label", json) -> Json.string json |! "not a string" = op
                | _ -> false)
              members)
      (json ())
  in
  match descr with
  | None -> ""
  | Some o ->
      Json.get_force "documentation" o
      |> Json.string
      |! "lol" ^ (Json.get_force "detail" o |> Json.string |! "")
