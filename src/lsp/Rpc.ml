module Encodings = struct
  open Data_encoding

  let header = obj2 (req "jsonrpc" string) (req "id" json)

  let message = obj3 (req "jsonrpc" string) (req "id" json) (req "result" json)

  let error = obj3 (req "jsonrpc" string) (req "id" json) (req "error" json)

  let notification =
    obj3 (req "jsonrpc" string) (req "method" string) (req "params" json)

  let request =
    obj4
      (req "jsonrpc" string)
      (req "id" json)
      (req "method" string)
      (req "params" json)
end

module Infix = struct
  let ( |?>> ) = Option.( >>| )

  let ( |?> ) = Option.( >>= )

  (* force unwrap *)
  let ( |! ) o d = match o with None -> Caml.failwith d | Some v -> v

  (* unwrap with default *)
  let ( |? ) o d = match o with None -> d | Some v -> v

  (*  *)
end

open Infix

type jsonrpc =
  | Message of Json.t * string * Json.t
  | Notification of string * Json.t
  | Response of Json.t * Json.t

let msg_from_json json =
  let id = Json.get "id" json in
  let meth = Json.get "method" json |?> Json.string in
  let result = Json.get "result" json in
  let params = Json.get "params" json in
  match id with
  | None ->
      Notification
        (meth |! "method required", params |! "params required for notification")
  | Some id -> (
      match (meth, result) with
      | (_, Some result) -> Response (id, result)
      | (Some meth, _) -> Message (id, meth, params |! "params required")
      | (None, None) -> Caml.failwith "Either method or result required" )

let read_msg log input =
  let clen = input_line input in
  let cl = "Content-Length: " in
  let cll = String.length cl in
  log @@ string_of_int cll ;
  log clen ;
  if String.length clen >= cll && String.has_prefix ~prefix:cl clen then (
    let offset = if Sys.os_type = "Win32" then 0 else -1 in
    let num = String.sub clen cll (String.length clen - cll + offset) in
    log @@ "Num bytes to read: " ^ String.escaped num ;
    let num =
      match int_of_string num with
      | i -> i
      | exception Failure _ -> raise @@ Failure ("int_of_string :" ^ num)
    in
    let num = num + if Sys.os_type = "Win32" then 1 else 2 in
    let buffer = Buffer.create num in
    Buffer.add_channel buffer input num ;
    let raw = Buffer.contents buffer in
    log @@ "Read message " ^ raw ;
    match Json.parse raw with
    | Error err ->
        Caml.failwith @@ "Unable to parse message " ^ raw ^ " as json: " ^ err
    | Ok json -> msg_from_json json )
  else Caml.failwith @@ "Invalid header"

let send output content =
  let len = String.length content in
  let sep = if Sys.os_type = "Unix" then "\r\n\r\n" else "\n\n" in
  output_string output @@ "Content-Length: " ^ string_of_int len ^ sep ^ content ;
  flush output

let send_message log output id result =
  let content =
    Json.to_string ~minify:true
    @@ Data_encoding.Json.construct Encodings.message ("2.0", id, result)
  in
  log @@ "Sending message" ^ content ;
  send output content

let send_error log output id error =
  let content =
    Data_encoding.Json.construct Encodings.error ("2.0", id, error)
    |> Json.to_string ~minify:true
  in
  log @@ "Sending error " ^ content ;
  send output content

let send_notification log output meth params =
  let content =
    Json.construct Encodings.notification ("2.0", meth, params)
    |> Json.to_string ~minify:true
  in
  log @@ "Sending notification" ^ content ;
  send output content

let server_req_num = ref 0

let send_request log output meth params =
  incr server_req_num ;
  let content =
    Json.construct
      Encodings.request
      ( "2.0",
        Json.Short.s @@ "server-" ^ string_of_int !server_req_num,
        meth,
        params )
    |> Json.to_string
  in
  log @@ "Sending server request " ^ content ;
  send output content
