type uri = string

type file_path = string

type settings = { format_width : int option }

type document_info = {
  contents : string;
  (* Those two typing information belong together *)
  type_map : int list;
  typechecks : bool;
}

type type_info =
  ((Micheline_parser.location * bool) * string list * string list) list tzresult
