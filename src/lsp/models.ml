include Shared_models
open Extdlib

module Stack = struct
  type ty = Sexp of string * ty list | Atom of string

  module Repr = struct
    let rec dbg_string = function
      | Atom word -> Format.asprintf "Atom \"%s\"" word
      | Sexp (word, atoms) ->
          Format.asprintf
            "Sexp (%s, %s)"
            word
            (List.map dbg_string atoms |> String.concat ", ")

    let rec ty_to_string = function
      | Atom word -> word
      | Sexp (word, atoms) ->
          Format.asprintf
            "(%s %s)"
            word
            (List.map ty_to_string atoms |> String.concat " ")

    let to_string ty width =
      let rec to_string ty width spacing =
        Log.log @@ string_of_int spacing ;
        let unsplit = ty_to_string ty in
        if String.length unsplit + spacing <= width then unsplit
        else
          match ty with
          | Atom word -> word
          | Sexp (word, atoms) ->
              let indent_spaces = String.make (spacing + 2) ' ' in
              Format.asprintf
                "%s(%s@.%s%s)"
                ""
                word
                indent_spaces
                ( List.map (fun atom -> to_string atom width (spacing + 2)) atoms
                |> String.concat ("\n" ^ indent_spaces) )
      in
      to_string ty width 0
  end

  module Parse = struct
    let is_ident c =
      String.is_ident c
      || List.mem c ['_'; '.'; '%'; '@'; ':'; '$'; '&'; '!'; '?']

    let rec word_end_index str i =
      match str.[i] with
      | s when is_ident s -> word_end_index str (i + 1)
      | _ -> i
      | exception _ -> i

    let parse_word str i =
      let end_word = word_end_index str i in
      let length = end_word - i in
      (String.sub str i length, end_word)

    let rec parse_atom str i =
      let (word, index) = parse_word str i in
      (Atom word, index)

    and parse_atoms str i =
      match str.[i] with
      | ')' -> ([], i + 1)
      | '\n' | ' ' -> parse_atoms str (i + 1)
      | c when is_ident c ->
          let (word, i) = parse_word str i in
          Log.log word ;
          let (atoms, i) = parse_atoms str i in
          (Atom word :: atoms, i)
      | '(' ->
          let (exp, i) = parse_sexp str (i + 1) in
          let (atoms, i) = parse_atoms str i in
          (exp :: atoms, i)
      | c ->
          let s = String.make 1 c in
          Log.log ("not a sexp: " ^ s) ;
          Caml.failwith ("not a sexp: " ^ s)
      (* When we go beyond the end of the string *)
      | exception Invalid_argument _ -> ([], i)

    and parse_sexp str i =
      Log.log "parse sexp" ;
      let (word, index) = parse_word str i in
      Log.log word ;
      let (atoms, index) = parse_atoms str index in
      (Sexp (word, atoms), index)

    and entrypoint str = parse_atoms str 0 |> fst |> List.hd
  end
end
