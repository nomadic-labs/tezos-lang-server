(* This code is kind of equivalent to server.ml *)
module Driver = Driver_006.Driver
module Proto = Driver.Proto
module ParseTools = Driver.ParseTools
open Models
open Shared_models

let typecheck uri state text get_type_info =
  let (script, errs) = Driver.Michelson_v1_parser.parse_toplevel text in
  let parse_errs = ParseTools.Priority.prioritize_proto_errs errs in
  let type_info = get_type_info uri state in
  let type_errs =
    match type_info with
    | Ok _ -> []
    | Error errs -> ParseTools.Priority.prioritize_proto_errs errs
  in
  let errs = parse_errs @ type_errs in
  let errs = ParseTools.Priority.prioritize_errs errs in
  (* We only want to show the most important one *)
  let errs = match errs with [] -> [] | err :: _ -> [err] in
  let payload =
    List.map
      (fun err ->
        let (range, msg) = ParseTools.get_msg script err in
        let open Json.Short in
        o [("range", Location.to_json range); ("message", s msg)])
      errs
  in
  Rpc.send_notification
    Log.log
    stdout
    "textDocument/publishDiagnostics"
    Json.Short.(o [("uri", s uri); ("diagnostics", l payload)])
