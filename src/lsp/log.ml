let log =
  let oc = open_out_gen [ Open_append; Open_creat ] 0o666 "/tmp/log.txt" in
  fun s ->
    Printf.fprintf oc "%s\n" s;
    flush oc
