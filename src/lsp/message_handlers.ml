open State
open Rpc
open Rresult
open Models
open Extdlib
module Protocol = Protocol_decrypter

let handlers :
    (string * (state -> Json.t -> (state * Json.t, string) result)) list =
  [ ( "textDocument/hover",
      fun state params ->
        let open Json.Short in
        (* "params":{"textDocument":{"uri":"file:///tmp/lol.tz"},"position":{"line":2,"character":17}}} *)
        let uri = Protocol.uri_of_params params in
        let contents = State.get_contents uri state in
        Log.log contents ;
        let pos = Json.get_force "position" params |> Location.Decode.point in
        let loc =
          Protocol_decrypter.range_ints pos.line pos.col pos.line pos.col
        in
        let op = String.word (pos.col - 1) (String.line pos.line contents) in
        let contents = Doc.descr_of_op op in
        Ok
          ( state,
            o
              [ ("range", loc);
                ("contents", Protocol.content_kind ~markdown:true contents) ] )
    );
    ( "textDocument/completion",
      fun state _params ->
        let open Json.Short in
        Ok (state, o [("isIncomplete", f); ("items", l (Doc.json ()))]) );
    ( "textDocument/codeAction",
      fun state _params ->
        let open Json.Short in
        Ok (state, l [o [("title", s "Show Stack"); ("id", s "showStack")]]) );
    ( "workspace/executeCommand",
      fun state _params ->
        let open Json.Short in
        Log.log "There's a command executed" ;
        Ok (state, o []) );
    ( "custom:showStack",
      fun state params ->
        let open Json.Short in
        let open Rpc.Infix in
        let range =
          Json.get_force "cursor" params |> Location.Decode.range_with_end
        in
        let uri =
          Json.get_force "uri" params |> Json.string |! "not a string"
        in
        let type_info = State.get_type_info uri state in
        let str =
          match type_info with
          | Error _ -> "typecheck error"
          | Ok type_info -> (
              let type_info = Location.filter_expanded type_info in
              (* The list is reversed as it is "sorted" from largest to
                 narrowest interval. *)
              match
                List.assoc_third
                  (fun ({ start; _ } : Location.range) loc ->
                    Location.is_inside start.line start.col loc)
                  range
                @@ List.rev type_info
              with
              | None -> ""
              | Some (before, after) ->
                  Log.log "--- before stack ---" ;
                  Log.log @@ String.concat "\n" before ;
                  Log.log "--- after stack ---" ;
                  Log.log @@ String.concat "\n" after ;
                  let offset = 38 in
                  let space str width =
                    let len = String.length str in
                    str ^ String.make (width - len) ' '
                  in
                  let f offset ty =
                    let sexp = Stack.Parse.entrypoint ty in
                    Stack.Repr.to_string sexp offset
                  in
                  Log.log @@ String.concat "\n" (List.map (f 20000) before) ;
                  let after = List.map (f offset) after in
                  let before = List.map (f offset) before in
                  let concatenate a b =
                    match (a, b) with
                    | (None, None) -> assert false
                    | (Some value, None) ->
                        Format.asprintf "%s | %s" (space value offset) ""
                    | (None, Some value) ->
                        Format.asprintf "%s | %s" (space "" offset) value
                    | (Some a, Some b) ->
                        Format.asprintf "%s | %s" (space a offset) b
                  in
                  let reconciliate a b =
                    match (a, b) with
                    | (None, None) -> assert false
                    | (Some value, None) ->
                        let lines = String.split_on_char '\n' value in
                        List.map2_opt concatenate lines [] |> String.concat "\n"
                        |> fun s -> ( ^ ) s @@ "\n" ^ String.make 80 '-'
                    | (None, Some value) ->
                        let lines = String.split_on_char '\n' value in
                        List.map2_opt concatenate [] lines |> String.concat "\n"
                        |> fun s -> ( ^ ) s @@ "\n" ^ String.make 80 '-'
                    | (Some a, Some b) ->
                        let left = String.split_on_char '\n' a in
                        let right = String.split_on_char '\n' b in
                        List.map2_opt concatenate left right
                        |> String.concat "\n"
                        |> fun s -> ( ^ ) s @@ "\n" ^ String.make 80 '-'
                  in
                  List.map2_opt reconciliate (List.rev before) (List.rev after)
                  |> List.rev |> String.concat "\n" )
        in
        Ok (state, o [("after", s str)]) ) ]
