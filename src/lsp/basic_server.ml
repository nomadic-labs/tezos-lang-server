open State

module Utils = struct
  let slice_to_end s start =
    let len = String.length s in
    if start <= len then String.sub s start (len - start) else ""

  let parseUri uri =
    if String.has_prefix ~prefix:"file://" uri then
      let without_scheme = slice_to_end uri 7 in
      if Sys.os_type = "Unix" then Some without_scheme
      else (
        (* FIXME: Windows not supported yet *)
        Log.log "in windows land" ;
        None )
    else None
end

module Main = struct
  open Rpc
  open Infix

  let get_init_state _params =
    let state = State.empty () in
    (* Rpc.send_notification Log.log stdout "client/registerCapability"
       Rpc.Json.Short.(
         o([
           ("registrations", l[o[]])
         ])); *)
    Ok state
end

let can_read desc =
  let (r, _, _) = Unix.select [desc] [] [] 0.1 in
  r != []

open Rpc

type msg_severity = Error | Warning | Info | Log

let severity = function Error -> 1 | Warning -> 2 | Info -> 3 | Log -> 4

let show_message log ty msg =
  Rpc.send_notification
    log
    stdout
    "window/showMessage"
    Json.Short.(o [("type", i (severity ty)); ("message", s msg)])

let handle_message log message_handlers id meth params (state : state) =
  let start = Unix.gettimeofday () in
  log @@ "[server] Got a method " ^ meth ;
  match List.assoc meth message_handlers with
  | exception Not_found ->
      Rpc.send_error
        log
        stdout
        id
        Json.Short.(
          o [("code", i (-32601)); ("message", s ("Unexpected method: " ^ meth))]) ;
      state
  | handler -> (
      log @@ "[server] processing took "
      ^ string_of_float ((Unix.gettimeofday () -. start) *. 1000.)
      ^ "ms" ;
      let result = handler state params in
      match result with
      | Ok (state, result) ->
          Rpc.send_message log stdout id result ;
          state
      | Error str ->
          Rpc.send_error
            log
            stdout
            id
            Json.Short.(o [("code", i (-32603)); ("message", s str)]) ;
          state
      | exception e ->
          Rpc.send_error
            log
            stdout
            id
            Json.Short.(
              o
                [ ("code", i (-32603));
                  ( "message",
                    s (Printexc.to_string e ^ Printexc.get_backtrace ()) ) ]) ;
          state )

let handle_notification log notification_handlers meth params state =
  match List.assoc meth notification_handlers with
  | exception Not_found -> state
  | handler -> (
      match handler state params with
      | Ok state -> state
      | Error str ->
          show_message log Error str ;
          state
      | exception e ->
          show_message log Error (Printexc.to_string e) ;
          state )

let run ~tick ~log ~message_handlers ~notification_handlers ~stdin_desc
    ~capabilities =
  let rec loop ~shutdown state =
    let state = tick state in
    if can_read stdin_desc then
      match Rpc.read_msg log stdin with
      | Message (id, "shutdown", _) ->
          Rpc.send_message log stdout id Json.(`Null) ;
          loop ~shutdown:true state
      | Message (id, meth, params) ->
          loop ~shutdown
          @@ handle_message log message_handlers id meth params state
      | Notification ("exit", _) ->
          if shutdown then log "Got exit - terminating."
          else log "Got exit without shutdown ~" ;
          exit 1
      | Notification (meth, params) ->
          loop ~shutdown
          @@ handle_notification log notification_handlers meth params state
      | _ -> loop ~shutdown state
    else loop ~shutdown state
  in
  let initialize () =
    let open Rpc in
    match Rpc.read_msg log stdin with
    | Message (id, "initialize", params) -> (
        match Main.get_init_state params with
        | Ok state ->
            show_message log Info "Hello from the server!" ;
            Rpc.send_message log stdout id
            @@ Json.Short.o [("capabilities", capabilities)] ;
            loop ~shutdown:false state
        | Error str -> Rpc.send_error log stdout id @@ Json.Short.s str )
    | _ -> Caml.failwith "Client must send initialize as first event"
  in
  initialize ()
