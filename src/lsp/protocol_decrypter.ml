open Rpc
open Infix
open Json.Short

module Encodings = struct
  open Data_encoding

  let textDocumentItem =
    obj4 (req "uri" string) (req "languageId" string) (req "version" int32)
      (req "text" string)
end

let pos ~line ~char = o [ ("line", i line); ("character", i char) ]

let range ~start ~stop = o [ ("start", start); ("end", stop) ]

let range_ints l0 c0 l1 c1 =
  range ~start:(pos ~line:l0 ~char:c0) ~stop:(pos ~line:l1 ~char:c1)

let get_pos json =
  let line = Json.get "line" json |?> Json.int in
  let char = Json.get "character" json |?> Json.int in
  line |?> fun line ->
  char |?> fun char -> Some (line, char)

let content_kind ~markdown txt =
  o
    [
      ("kind", s (if markdown then "markdown" else "plaintext"));
      ("value", s txt);
    ]

let path_of_uri uri =
  assert (String.has_prefix ~prefix:"file://" uri);
  String.sub uri 7 (String.length uri - 7)

let uri_of_params params =
  let uri =
    Json.get_force "textDocument" params
    |> Json.get_force "uri" |> Json.string
    |> Option.unopt_exn (Failure "No uri in hoover")
  in
  uri

let get_text_document_item json =
  Json.get_force "textDocument" json
  |> Data_encoding.Json.destruct Encodings.textDocumentItem
