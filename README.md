# Michelson Language Server

This project aims to provide some nice features
to people using Michelson to develop smart contracts
on the Tezos Blockchain.

It provides three modes of interaction.

## language-server

This mode behaves as a traditional language server.
The binary should be called by an extension of your editor
of choice. Launch that mode using `./lang-server launch lang server`

### Features

* Typechecking
* Autocompletion
* Stack Visualization

## Command Mode

This project also presents a sandbox running a dummy blockchain
environment for all your testing needs. You can easily create accounts,
originate contracts, and call them. You can launch it invoking
`./lang-server launch repl`. It is advised to use `rlwrap`.

_Language Commands_ are available outside of the REPL. To invoke
them, use `./lang-server $command`

### Commands

#### Language commands

 ```
 micheline this <file>
    Translate to micheline
    <file>: filename of the contract
```
```
  gen value of type <ty>
    Generate value of given type
    <ty>: type
```
```
  gen rand value of type <ty>
    Generate random value of a given type
    <ty>: type
```
```
  gen sample parameter of <script> [--entrypoint <value>]
    Get sample parameter for script
    <script>: file location
    --entrypoint <value>: Entrypoint on which to call the contract on
```
```
  gen attributes of script <script>
    Generate contract attributes (possible values of storage and parameters)
    <script>: file location
```

#### Sandbox commands

```
create account <name> [-f --funds <value>]
  -f --funds <value>`: Specify the funds of the account. Defaults to 8000
```
```
transfer <amount> from <source> to <destination> [--arg <value>]
    [--entrypoint <value>]
  Transfer tokens from <source> to <destination>
  <amount>: amount transferred
  <source>: Spent contract
  <destination>: Credited contract
  --arg <value>: Parameter of the contract call
  --entrypoint <value>: Entrypoint on which to call the contract on
```
```
originate contract <name> <file> [--storage <value>] [--amount <value>]
  originate a contract from <filename> putting <value> tokens on it wich will be known as <name>
  <name>: alias of the contract
  <file>: filename of the contract
  --storage <value>: Storage of the contract
  --amount <value>: Amount put in the balance of the originated contract
```
```
list accounts
  List known accounts
```
```
list contracts
  List contracts
```
```
now
  prints the value of now
```
```
set timestamp <timestamp>
  Set NOW to specified timestamp
  <timestamp>: timestamp to set now to
```
```
save context [--filename <value>]
  Save the context in a file on-disk
  --filename <value>: filename
```
```
load context from <file>
  Load the context from a file
  <file>: filename of the contract
```

#### Repl commands
```
  run commands from <file>
    Run commands located in <filename> on the current context
    <file>: filename of the contract
```
```
  exit
    Exit the REPL
```

## Server Backend

The third and last way to use this software is to use it as a server backend.
Launch the server, and it will respond to HTTP requests. This is what try-michelson
uses. You can use it invoking `./lang-server launch backend`

### RPCs


## Setup

If you want to use this project in the language server mode,
know that the vscode extension distributes the binary. Please
refer to the usage manual of the extension you wish to use this project with.

Otherwise, follow these steps:

```
make build-deps
make
```