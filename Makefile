
all:
	eval $$(opam env) && dune build src/
	@cp _build/default/src/main_lang_server.exe tezos-lang-server
	@chmod +w tezos-lang-server

build-deps:
	scripts/build-deps.sh

build-deps-ci:
	scripts/build-deps.sh CI

docker:
	docker build -f docker/Dockerfile -t nomadiclabs/tezos-lang-server .

push-docker: docker
	docker push nomadiclabs/tezos-lang-server

clean:
	dune clean

clean-dist:
	dune clean
	rm -Rf _opam

lint:
	ocamlformat --inplace $(shell find src \( -name "*.ml" -o -name "*.mli"  -o -name "*.mlt" \) -type f -print)

run: all
	rlwrap ./tezos-lang-server launch repl

.PHONY: docker
