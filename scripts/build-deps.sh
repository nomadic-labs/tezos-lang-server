#!/bin/bash

set -x -e

script_dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"
src_dir="$(dirname "$script_dir")"

TEZOS_BRANCH=master
OPAMYES=true
PROJECT=lang-server

eval $(opam env)

# when invoked with a parameter we assume from the CI and skip the initialization
if [[ ! -d "$src_dir/_opam" ]] && [[ $# -eq 0 ]] ; then
  opam switch create "$src_dir" 4.09.1
fi
eval $(opam env --shell=sh)

opam install --deps-only "$src_dir/src"
